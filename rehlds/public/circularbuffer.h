/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

#pragma once

#include "tier0/dbg.h"

class CCircularBuffer
{
public:
	CCircularBuffer();
	CCircularBuffer(int size);
	void SetSize(int nSize);

protected:
	inline void AssertValid()
	{
#ifdef _DEBUG
		Assert(this);
		Assert(m_nSize > 0);
		Assert(m_nCount >= 0);
		Assert(m_nCount <= m_nSize);
		Assert(m_nWrite < m_nSize);

		// Verify that m_nCount is correct.
		if (m_nRead == m_nWrite)
		{
			Assert(m_nCount == 0 || m_nCount == m_nSize);
		}
		else
		{
			int testCount=0;
			if (m_nRead < m_nWrite)
				testCount = m_nWrite - m_nRead;
			else
				testCount = (m_nSize - m_nRead) + m_nWrite;

			Assert(testCount == m_nCount);
		}
#endif // _DEBUG
	}

public:

	void Flush();
	int GetSize();					// Get the size of the buffer (how much can you write without reading
									// before losing data.

	int GetWriteAvailable();		// Get the amount available to write without overflowing.
									// Note: you can write however much you want, but it may overflow,
									// in which case the newest data is kept and the oldest is discarded.

	int GetReadAvailable();			// Get the amount available to read.

	int GetMaxUsed();
	int Peek(char *pchDest, int nCount);
	int Advance(int nCount);
	int Read(void *pchDest, int nCount);
	int Write(void *pchData, int nCount);

public:
	int m_nCount;		// Space between the read and write pointers (how much data we can read).

	int m_nRead;		// Read index into circular buffer
	int m_nWrite;		// Write index into circular buffer

	int m_nSize;		// Size of circular buffer in bytes (how much data it can hold).
	char m_chData[1];	// Circular buffer holding data
};


// Use this to instantiate a CircularBuffer.
template<int size>
class CSizedCircularBuffer: public CCircularBuffer
{
public:
	CSizedCircularBuffer() : CCircularBuffer(size) {}

private:
	char myData[size - 1];
};

CCircularBuffer *AllocateCircularBuffer(int nSize);
void FreeCircularBuffer(CCircularBuffer *pCircularBuffer);
