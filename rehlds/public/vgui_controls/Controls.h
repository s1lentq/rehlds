#ifndef CONTROLS_H
#define CONTROLS_H

#ifdef _WIN32
#pragma once
#endif

#include "interface.h"
#include "ikeyvalues.h"

#include <vgui/VGUI.h>
#include <vgui/IPanel.h>
#include <vgui/KeyCode.h>

namespace vgui2
{

// handles the initialization of the vgui interfaces
// interfaces (listed below) are first attempted to be loaded from primaryProvider, then secondaryProvider
// moduleName should be the name of the module that this instance of the vgui_controls has been compiled into
bool VGuiControls_Init(const char *moduleName, CreateInterfaceFn *factoryList, int numFactories);

// returns the name of the module as specified above
const char *GetControlsModuleName();

class IKeyValues;
class IInput;
class ISchemeManager;
class ISurface;
class ISystem;
class IVGui;
class IPanel;
class IFileSystem;
class ILocalize;

extern IKeyValues *g_pKeyValuesInterface;
extern IInput *g_pInputInterface;
extern ISchemeManager *g_pSchemeInterface;
extern ISurface *g_pSurfaceInterface;
extern ISystem *g_pSystemInterface;
extern IVGui *g_pVGuiInterface;
extern IPanel *g_pPanelInterface;
extern IFileSystem *g_pFileSystemInterface;
extern ILocalize *g_pLocalizeInterface;

// set of accessor functions to vgui interfaces
// the appropriate header file for each is listed above the item

// #include <ikeyvalues.h>
IKeyValues *keyvalues()
{
	return g_pKeyValuesInterface;
}

// #include <vgui/IInput.h>
inline IInput *input()
{
	return g_pInputInterface;
}

// #include <vgui/IScheme.h>
inline ISchemeManager *scheme()
{
	return g_pSchemeInterface;
}

// #include <vgui/ISurface.h>
inline ISurface *surface()
{
	return g_pSurfaceInterface;
}

// #include <vgui/ISystem.h>
inline ISystem *system()
{
	return g_pSystemInterface;
}

// #include <vgui/IVGui.h>
inline IVGui *ivgui()
{
	return g_pVGuiInterface;
}

// #include <vgui/IPanel.h>
inline IPanel *ipanel()
{
	return g_pPanelInterface;
}

// #include <ikeyvalues.h>
inline IFileSystem *filesystem()
{
	return g_pFileSystemInterface;
}

// #include <vgui/ILocalize.h>
inline ILocalize *localize()
{
	return g_pLocalizeInterface;
}

// predeclare all the vgui control class names
class Button;
class ComboBox;
class EditablePanel;
class Frame;
class HTML;
class ImagePanel;
class MessageBox;
class Panel;
class QueryBox;

// vgui controls helper classes
class IBorder;
class IImage;
class Image;
class ImageList;
class TextImage;

} // namespace vgui2

// hotkeys disabled until we work out exactly how we want to do them
#define VGUI_HOTKEYS_ENABLED
// #define VGUI_DRAW_HOTKEYS_ENABLED

#endif // CONTROLS_H
