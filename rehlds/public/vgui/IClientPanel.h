/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

#pragma once

#include <vgui/VGUI.h>

#ifdef GetClassName
#undef GetClassName
#endif

class KeyValues;

namespace vgui2
{
class Panel;
class SurfaceBase;

enum EInterfaceID
{
	ICLIENTPANEL_STANDARD_INTERFACE = 0,
};

// Interface from vgui2 panels -> Client panels
// This interface cannot be changed without rebuilding all vgui2 projects
// Primarily this interface handles dispatching messages from core vgui2 to controls
// The additional functions are all their for debugging or optimization reasons
// To add to this later, use QueryInterface() to see if they support new interfaces
class IClientPanel
{
public:
	virtual VPANEL GetVPanel() = 0;

	// straight interface to Panel functions
	virtual void Think() = 0;
	virtual void PerformApplySchemeSettings() = 0;
	virtual void PaintTraverse(bool forceRepaint, bool allowForce) = 0;
	virtual void Repaint() = 0;
	virtual VPANEL IsWithinTraverse(int x, int y, bool traversePopups) = 0;
	virtual void GetInset(int &top, int &left, int &right, int &bottom) = 0;
	virtual void GetClipRect(int &x0, int &y0, int &x1, int &y1) = 0;
	virtual void OnChildAdded(VPANEL child) = 0;
	virtual void OnSizeChanged(int newWide, int newTall) = 0;

	virtual void InternalFocusChanged(bool lost) = 0;
	virtual bool RequestInfo(KeyValues *outputData) = 0;
	virtual void RequestFocus(int direction) = 0;
	virtual bool RequestFocusPrev(VPANEL existingPanel) = 0;
	virtual bool RequestFocusNext(VPANEL existingPanel) = 0;
	virtual void OnMessage(const KeyValues *params, VPANEL ifromPanel) = 0;
	virtual VPANEL GetCurrentKeyFocus() = 0;
	virtual int GetTabPosition() = 0;

	// for debugging purposes
	virtual const char *GetName() = 0;
	virtual const char *GetClassName() = 0;

	// get scheme handles from panels
	virtual HScheme GetScheme() = 0;		// get scheme handles from panels
	virtual bool IsProportional() = 0;		// gets whether or not this panel should scale with screen resolution
	virtual bool IsAutoDeleteSet() = 0;		// auto-deletion
	virtual void DeletePanel() = 0;			// deletes this

	// interfaces
	virtual void *QueryInterface(EInterfaceID id) = 0;

	// returns a pointer to the vgui controls baseclass Panel *
	virtual Panel *GetPanel() = 0;

	// returns the name of the module this panel is part of
	virtual const char *GetModuleName() = 0;
};

} // namespace vgui2
