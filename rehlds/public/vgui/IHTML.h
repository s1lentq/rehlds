/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

#pragma once

// TODO: These methods have not been tested on compatibility.
// Check me.
namespace vgui2
{

// basic interface for a HTML window
class IHTML
{
public:
	// open a new page
	virtual void OpenURL(const char *URL, const char *postData, bool force) = 0;

	virtual bool StopLoading() = 0;	// stops the existing page from loading
	virtual bool Refresh() = 0;		// refreshes the current page
};

// basic callback interface for a HTML window
class IHTMLEvents
{
public:
	// call backs for events
	virtual bool OnStartURL(const char *url, const char *target, bool first) = 0;
	virtual void OnFinishURL(const char *url) = 0;
	virtual void OnProgressURL(long current, long maximum) = 0;
	virtual void OnSetStatusText(const char *text) = 0;
	virtual void OnUpdate() = 0;
	virtual void OnLink() = 0;
	virtual void OffLink() = 0;
};

} // namespace vgui2
