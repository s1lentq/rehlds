/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

#pragma once

#include <stdlib.h>
#include <string.h>
#include <vgui/VGUI.h>

#include "utlvector.h"

namespace vgui2
{

// Simple lightweight dynamic array implementation
template<class ELEMTYPE> class Dar: public CUtlVector<ELEMTYPE>
{
	typedef CUtlVector<ELEMTYPE> BaseClass;

public:
	Dar() {}
	Dar(int initialCapacity) : BaseClass(0, initialCapacity) {}

public:
	void SetCount(int count)
	{
		this->EnsureCount(count);
	}
	int GetCount()
	{
		return this->Count();
	}
	int AddElement(ELEMTYPE elem)
	{
		return this->AddToTail(elem);
	}
	void MoveElementToEnd(ELEMTYPE elem)
	{
		if (this->Count() == 0)
			return;

		// quick check to see if it's already at the end
		if (this->Element(this->Count() - 1) == elem)
			return;

		int idx = this->Find(elem);
		if (idx == this->InvalidIndex())
			return;

		this->Remove(idx);
		this->AddToTail(elem);
	}
	// returns the index of the element in the array, -1 if not found
	int FindElement(ELEMTYPE elem)
	{
		return this->Find(elem);
	}
	bool HasElement(ELEMTYPE elem)
	{
		if (this->FindElement(elem) != this->InvalidIndex())
		{
			return true;
		}
		return false;
	}
	int PutElement(ELEMTYPE elem)
	{
		int index = this->FindElement(elem);
		if (index >= 0)
		{
			return index;
		}
		return this->AddElement(elem);
	}
	// insert element at index and move all the others down 1
	void InsertElementAt(ELEMTYPE elem, int index)
	{
		this->InsertBefore(index, elem);
	}
	void SetElementAt(ELEMTYPE elem, int index)
	{
		this->EnsureCount(index + 1);
		this->Element(index) = elem;
	}
	void RemoveElementAt(int index)
	{
		this->Remove(index);
	}
	void RemoveElementsBefore(int index)
	{
		if (index <= 0)
			return;
		this->RemoveMultiple(0, index - 1);
	}
	void RemoveElement(ELEMTYPE elem)
	{
		this->FindAndRemove(elem);
	}
	void *GetBaseData()
	{
		return this->Base();
	}
	void CopyFrom(Dar<ELEMTYPE> &dar)
	{
		this->CopyArray(dar.Base(), dar.Count());
	}
};

} // namespace vgui2
