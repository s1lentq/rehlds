/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

#pragma once

#include <vgui/VGUI.h>
#include <vgui/KeyCode.h>

#include "interface.h"

namespace vgui2
{

class Cursor;
typedef unsigned long HCursor;

#define VGUI_GCS_COMPREADSTR		0x0001
#define VGUI_GCS_COMPREADATTR		0x0002
#define VGUI_GCS_COMPREADCLAUSE		0x0004
#define VGUI_GCS_COMPSTR			0x0008
#define VGUI_GCS_COMPATTR			0x0010
#define VGUI_GCS_COMPCLAUSE			0x0020
#define VGUI_GCS_CURSORPOS			0x0080
#define VGUI_GCS_DELTASTART			0x0100
#define VGUI_GCS_RESULTREADSTR		0x0200
#define VGUI_GCS_RESULTREADCLAUSE	0x0400
#define VGUI_GCS_RESULTSTR			0x0800
#define VGUI_GCS_RESULTCLAUSE		0x1000

// style bit flags for WM_IME_COMPOSITION
#define VGUI_CS_INSERTCHAR			0x2000
#define VGUI_CS_NOMOVECARET			0x4000

#define MESSAGE_CURSOR_POS			-1
#define MESSAGE_CURRENT_KEYFOCUS	-2

class IInput: public IBaseInterface
{
public:
	virtual void SetMouseFocus(VPANEL newMouseFocus) = 0;
	virtual void SetMouseCapture(VPANEL panel) = 0;

	// returns the string name of a scan code
	virtual void GetKeyCodeText(KeyCode code, char *buf, int buflen) = 0;

	// focus
	virtual VPANEL GetFocus() = 0;
	virtual VPANEL GetMouseOver() = 0;		// returns the panel the mouse is currently over, ignoring mouse capture

	// mouse state
	virtual void SetCursorPos(int x, int y) = 0;
	virtual void GetCursorPos(int &x, int &y) = 0;
	virtual bool WasMousePressed(MouseCode code) = 0;
	virtual bool WasMouseDoublePressed(MouseCode code) = 0;
	virtual bool IsMouseDown(MouseCode code) = 0;

	// cursor override
	virtual void SetCursorOveride(HCursor cursor) = 0;
	virtual HCursor GetCursorOveride() = 0;

	// key state
	virtual bool WasMouseReleased(MouseCode code) = 0;
	virtual bool WasKeyPressed(KeyCode code) = 0;
	virtual bool IsKeyDown(KeyCode code) = 0;
	virtual bool WasKeyTyped(KeyCode code) = 0;
	virtual bool WasKeyReleased(KeyCode code) = 0;

	virtual VPANEL GetAppModalSurface() = 0;
	// set the modal dialog panel.
	// all events will go only to this panel and its children.
	virtual void SetAppModalSurface(VPANEL panel) = 0;
	// release the modal dialog panel
	// do this when your modal dialog finishes.
	virtual void ReleaseAppModalSurface() = 0;

	virtual void GetCursorPosition(int &x, int &y) = 0;

	// virtual destructor
	virtual ~IInput() {}
};

} // namespace vgui2

#define VGUI_INPUT_INTERFACE_VERSION "VGUI_Input004"
