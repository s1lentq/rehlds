#if defined(_WIN32)
#include <windows.h> // for WideCharToMultiByte and MultiByteToWideChar
#elif defined(_LINUX)
#include <wchar.h> // wcslen()
#define _alloca alloca
#endif

#include <ikeyvalues.h>
#include <KeyValues.h>
#include <Color.h>
#include <stdlib.h>

#include "filesystem.h"
#include "utlvector.h"
#include "utlbuffer.h"

#define KEYVALUES_TOKEN_SIZE 512
static char s_pTokenBuf[ KEYVALUES_TOKEN_SIZE ];

#define INTERNALWRITE(pData, len) pBuf.Put(pData, len)

// Constructor
KeyValues::KeyValues(const char *setName)
{
	Init();
	SetName(setName);
}

// Constructor
KeyValues::KeyValues(const char *setName, const char *firstKey, const char *firstValue)
{
	Init();
	SetName(setName);
	SetString(firstKey, firstValue);
}

// Constructor
KeyValues::KeyValues(const char *setName, const char *firstKey, const wchar_t *firstValue)
{
	Init();
	SetName(setName);
	SetWString(firstKey, firstValue);
}

// Constructor
KeyValues::KeyValues(const char *setName, const char *firstKey, int firstValue)
{
	Init();
	SetName(setName);
	SetInt(firstKey, firstValue);
}

// TODO: NOXREF
// Constructor
KeyValues::KeyValues(const char *setName, const char *firstKey, const char *firstValue, const char *secondKey, const char *secondValue)
{
	Init();
	SetName(setName);
	SetString(firstKey, firstValue);
	SetString(secondKey, secondValue);
}

// Constructor
KeyValues::KeyValues(const char *setName, const char *firstKey, int firstValue, const char *secondKey, int secondValue)
{
	Init();
	SetName(setName);
	SetInt(firstKey, firstValue);
	SetInt(secondKey, secondValue);
}

// Initialize member variables
void KeyValues::Init()
{
	m_iKeyName = INVALID_KEY_SYMBOL;
	m_iDataType = TYPE_NONE;

	m_pSub = nullptr;
	m_pPeer = nullptr;

	m_iAllocationSize = 0;
	m_iValue = 0;
}

void KeyValues::FreeAllocatedValue()
{
	if (m_iAllocationSize <= 0)
		return;

	if (m_iAllocationSize > sizeof(KeyValues))
	{
		::delete [] m_pValue;
		m_iAllocationSize = 0;
		return;
	}

	vgui::keyvalues()->FreeKeyValuesMemory(m_pValue);
	m_iAllocationSize = 0;
}

void KeyValues::AllocateValueBlock(int iAllocSize)
{
	if (iAllocSize > sizeof(KeyValues))
	{
		m_iAllocationSize = iAllocSize;
		m_pValue = ::operator new (iAllocSize);
		return;
	}

	m_iAllocationSize = iAllocSize;
	m_pValue = vgui::keyvalues()->AllocKeyValuesMemory(iAllocSize);
}

// Destructor
KeyValues::~KeyValues()
{
	KeyValues *dat;
	KeyValues *datNext = nullptr;
	for (dat = m_pSub; dat != nullptr; dat = datNext)
	{
		datNext = dat->m_pPeer;
		dat->m_pPeer = nullptr;
		delete dat;
	}

	FreeAllocatedValue();
}

// Adds a chain... if we don't find stuff in this keyvalue, we'll look
// in the one we're chained to.
void KeyValues::ChainKeyValue(KeyValues *pChain)
{
	m_pChain = pChain;
}

// Get the name of the current key section
const char *KeyValues::GetName() const
{
	return vgui::keyvalues()->GetStringForSymbol(m_iKeyName);
}

// Get the symbol name of the current key section
int KeyValues::GetNameSymbol() const
{
	return m_iKeyName;
}

// Read a single token from buffer (0 terminated)
char *ReadToken(char **buffer)
{
	char c;
	int nCount = 0;

	// eating white spaces and remarks loop
	while (true)
	{
		do
		{
			c = *(*buffer)++;
		}
		while (isspace(c))

		if (c != '/')
			break;

		c = *(*buffer)++;
		if (c != '/')
		{
			s_pTokenBuf[0] = '/';
			nCount = 1;
			break;
		}

		do
		{
			if (c == '\n')
				break;

			c = *(*buffer)++;
		}
		while (c > 0);
	}

	// read quoted strings specially
	if (c == '"')
	{
		while (nCount < KEYVALUES_TOKEN_SIZE)
		{
			c = *(*buffer)++;
			if (c == '"')
				break;

			// end of file
			if (c == 0)
				break;

			if (c == '\\')
			{
				c = **buffer;
				*buffer = *buffer + 1;

				switch (c)
				{
				case 'n'  s_pTokenBuf[ nCount++ ] = '\n'; break;
				case '\\' s_pTokenBuf[ nCount++ ] = '\\'; break;
				case 't'  s_pTokenBuf[ nCount++ ] = '\t'; break;
				case '"'  s_pTokenBuf[ nCount++ ] = '"';  break;
				}
			}
		}
	}
	else
	{
		while (nCount < KEYVALUES_TOKEN_SIZE)
		{
			// end of file
			if (c == 0)
				break;

			// break on whitespace
			if (isspace(c))
				break;

			// add char to buffer
			s_pTokenBuf[ nCount++ ] = c;
			c = *(*buffer)++;
		}
	}

	if (c < 0 && nCount == 0)
		return nullptr;

	if (nCount >= KEYVALUES_TOKEN_SIZE)
		nCount = KEYVALUES_TOKEN_SIZE - 1;

	s_pTokenBuf[ nCount ] = '\0';
	return s_pTokenBuf;
}

// Load keyValues from disk
bool KeyValues::LoadFromFile(IFileSystem *filesystem, const char *resourceName, const char *pathID)
{
	FileHandle_t f = filesystem->Open(resourceName, "rb", pathID);
	if (!f)
		return false;

	// load file into a null-terminated buffer
	int fileSize = filesystem->Size(f);
	char *buffer = (char *)malloc(fileSize + 1);
	if (!buffer)
		return false;

	// null terminate file as EOF
	buffer[fileSize] = '\0';
	filesystem->Read(buffer, fileSize, f);

	LoadFromBuffer(buffer);

	// close file after reading
	filesystem->Close(f);
	free(buffer);

	return true;
}

// Save the keyvalues to disk
// Creates the path to the file if it doesn't exist
bool KeyValues::SaveToFile(IBaseFileSystem *filesystem, const char *resourceName, const char *pathID)
{
	// create a write file
	FileHandle_t f = filesystem->Open(resourceName, "wb", pathID);
	if (f == FILESYSTEM_INVALID_HANDLE)
	{
		char szBuf[1024];
		while (true)
		{
			const char *fileSlash = Q_strchr(currentFileName, '\\');
			if (!fileSlash)
				break;

			int pathSize = fileSlash - currentFileName;
			Q_strcat(szBuf, "\\");
			Q_strncat(szBuf, currentFileName, pathSize);
			Q_mkdir(szBuf);

			currentFileName += pathSize + 1;
		}

		f = filesystem->Open(resourceName, "wb", pathID);
		if (f == FILESYSTEM_INVALID_HANDLE)
			return false;
	}

	CUtlBuffer buf(0, 4096, CUtlBuffer::TEXT_BUFFER);
	RecursiveSaveToFile(filesystem, buf, 0);
	buf.PutChar(0);

	filesystem->Write(response.Base(), buf.TellPut(), f);
	filesystem->Close(f);

	return true;
}

// Write out a set of indenting
void KeyValues::WriteIndents(CUtlBuffer &pBuf, int indentLevel)
{
	for (int i = 0; i < indentLevel; i++)
	{
		INTERNALWRITE("\t", 1);
	}
}

// Write out a string where we convert the double quotes to backslash double quote
void KeyValues::WriteConvertedString(CUtlBuffer &pBuf, const char *pszString)
{
	// handle double quote chars within the string
	// the worst possible case is that the whole string is quotes
	int len = Q_strlen(pszString);

	// TODO: check me, how much we need to allocate
	//char *convertedString = (char *)_alloca(2 * len + (sizeof(wchar_t) * sizeof(wchar_t) + 1));
	char *convertedString = (char *)_alloca((len + 1) * sizeof(char) * 2);

	int j = 0;
	for (int i = 0; i <= len; i++)
	{
		if (pszString[i] == '"')
		{
			convertedString[j] = '\\';
			j++;
		}
		else if (pszString[i] == '\\')
		{
			convertedString[j] = '\\';
			j++;
		}
		convertedString[j] = pszString[i];
		j++;
	}

	INTERNALWRITE(convertedString, Q_strlen(convertedString));
}

// Save keyvalues from disk, if subkey values are detected, calls itself to save those
void KeyValues::RecursiveSaveToFile(IFileSystem *filesystem, CUtlBuffer &pBuf, int indentLevel)
{
	// write header
	WriteIndents(pBuf, indentLevel);
	INTERNALWRITE("\"", 1);
	WriteConvertedString(pBuf, GetName());
	INTERNALWRITE("\"\n", 2);
	WriteIndents(pBuf, indentLevel);
	INTERNALWRITE("{\n", 2);

	// loop through all our keys writing them to disk
	for (KeyValues *dat = m_pSub; dat != nullptr; dat = dat->m_pPeer)
	{
		if (dat->m_pSub)
		{
			dat->RecursiveSaveToFile(filesystem, pBuf, indentLevel + 1);
		}
		else
		{
			// only write non-empty keys
			switch (dat->m_iDataType)
			{
			case TYPE_STRING:
			{
				if (dat->m_sValue && *(dat->m_sValue))
				{
					WriteIndents(pBuf, indentLevel + 1);
					INTERNALWRITE("\"", 1);
					INTERNALWRITE(dat->GetName(), Q_strlen(dat->GetName()));
					INTERNALWRITE("\"\t\t\"", 4);

					WriteConvertedString(pBuf, dat->m_sValue);
					INTERNALWRITE("\"\n", 2);
				}
				break;
			}
			case TYPE_WSTRING:
			{
				if (dat->m_sValue)
				{
					char szBuf[1024];
					vgui::keyvalues()->GetANSIFromLocalized((const wchar_t *)dat->m_sValue, szBuf, sizeof(szBuf));

					WriteIndents(pBuf, indentLevel + 1);
					INTERNALWRITE("\"", 1);
					INTERNALWRITE(dat->GetName(), Q_strlen(dat->GetName()));
					INTERNALWRITE("\"\t\t\"", 4);

					WriteConvertedString(pBuf, szBuf);
					INTERNALWRITE("\"\n", 2);
				}
				break;
			}
			case TYPE_INT:
			{
				WriteIndents(pBuf, indentLevel + 1);

				INTERNALWRITE("\"", 1);
				INTERNALWRITE(dat->GetName(), Q_strlen(dat->GetName()));
				INTERNALWRITE("\"\t\t\"", 4);

				char buf[32];
				Q_snprintf(buf, sizeof(buf), "%d", dat->m_iValue);

				INTERNALWRITE(buf, Q_strlen(buf));
				INTERNALWRITE("\"\n", 2);
				break;
			}
			case TYPE_FLOAT:
			{
				WriteIndents(pBuf, indentLevel + 1);

				INTERNALWRITE("\"", 1);
				INTERNALWRITE(dat->GetName(), Q_strlen(dat->GetName()));
				INTERNALWRITE("\"\t\t\"", 4);

				char buf[32];
				Q_snprintf(buf, sizeof(buf), "%f", dat->m_flValue);

				INTERNALWRITE(buf, Q_strlen(buf));
				INTERNALWRITE("\"\n", 2);
				break;
			}
			default:
				break;
			}
		}
	}

	// write tail
	WriteIndents(pBuf, indentLevel);
	INTERNALWRITE("}\n", 2);
}

// looks up a key by symbol name
KeyValues *KeyValues::FindKey(int keySymbol) const
{
	for (KeyValues *dat = m_pSub; dat != nullptr; dat = dat->m_pPeer)
	{
		if (dat->m_iKeyName == keySymbol)
			return dat;
	}

	return nullptr;
}

// Find a keyValue, create it if it is not found.
// Set bCreate to true to create the key if it doesn't already exist (which ensures a valid pointer will be returned)
KeyValues *KeyValues::FindKey(const char *keyName, bool bCreate)
{
	// return the current key if a nullptr subkey is asked for
	if (!keyName || !keyName[0])
		return this;

	// look for '/' characters deliminating sub fields
	char szBuf[256];
	const char *subStr = Q_strchr(keyName, '/');
	const char *searchStr = keyName;

	// pull out the substring if it exists
	if (subStr)
	{
		int size = subStr - keyName;
		Q_memcpy(szBuf, keyName, size);
		szBuf[size] = '\0';
		searchStr = szBuf;
	}

	// lookup the symbol for the search string
	HKeySymbol iSearchStr = vgui::keyvalues()->GetSymbolForString(searchStr);
	if (iSearchStr == INVALID_KEY_SYMBOL)
	{
		// not found, couldn't possibly be in key value list
		return nullptr;
	}

	KeyValues *lastItem = nullptr;
	KeyValues *dat;
	// find the searchStr in the current peer list
	for (dat = m_pSub; dat != nullptr; dat = dat->m_pPeer)
	{
		lastItem = dat;	// record the last item looked at (for if we need to append to the end of the list)

		// symbol compare
		if (dat->m_iKeyName == iSearchStr)
		{
			break;
		}
	}

	// make sure a key was found
	if (!dat)
	{
		if (bCreate)
		{
			// we need to create a new key
			dat = new KeyValues(searchStr);

			// insert new key at end of list
			if (lastItem)
			{
				lastItem->m_pPeer = dat;
			}
			else
			{
				m_pSub = dat;
			}
			dat->m_pPeer = nullptr;

			// a key graduates to be a submsg as soon as it's m_pSub is set
			// this should be the only place m_pSub is set
			m_iDataType = TYPE_NONE;
		}
		else
		{
			return nullptr;
		}
	}

	// if we've still got a subStr we need to keep looking deeper in the tree
	if (subStr)
	{
		// recursively chain down through the paths in the string
		return dat->FindKey(subStr + 1, bCreate);
	}

	return dat;
}

// Create a new key, with an autogenerated name.
// Name is guaranteed to be an integer, of value 1 higher than the highest other integer key name
KeyValues *KeyValues::CreateNewKey()
{
	int newID = 1;

	// search for any key with higher values
	for (KeyValues *dat = m_pSub; dat != nullptr; dat = dat->m_pPeer)
	{
		// case-insensitive string compare
		int val = atoi(dat->GetName());
		if (newID <= val)
		{
			newID = val + 1;
		}
	}

	char buf[12];
	Q_snprintf(buf, sizeof(buf), "%d", newID);

	return FindKey(buf, true);
}

// Adds a subkey. Make sure the subkey isn't a child of some other keyvalues
void KeyValues::AddSubKey(KeyValues *pSubkey)
{
	// Make sure the subkey isn't a child of some other keyvalues
	Assert(pSubkey->m_pPeer == nullptr);

	// add into subkey list
	if (m_pSub == nullptr)
	{
		m_pSub = pSubkey;
	}
	else
	{
		KeyValues *pTempDat = m_pSub;
		while (pTempDat->GetNextKey() != nullptr)
		{
			pTempDat = pTempDat->GetNextKey();
		}

		pTempDat->SetNextKey(pSubkey);
	}
}

// Remove a subkey from the list
void KeyValues::RemoveSubKey(KeyValues *subKey)
{
	if (!subKey)
		return;

	// check the list pointer
	if (m_pSub == subKey)
	{
		m_pSub = subKey->m_pPeer;
	}
	else
	{
		// look through the list
		KeyValues *kv = m_pSub;
		while (kv->m_pPeer)
		{
			if (kv->m_pPeer == subKey)
			{
				kv->m_pPeer = subKey->m_pPeer;
				break;
			}

			kv = kv->m_pPeer;
		}
	}

	subKey->m_pPeer = nullptr;
}

// Return the first subkey in the list
KeyValues *KeyValues::GetFirstSubKey()
{
	return m_pSub;
}

// Return the next subkey
KeyValues *KeyValues::GetNextKey()
{
	return m_pPeer;
}

// Sets this key's peer to the KeyValues passed in
void KeyValues::SetNextKey(KeyValues *pDat)
{
	m_pPeer = pDat;
}

// Get the integer value of a keyName. Default value is returned if the keyName can't be found.
int KeyValues::GetInt(const char *keyName, int defaultValue)
{
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
	{
		switch (dat->m_iDataType)
		{
		case TYPE_STRING:
			return atoi(dat->m_sValue);
		case TYPE_WSTRING:
#ifdef _WIN32
			return _wtoi((const wchar_t *)dat->m_sValue);
#else
			return wcstol((const wchar_t *)dat->m_sValue, 0, 10);
#endif
		case TYPE_FLOAT:
			return (int)dat->m_flValue;
		case TYPE_UINT64:
			// can't convert, since it would lose data
			Assert(0);
			return 0;
		case TYPE_INT:
		case TYPE_PTR:
		default:
			return dat->m_iValue;
		}
	}

	return defaultValue;
}

// Get the integer value of a keyName.
// Default value is returned if the keyName can't be found.
uint64 KeyValues::GetUint64(const char *keyName, uint64 defaultValue)
{
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
	{
		switch (dat->m_iDataType)
		{
		case TYPE_STRING:
			return atoi(dat->m_sValue);
		case TYPE_WSTRING:
#ifdef _WIN32
			return _wtoi((const wchar_t *)dat->m_sValue);
#else
			return wcstol((const wchar_t *)dat->m_sValue, 0, 10);
#endif
		case TYPE_FLOAT:
			return (int)dat->m_flValue;
		case TYPE_UINT64:
			return *((uint64 *)dat->m_sValue);
		case TYPE_INT:
		case TYPE_PTR:
		default:
			return dat->m_iValue;
		}
	}

	return defaultValue;
}

// Get the pointer value of a keyName.
// Default value is returned if the keyName can't be found.
void *KeyValues::GetPtr(const char *keyName, void *defaultValue)
{
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
	{
		switch (dat->m_iDataType)
		{
		case TYPE_PTR:
			return dat->m_pValue;

		case TYPE_WSTRING:
		case TYPE_STRING:
		case TYPE_FLOAT:
		case TYPE_INT:
		case TYPE_UINT64:
		default:
			return nullptr;
		}
	}

	return defaultValue;
}

// Get the float value of a keyName.
// Default value is returned if the keyName can't be found.
float KeyValues::GetFloat(const char *keyName, float defaultValue)
{
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
	{
		switch (dat->m_iDataType)
		{
		case TYPE_STRING:
			return (float)atof(dat->m_sValue);
		case TYPE_FLOAT:
			return dat->m_flValue;
		case TYPE_INT:
			return (float)dat->m_iValue;
		case TYPE_UINT64:
			return (float)(*((uint64 *)dat->m_sValue));
		case TYPE_PTR:
		default:
			return 0.0f;
		}
	}

	return defaultValue;
}

// Get the string pointer of a keyName.
// Default value is returned if the keyName can't be found.
const char *KeyValues::GetString(const char *keyName, const char *defaultValue)
{
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
	{
		// convert the data to string form then return it
		char buf[64];
		switch (dat->m_iDataType)
		{
		case TYPE_FLOAT:
			Q_snprintf(buf, sizeof(buf), "%f", dat->m_flValue);
			SetString(keyName, buf);
			break;
		case TYPE_INT:
		case TYPE_PTR:
			Q_snprintf(buf, sizeof(buf), "%d", dat->m_iValue);
			SetString(keyName, buf);
			break;
		case TYPE_UINT64:
			Q_snprintf(buf, sizeof(buf), "%I64i", *((uint64 *)(dat->m_sValue)));
			SetString(keyName, buf);
			break;
		case TYPE_WSTRING:
		{
			// convert the string to char *, set it for future use, and return it
			char wideBuf[512];
			vgui::keyvalues()->GetANSIFromLocalized((const wchar_t *)dat->m_sValue, wideBuf, sizeof(wideBuf));
			SetString(keyName, wideBuf);
			break;
		}
		case TYPE_STRING:
			break;
		default:
			return defaultValue;
		}

		return dat->m_sValue;
	}

	return defaultValue;
}

const wchar_t *KeyValues::GetWString(const char *keyName, const wchar_t *defaultValue)
{
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
	{
		wchar_t wbuf[64];
		switch (dat->m_iDataType)
		{
		case TYPE_FLOAT:
			Q_snwprintf(wbuf, sizeof(wbuf), L"%f", dat->m_flValue);
			SetWString(keyName, wbuf);
			break;
		case TYPE_INT:
		case TYPE_PTR:
			Q_snwprintf(wbuf, sizeof(wbuf), L"%d", dat->m_iValue);
			SetWString(keyName, wbuf);
			break;
		case TYPE_UINT64:
			Q_snwprintf(wbuf, sizeof(wbuf), L"%I64i", *((uint64 *)(dat->m_sValue)));
			SetWString(keyName, wbuf);
			break;
		case TYPE_STRING:
		{
			// convert to wide
			wchar_t wbuftemp[512];
			vgui::keyvalues()->GetLocalizedFromANSI(dat->m_sValue, wbuftemp, sizeof(wbuftemp));
			SetWString(keyName, wbuftemp);
			break;
		}
		case TYPE_WSTRING:
			break;
		default:
			return defaultValue;
		}

		return (const wchar_t *)dat->m_sValue;
	}

	return defaultValue;
}

// Gets a color
Color KeyValues::GetColor(const char *keyName)
{
	Color color(0, 0, 0, 0);
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
	{
		if (dat->m_iDataType == TYPE_COLOR)
		{
			color[0] = dat->m_Color[0];
			color[1] = dat->m_Color[1];
			color[2] = dat->m_Color[2];
			color[3] = dat->m_Color[3];
		}
		else if (dat->m_iDataType == TYPE_FLOAT)
		{
			color[0] = dat->m_flValue;
		}
		else if (dat->m_iDataType == TYPE_INT)
		{
			color[0] = dat->m_iValue;
		}
		else if (dat->m_iDataType == TYPE_STRING)
		{
			// parse the colors out of the string
			float a, b, c, d;
			Q_sscanf(dat->m_sValue, "%f %f %f %f", &a, &b, &c, &d);
			color[0] = (unsigned char)a;
			color[1] = (unsigned char)b;
			color[2] = (unsigned char)c;
			color[3] = (unsigned char)d;
		}
	}

	return color;
}

// Sets a color
void KeyValues::SetColor(const char *keyName, Color value)
{
	KeyValues *dat = FindKey(keyName, true);
	if (dat)
	{
		dat->m_iDataType = TYPE_COLOR;
		dat->m_Color[0] = value[0];
		dat->m_Color[1] = value[1];
		dat->m_Color[2] = value[2];
		dat->m_Color[3] = value[3];
	}
}

// Set the string value of a keyName.
void KeyValues::SetString(const char *keyName, const char *value)
{
	KeyValues *dat = FindKey(keyName, true);
	if (dat)
	{
		dat->FreeAllocatedValue();
		if (!value)
		{
			// ensure a valid value
			value = "";
		}

		// allocate memory for the new value and copy it in
		int len = Q_strlen(value) + 1;

		dat->AllocateValueBlock(len);
		dat->m_iDataType = TYPE_STRING;

		Q_memcpy(dat->m_sValue, value, len);
	}
}

// Set the string value of a keyName.
void KeyValues::SetWString(const char *keyName, const wchar_t *value)
{
	KeyValues *dat = FindKey(keyName, true);
	if (dat)
	{
		dat->FreeAllocatedValue();
		if (!value)
		{
			// ensure a valid value
			value = L"";
		}

		// allocate memory for the new value and copy it in
		int cch = wcslen(value);
		int len = (cch + 1) * sizeof(wchar_t);

		dat->AllocateValueBlock(len);
		dat->m_iDataType = TYPE_WSTRING;

		Q_memcpy(dat->m_sValue, value, len);
	}
}

// Set the integer value of a keyName.
void KeyValues::SetInt(const char *keyName, int value)
{
	KeyValues *dat = FindKey(keyName, true);
	if (dat)
	{
		dat->FreeAllocatedValue();
		dat->m_iValue = value;
		dat->m_iDataType = TYPE_INT;
	}
}

// Set the long integer value of a keyName.
void KeyValues::SetUint64(const char *keyName, uint64 value)
{
	KeyValues *dat = FindKey(keyName, true);
	if (dat)
	{
		// delete the old value
		dat->FreeAllocatedValue();
		dat->AllocateValueBlock(sizeof(uint64));
		dat->m_iDataType = TYPE_UINT64;
		*((uint64 *)dat->m_sValue) = value;
	}
}

// Set the float value of a keyName.
void KeyValues::SetFloat(const char *keyName, float value)
{
	KeyValues *dat = FindKey(keyName, true);
	if (dat)
	{
		dat->FreeAllocatedValue();
		dat->m_flValue = value;
		dat->m_iDataType = TYPE_FLOAT;
	}
}

void KeyValues::SetName(const char *setName)
{
	m_iKeyName = vgui::keyvalues()->GetSymbolForString(setName);
}

// Set the pointer value of a keyName.
void KeyValues::SetPtr(const char *keyName, void *value)
{
	KeyValues *dat = FindKey(keyName, true);
	if (dat)
	{
		dat->FreeAllocatedValue();
		dat->m_pValue = value;
		dat->m_iDataType = TYPE_PTR;
	}
}

// Make a new copy of all subkeys, add them all to the passed-in keyvalues
void KeyValues::CopySubkeys(KeyValues *pParent) const
{
	// recursively copy subkeys
	// Also maintain ordering....
	KeyValues *pPrev = nullptr;
	for (KeyValues *sub = m_pSub; sub != nullptr; sub = sub->m_pPeer)
	{
		// take a copy of the subkey
		KeyValues *dat = sub->MakeCopy();

		// add into subkey list
		if (pPrev)
		{
			pPrev->m_pPeer = dat;
		}
		else
		{
			pParent->m_pSub = dat;
		}

		dat->m_pPeer = nullptr;
		pPrev = dat;
	}
}

// Makes a copy of the whole key-value pair set
KeyValues *KeyValues::MakeCopy() const
{
	KeyValues *newKeyValue = new KeyValues(GetName());

	// copy data
	newKeyValue->m_iDataType = m_iDataType;
	switch (m_iDataType)
	{
	case TYPE_STRING:
	{
		if (m_sValue)
		{
			int len = Q_strlen(m_sValue) + 1;
			newKeyValue->AllocateValueBlock(len);
			Q_memcpy(newKeyValue->m_sValue, m_sValue, len);
		}
		break;
	}
	case TYPE_WSTRING:
	{
		if (m_sValue)
		{
			int cch = wcslen((const wchar_t *)m_sValue);
			int len = (cch + 1) * sizeof(wchar_t);

			newKeyValue->AllocateValueBlock(len);
			Q_memcpy(newKeyValue->m_sValue, m_sValue, len);
		}
		break;
	}
	case TYPE_INT:
		newKeyValue->m_iValue = m_iValue;
		break;
	case TYPE_FLOAT:
		newKeyValue->m_flValue = m_flValue;
		break;
	case TYPE_PTR:
		newKeyValue->m_pValue = m_pValue;
		break;
	case TYPE_COLOR:
		newKeyValue->m_Color[0] = m_Color[0];
		newKeyValue->m_Color[1] = m_Color[1];
		newKeyValue->m_Color[2] = m_Color[2];
		newKeyValue->m_Color[3] = m_Color[3];
		break;
	case TYPE_UINT64:
		newKeyValue->AllocateValueBlock(sizeof(uint64));
		Q_memcpy(newKeyValue->m_sValue, m_sValue, sizeof(uint64));
		break;
	}

	// recursively copy subkeys
	CopySubkeys(newKeyValue);
	return newKeyValue;
}

// Check if a keyName has no value assigned to it.
bool KeyValues::IsEmpty(const char *keyName)
{
	KeyValues *dat = FindKey(keyName, false);
	if (!dat)
		return true;

	if (dat->m_iDataType == TYPE_NONE && dat->m_pSub == nullptr)
		return true;

	return false;
}

// Clear out all subkeys, and the current value
void KeyValues::Clear()
{
	delete m_pSub;
	m_pSub = nullptr;
	m_iDataType = TYPE_NONE;
}

// Get the data type of the value stored in a keyName
KeyValues::types_t KeyValues::GetDataType(const char *keyName)
{
	KeyValues *dat = FindKey(keyName, false);
	if (dat)
		return (types_t)dat->m_iDataType;

	return TYPE_NONE;
}

// Deletion, ensures object gets deleted from correct heap
void KeyValues::deleteThis()
{
	delete this;
}

// Read from a buffer...
void KeyValues::LoadFromBuffer(const char *buffer)
{
	char *bufStart, *s;

	bufStart = const_cast<char *>(buffer);
	s = ReadToken(reinterpret_cast<char **>(&bufStart));

	if (s)
	{
		m_iKeyName = vgui::keyvalues()->GetSymbolForString(s);
	}

	s = ReadToken(reinterpret_cast<char **>(&bufStart));

	if (m_iKeyName != INVALID_KEY_SYMBOL && s && s[0] == '{')
	{
		RecursiveLoadFromBuffer(reinterpret_cast<char **>(&bufStart));
	}
}

void KeyValues::RecursiveLoadFromBuffer(char **buffer)
{
	// keep this out of the stack until a key is parsed
	while (true)
	{
		// get the value
		char *value = ReadToken(buffer);
		if (!value || value[0] == '\0')
			break;

		if (Q_strncmp(value, "}", 2) == 0)
			break;

		KeyValues *dat = FindKey(value, true);
		value = ReadToken(buffer);

		if (!value || Q_strncmp(value, "}", 2) == 0)
			break;

		if (Q_strncmp(value, "{", 2) == 0)
		{
			// sub value list
			RecursiveLoadFromBuffer(dat, buffer);
		}
		else
		{
			int len = Q_strlen(value) + 1;

			// CHECK ME!
			// TODO: probably need via the pointer of dat
			// dat->FreeAllocatedValue() !!

			FreeAllocatedValue();
			dat->AllocateValueBlock(len);
			dat->m_iDataType = TYPE_STRING;

			// copy in the string information
			Q_memcpy(dat->m_sValue, value, len);
		}
	}
}

// memory allocator
void *KeyValues::operator new(size_t iAllocSize)
{
	return vgui::keyvalues()->AllocKeyValuesMemory(iAllocSize);
}

void *KeyValues::operator new(size_t iAllocSize, int nBlockUse, const char *pFileName, int nLine)
{
	return vgui::keyvalues()->AllocKeyValuesMemory(iAllocSize);
}

// deallocator
void KeyValues::operator delete(void *pMem)
{
	vgui::keyvalues()->FreeKeyValuesMemory(pMem);
}

void KeyValues::operator delete(void *pMem, int nBlockUse, const char *pFileName, int nLine)
{
	vgui::keyvalues()->FreeKeyValuesMemory(pMem);
}
