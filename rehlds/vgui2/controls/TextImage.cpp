#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <assert.h>
#include <malloc.h>

#include <vgui/IPanel.h>
#include <vgui/ISurface.h>
#include <vgui/IScheme.h>
#include <vgui/IInput.h>
#include <vgui/ILocalize.h>
#include <KeyValues.h>

#include <vgui_controls/TextImage.h>
#include <vgui_controls/Controls.h>

using namespace vgui;

// Constructor
TextImage::TextImage(const char *text) : Image()
{
	_utext = nullptr;
	_font = INVALID_FONT;
	_fallbackFont = INVALID_FONT;
	_unlocalizedTextSymbol = INVALID_STRING_INDEX;
	_drawWidth = 0;
	_textBufferLen = 0;
	_textLen = 0;
	m_bWrap = false;
	m_LineBreaks.RemoveAll();
	m_pwszEllipsesPosition = nullptr;
	m_bUseFallbackFont = false;
	m_bRenderUsingFallbackFont = false;

	// set the text.
	SetText(text);
}

// Constructor
TextImage::TextImage(const wchar_t *wszText) : Image()
{
	_utext = nullptr;
	_font = INVALID_FONT;
	_fallbackFont = INVALID_FONT;
	_unlocalizedTextSymbol = INVALID_STRING_INDEX;
	_drawWidth = 0;
	_textBufferLen = 0;
	_textLen = 0;
	m_bWrap = false;
	m_LineBreaks.RemoveAll();
	m_bUseFallbackFont = false;
	m_bRenderUsingFallbackFont = false;

	// set the text.
	SetText(wszText);
}

// Destructor
TextImage::~TextImage()
{
	delete [] _utext;
}

// takes the string and looks it up in the localization file to convert it to unicode
void TextImage::SetText(const char *text)
{
	if (!text)
	{
		text = "";
	}

	// check for localization
	if (*text == '#')
	{
		// try lookup in localization tables
		_unlocalizedTextSymbol = localize()->FindIndex(text + 1);

		if (_unlocalizedTextSymbol != INVALID_STRING_INDEX)
		{
			wchar_t *unicode = localize()->GetValueByIndex(_unlocalizedTextSymbol);
			SetText(unicode);
			return;
		}
	}

	// convert the ansi string to unicode and use that
	wchar_t unicode[1024];
	localize()->ConvertANSIToUnicode(text, unicode, sizeof(unicode));
	SetText(unicode);
}

// Sets the width that the text can be.
void TextImage::SetDrawWidth(int width)
{
	_drawWidth = width;
	m_bRecalculateTruncation = true;
}

// Gets the width that the text can be.
void TextImage::GetDrawWidth(int &width)
{
	width = _drawWidth;
}

// sets unicode text directly
void TextImage::SetText(const wchar_t *unicode)
{
	if (!unicode)
	{
		unicode = L"";
	}

	// reallocate the buffer if necessary
	_textLen = (short)wcslen(unicode);
	if (_textLen >= _textBufferLen)
	{
		delete [] _utext;
		_textBufferLen = (short)(_textLen + 1);
		_utext = new wchar_t[_textBufferLen];
	}

	m_LineBreaks.RemoveAll();

	// store the text as unicode
	wcscpy(_utext, unicode);
	m_bRecalculateTruncation = true;

	if (m_bWrap)
	{
		RecalculateNewLinePositions();
	}
}

// Gets the text in the textImage
void TextImage::GetText(char *buffer, int bufferSize)
{
	localize()->ConvertUnicodeToANSI(_utext, buffer, bufferSize);
}

// Gets the text in the textImage
void TextImage::GetText(wchar_t *buffer, int bufLenInBytes)
{
	wcsncpy(buffer, _utext, bufLenInBytes);
}

// data accessor
void TextImage::GetUnlocalizedText(char *buffer, int bufferSize)
{
	if (_unlocalizedTextSymbol != INVALID_STRING_INDEX)
	{
		const char *text = localize()->GetNameByIndex(_unlocalizedTextSymbol);
		buffer[0] = '#';
		Q_strncpy(buffer + 1, text, bufferSize - 1);
		buffer[bufferSize - 1] = '\0';
	}
	else
	{
		GetText(buffer, bufferSize);
	}
}

// unlocalized text symbol
StringIndex_t TextImage::GetUnlocalizedTextSymbol()
{
	return _unlocalizedTextSymbol;
}

// Changes the current font
void TextImage::SetFont(HFont font)
{
	_font = font;
	m_bRecalculateTruncation = true;
}

// Gets the font of the text.
HFont TextImage::GetFont()
{
	if (m_bRenderUsingFallbackFont)
	{
		return _fallbackFont;
	}

	return _font;
}

// Sets the size of the TextImage. This is directly tied to drawWidth
void TextImage::SetSize(int wide, int tall)
{
	Image::SetSize(wide, tall);
	_drawWidth = wide;
	m_bRecalculateTruncation = true;
}

// Draw the Image on screen.
void TextImage::Paint()
{
	int wide, tall;
	GetSize(wide, tall);

	if (!_utext || GetFont() == INVALID_FONT)
		return;

	if (m_bRecalculateTruncation)
	{
		RecalculateEllipsesPosition();
	}

	DrawSetTextColor(GetColor());
	HFont font = GetFont();
	DrawSetTextFont(font);

	int lineHeight = surface()->GetFontTall(font);
	int x = 0, y = 0;

	int px, py;
	GetPos(px, py);

	int currentLineBreak = 0;

	for (wchar_t *wsz = _utext; *wsz != 0; wsz++)
	{
		wchar_t ch = wsz[0];

		// check for special characters
		if (ch == '\r')
		{
			// ignore, just use \n for newlines
			continue;
		}
		else if (ch == '\n')
		{
			// newline
			x = 0;
			y += lineHeight;
			continue;
		}
		else if (ch == '&')
		{
			// "&&" means draw a single ampersand, single one is a shortcut character
			if (wsz[1] == '&')
			{
				// just move on and draw the second ampersand
				wsz++;
			}
			else
			{
				// draw the underline, then continue to the next character without moving forward
#ifdef VGUI_DRAW_HOTKEYS_ENABLED
				DrawPrintChar(x + px, y + py, '_');
#endif
				continue;
			}
		}

		// see if we've hit the truncated portion of the string
		if (wsz == m_pwszEllipsesPosition)
		{
			// string is truncated, draw ellipses
			for (int i = 0; i < 3; i++)
			{
				surface()->DrawSetTextPos(x + px, y + py);
				surface()->DrawUnicodeChar('.');
				x += surface()->GetCharacterWidth(font, '.');
			}
			break;
		}

		if (currentLineBreak != m_LineBreaks.Count())
		{
			if (wsz == m_LineBreaks[currentLineBreak])
			{
				x = 0;
				y += lineHeight;
				currentLineBreak++;
			}
		}

		// Underlined text wants to draw the spaces anyway
		surface()->DrawSetTextPos(x + px, y + py);
		surface()->DrawUnicodeChar(ch);
		x += surface()->GetCharacterWidth(font, ch);
	}
}

// Get the size of a text string in pixels
void TextImage::GetTextSize(int &wide, int &tall)
{
	wide = 0;
	tall = 0;
	int maxWide = 0;
	const wchar_t *text = _utext;

	HFont font = _font;
	if (font == INVALID_FONT)
		return;

	if (m_bWrap)
		RecalculateNewLinePositions();

	// For height, use the remapped font
	int fontHeight = surface()->GetFontTall(GetFont());
	tall = fontHeight;

	int textLen = wcslen(text);
	for (int i = 0; i < textLen; i++)
	{
		// handle stupid special characters, these should be removed
		if (text[i] == '&' && text[i + 1] != 0)
		{
			i++;
		}

		int a, b, c;
		surface()->GetCharABCwide(font, text[i], a, b, c);
		wide += (a + b + c);

		if (text[i] == '\n')
		{
			tall += fontHeight;
			if(wide > maxWide)
			{
				maxWide = wide;
			}

			// new line, wide is reset...
			wide = 0;
		}

		if (m_bWrap)
		{
			for(int j = 0; j < m_LineBreaks.Count(); j++)
			{
				if (&text[i] == m_LineBreaks[j])
				{
					tall += fontHeight;
					if(wide > maxWide)
					{
						maxWide = wide;
					}

					// new line, wide is reset...
					wide = 0;
				}
			}
		}

	}

	if (wide < maxWide)
	{
		// maxWide only gets set if a newline is in the label
		wide = maxWide;
	}
}

// Get the size of the text in the image
void TextImage::GetContentSize(int &wide, int &tall)
{
	GetTextSize(wide, tall);
}

// Resize the text image to the content size
void TextImage::ResizeImageToContent()
{
	int wide, tall;
	GetContentSize(wide, tall);
	SetSize(wide, tall);
}GetContentSize

// Recalculates line breaks
void TextImage::RecalculateNewLinePositions()
{
	HFont font = GetFont();

	int charWidth;
	int x = 0;

	//int wordStartIndex = 0;
	wchar_t *wordStartIndex = _utext;
	int wordLength = 0;
	bool hasWord = false;
	bool justStartedNewLine = true;
	bool wordStartedOnNewLine = true;

	int startChar = 0;

	// clear the line breaks list
	m_LineBreaks.RemoveAll();

	// handle the case where this char is a new line, in that case
	// we have already taken its break index into account above so skip it.
	if (_utext[startChar] == '\r' || _utext[startChar] == '\n')
	{
		startChar++;
	}

	// loop through all the characters
	for (wchar_t *wsz = &_utext[startChar]; *wsz != '\0'; wsz++)
	{
		wchar_t ch = wsz[0];

		// line break only on whitespace characters
		if (!iswspace(ch))
		{
			if (!hasWord)
			{
				// Start a new word
				wordStartIndex = wsz;
				hasWord = true;
				wordStartedOnNewLine = justStartedNewLine;
				wordLength = 0;
			}
			//else append to the current word
		}
		else
		{
			// whitespace/punctuation character
			// end the word
			hasWord = false;
		}

		// get the width
		charWidth = surface()->GetCharacterWidth(font, ch);
		if (iswprint(ch))
		{
			justStartedNewLine = false;
		}

		// check to see if the word is past the end of the line [wordStartIndex, i)
		if ((x + charWidth) >= _drawWidth || ch == '\r' || ch == '\n')
		{
			justStartedNewLine = true;
			hasWord = false;

			if (ch == '\r' || ch == '\n')
			{
				// set the break at the current character
				//don't do this, paint will manually wrap on newline chars
				//m_LineBreaks.AddToTail(i);
			}
			else if (wordStartedOnNewLine)
			{
				// word is longer than a line, so set the break at the current cursor
				m_LineBreaks.AddToTail(wsz);
			}
			else
			{
				// set it at the last word Start
				m_LineBreaks.AddToTail(wordStartIndex);

				// just back to reparse the next line of text
				// ywb 8/1/07: Back off one extra char since the 'continue' will increment wsz for us by one in the for loop
				wsz = wordStartIndex - 1;
			}

			// reset word length
			wordLength = 0;
			x = 0;
			continue;
		}

		// add to the size
		x += charWidth;
		wordLength += charWidth;
	}
}

// Calculates where the text should be truncated
void TextImage::RecalculateEllipsesPosition()
{
	m_bRecalculateTruncation = false;
	m_pwszEllipsesPosition = nullptr;

	// don't insert ellipses on wrapped strings
	if (m_bWrap)
		return;

	// don't truncate strings with newlines
	if (wcschr(_utext, '\n'))
		return;

	for (int check = 0; check < (m_bUseFallbackFont ? 2 : 1); ++check)
	{
		HFont font = GetFont();
		if (check == 1 && _fallbackFont != INVALID_FONT)
		{
			font = _fallbackFont;
			m_pwszEllipsesPosition = nullptr;
			m_bRenderUsingFallbackFont = true;
		}

		int ellipsesWidth = 3 * surface()->GetCharacterWidth(font, '.');
		int x = 0;

		for (wchar_t *wsz = _utext; *wsz != 0; wsz++)
		{
			wchar_t ch = wsz[0];

			// check for special characters
			if (ch == '\r')
			{
				// ignore, just use \n for newlines
				continue;
			}
			else if (ch == '&')
			{
				// "&&" means draw a single ampersand, single one is a shortcut character
				if (wsz[1] == '&')
				{
					// just move on and draw the second ampersand
					wsz++;
				}
				else
				{
					continue;
				}
			}

			// don't truncate the first character
			if (wsz == _utext)
				continue;

			int len = surface()->GetCharacterWidth(font, ch);
			if (x + len + ellipsesWidth > _drawWidth)
			{
				// potential have an ellipses, see if the remaining characters will fit
				int remainingLength = len;
				for (const wchar_t *rwsz = wsz + 1; *rwsz != 0; rwsz++)
				{
					// check for special characters
					if (rwsz[0] == '\r')
					{
						// ignore, just use \n for newlines
						continue;
					}
					else if (rwsz[0] == '&')
					{
						// "&&" means draw a single ampersand, single one is a shortcut character
						if (rwsz[1] == '&')
						{
							// just move on and draw the second ampersand
							rwsz++;
						}
						else
						{
							continue;
						}
					}

					remainingLength += surface()->GetCharacterWidth(font, *rwsz);
				}

				if (x + remainingLength > _drawWidth)
				{
					// looks like we've got an ellipses situation
					m_pwszEllipsesPosition = wsz;
					break;
				}
			}

			x += len;
		}

		// Didn't need ellipses...
		if (!m_pwszEllipsesPosition)
			break;
	}
}

void TextImage::SetWrap(bool bWrap)
{
	m_bWrap = bWrap;
}

void TextImage::SetUseFallbackFont(bool bState, HFont hFallback)
{
	m_bUseFallbackFont = bState;
	_fallbackFont = hFallback;
}

void TextImage::ResizeImageToContentMaxWidth(int nMaxWidth)
{
	_drawWidth = nMaxWidth;
	// Since we might have to use the "fallback" font, go ahead and recalc the ellipses state first to see if that's the case
	// NOTE: I think there may be a race condition lurking here, but this seems to work.
	if (m_bRecalculateTruncation)
	{
		RecalculateEllipsesPosition();
	}

	ResizeImageToContent();
}
