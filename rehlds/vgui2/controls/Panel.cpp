#include <stdio.h>
#include <assert.h>

#include "utlvector.h"

#include <KeyValues.h>
#include <vgui/IBorder.h>
#include <vgui/IInput.h>
#include <vgui/IPanel.h>
#include <vgui/IScheme.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui/ILocalize.h>
#include <vgui/IVGui.h>
#include <vgui/KeyCode.h>

#include <vgui_controls/Panel.h>
#include <vgui_controls/BuildGroup.h>
#include <vgui_controls/Tooltip.h>
#include <vgui_controls/PHandle.h>
#include <vgui_controls/Controls.h>




using namespace vgui;












































class CPanelMessageMapDictionary
{
public:
	CPanelMessageMapDictionary() : m_PanelMessageMapPool(sizeof(PanelMessageMap), 32)
	{
		m_MessageMaps.RemoveAll();
	}

	PanelMessageMap	*FindOrAddPanelMessageMap(char const *className);
	PanelMessageMap	*FindPanelMessageMap(char const *className);

private:
	struct PanelMessageMapDictionaryEntry
	{
		PanelMessageMap *map;
	};

	char const *StripNamespace(char const *className);

	CUtlDict<PanelMessageMapDictionaryEntry, int> m_MessageMaps;
	CMemoryPool m_PanelMessageMapPool;
};




namespace vgui
{

PanelMessageMap *FindOrAddPanelMessageMap( har const *className)
{
	return GetPanelMessageMapDictionary().FindOrAddPanelMessageMap(className);
}

// Find but don't add mapping
PanelMessageMap *FindPanelMessageMap(char const *className)
{
	return GetPanelMessageMapDictionary().FindPanelMessageMap(className);
}


















// Panel handle implementation
// Returns a pointer to a valid panel, NULL if the panel has been deleted
Panel *PHandle::Get()
{
	if (m_iPanelID != INVALID_PANEL)
	{
		VPANEL panel = ivgui()->HandleToPanel(m_iPanelID);
		if (panel)
		{
			Panel *vguiPanel = ipanel()->GetPanel(panel, GetControlsModuleName());
			return vguiPanel;
		}
	}
	return NULL;
}

// sets the smart pointer
Panel *PHandle::Set(Panel *pent)
{
	if (pent)
	{
		m_iPanelID = ivgui()->PanelToHandle(pent->GetVPanel());
	}
	else
	{
		m_iPanelID = INVALID_PANEL;
	}
	return pent;
}

Panel *PHandle::Set(HPanel hPanel)
{
	m_iPanelID = hPanel;
	return Get();
}

// Returns a handle to a valid panel, NULL if the panel has been deleted
VPANEL VPanelHandle::Get()
{
	if (m_iPanelID != INVALID_PANEL)
	{
		return ivgui()->HandleToPanel(m_iPanelID);
	}
	return NULL;
}

// sets the smart pointer
VPANEL VPanelHandle::Set(VPANEL pent)
{
	if (pent)
	{
		m_iPanelID = ivgui()->PanelToHandle(pent);
	}
	else
	{
		m_iPanelID = INVALID_PANEL;
	}
	return pent;
}






} // namespace vgui
