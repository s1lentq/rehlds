#include <vgui/IPanel.h>
#include <vgui/ISurface.h>
#include <vgui/ISystem.h>
#include <vgui/ILocalize.h>
#include <KeyValues.h>

#include <vgui_controls/BuildGroup.h>
#include <vgui_controls/BuildModeDialog.h>
#include <vgui_controls/EditablePanel.h>

// these includes are all for the virtual contruction factory Dialog::CreateControlByName()
#include <vgui_controls/Button.h>
#include <vgui_controls/Label.h>
#include <vgui_controls/CheckButton.h>
#include <vgui_controls/ComboBox.h>
#include <vgui_controls/Menu.h>
#include <vgui_controls/MenuItem.h>
#include <vgui_controls/MessageBox.h>
#include <vgui_controls/ProgressBar.h>
#include <vgui_controls/RadioButton.h>
#include <vgui_controls/ScrollBar.h>
#include <vgui_controls/ToggleButton.h>
#include <vgui_controls/ImagePanel.h>
#include <vgui_controls/AnimatingImagePanel.h>
#include <vgui_controls/Divider.h>
#include <vgui_controls/URLLabel.h>
#include <vgui_controls/RichText.h>
#include <vgui_controls/BitmapImagePanel.h>

using namespace vgui;

// Constructor
#pragma warning(disable:4355)

EditablePanel::EditablePanel(Panel *parent, const char *panelName) : Panel(parent, panelName), m_NavGroup(this)
{
	_buildGroup = new BuildGroup(this, this);
	m_pszConfigName = nullptr;
	m_iConfigID = 0;
	m_pDialogVariables = nullptr;

	GetSize(_baseWide, _baseTall);
}

// Constructor
EditablePanel::EditablePanel(Panel *parent, const char *panelName, HScheme hScheme) : Panel(parent, panelName, hScheme), m_NavGroup(this)
{
	_buildGroup = new BuildGroup(this, this);
	m_pszConfigName = nullptr;
	m_iConfigID = 0;
	m_pDialogVariables = nullptr;

	GetSize(_baseWide, _baseTall);
}

#pragma warning(default:4355)

// Destructor
EditablePanel::~EditablePanel()
{
	delete [] m_pszConfigName;
	delete _buildGroup;

	if (m_pDialogVariables)
	{
		m_pDialogVariables->deleteThis();
	}
}

// Called when a child is added to the panel.
void EditablePanel::OnChildAdded(VPANEL child)
{
	BaseClass::OnChildAdded(child);

	// add only if we're in the same module
	Panel *panel = ipanel()->GetPanel(child, GetModuleName());
	if (panel)
	{
		panel->SetBuildGroup(_buildGroup);
		panel->AddActionSignalTarget(this);
	}
}

void EditablePanel::OnKeyCodeTyped(KeyCode code)
{
	if (code == KEY_ENTER || code == KEY_PAD_ENTER)
	{
		// check for a default button
		VPANEL panel = GetFocusNavGroup().GetCurrentDefaultButton();
		if (panel && ipanel()->IsVisible(panel) && ipanel()->IsEnabled(panel))
		{
			// Activate the button
			PostMessage(panel, new KeyValues("Hotkey"));
		}
		else
		{
			BaseClass::OnKeyCodeTyped(code);
		}
	}
	else
	{
		BaseClass::OnKeyCodeTyped(code);
	}
}

// Callback for when the panel size has been changed
void EditablePanel::OnSizeChanged(int wide, int tall)
{
	BaseClass::OnSizeChanged(wide, tall);
	InvalidateLayout();

	int dx, dy;

	dx = wide - _baseWide;
	dy = tall - _baseTall;

	for (int i = 0; i < GetChildCount(); i++)
	{
		// perform auto-layout on the child panel
		Panel *child = GetChild(i);
		if (!child)
			continue;

		int x, y, w, t;
		child->GetBounds(x, y, w, t);

		PinCorner_e pinCorner = child->GetPinCorner();
		if (pinCorner == PIN_TOPRIGHT || pinCorner == PIN_BOTTOMRIGHT)
		{
			// move along with the right edge
			x += dx;
		}

		if (pinCorner == PIN_BOTTOMLEFT || pinCorner == PIN_BOTTOMRIGHT)
		{
			// move along with the right edge
			y += dy
		}

		AutoResize_e autoResize = child->GetAutoResize();
		if (autoResize == AUTORESIZE_RIGHT || autoResize == AUTORESIZE_DOWNANDRIGHT)
		{
			w += dx;
		}

		if (autoResize == AUTORESIZE_DOWN || autoResize == AUTORESIZE_DOWNANDRIGHT)
		{
			t += dy;
		}

		if (wide < x + w)
		{
			w = wide - x;
			if (w < 0)
				continue;
		}
		else if (tall < y + t)
		{
			t = tall - y;
			if (t < 0)
				continue;
		}

		child->SetBounds(x, y, w, t);
		child->InvalidateLayout();
	}

	Repaint();

	_baseWide = wide;
	_baseTall = tall;
}

void EditablePanel::OnCurrentDefaultButtonSet(Panel *defaultButton)
{
	m_NavGroup.SetCurrentDefaultButton(defaultButton->GetVPanel(), false);

	// forward the message up
	if (GetVParent())
	{
		KeyValues *msg = new KeyValues("CurrentDefaultButtonSet");
		msg->SetPtr("button", defaultButton);
		PostMessage(GetVParent(), msg);
	}
}

void EditablePanel::OnDefaultButtonSet(Panel *defaultButton)
{
	m_NavGroup.SetDefaultButton(defaultButton);
}

void EditablePanel::OnFindDefaultButton()
{
	if (m_NavGroup.GetDefaultButton())
	{
		m_NavGroup.SetCurrentDefaultButton(m_NavGroup.GetDefaultButton());
		return;
	}

	if (GetVParent())
	{
		PostMessage(GetVParent(), new KeyValues("FindDefaultButton"));
	}
}

struct leaf_t
{
	short x, y, wide, tall;
	unsigned char split;		// 0 no split; 1 x-axis, 2 y-axis
	bool filled;			// true if this is already filled
	short splitpos;			// place of split

	leaf_t *left;
	leaf_t *right;
};

leaf_t g_Leaves[256];
int g_iNextLeaf;

inline leaf_t *AllocLeaf()
{
	Assert(g_iNextLeaf < 255);
	return &g_Leaves[g_iNextLeaf++];
}

void AddSolidToTree(leaf_t *leaf, int x, int y, int wide, int tall)
{
	// clip to this leaf
	if (x < leaf->x)
	{
		wide -= (leaf->x - x);
		if (wide < 1)
			return;
		x = leaf->x;
	}
	if (y < leaf->y)
	{
		tall -= (leaf->y - y);
		if (tall < 1)
			return;
		y = leaf->y;
	}
	if (x + wide > leaf->x + leaf->wide)
	{
		wide -= ((x + wide) - (leaf->x + leaf->wide));
		if (wide < 1)
			return;
	}
	if (y + tall > leaf->y + leaf->tall)
	{
		tall -= ((y + tall) - (leaf->y + leaf->tall));
		if (tall < 1)
			return;
	}

	// the rect should now be completely within the leaf
	if (leaf->split == 1)
	{
		// see if it is to the left or the right of the split
		if (x < leaf->splitpos)
		{
			// it's to the left
			AddSolidToTree(leaf->left, x, y, wide, tall);
		}
		else if (x + wide > leaf->splitpos)
		{
			// it's to the right
			AddSolidToTree(leaf->right, x, y, wide, tall);
		}
	}
	else if (leaf->split == 2)
	{
		// check y
		// see if it is to the left (above) or the right (below) of the split
		if (y < leaf->splitpos)
		{
			// it's above
			AddSolidToTree(leaf->left, x, y, wide, tall);
		}
		else if (y + tall > leaf->splitpos)
		{
			// it's below
			AddSolidToTree(leaf->right, x, y, wide, tall);
		}
	}
	else
	{
		// this leaf is unsplit, make the first split against the first edge we find
		if (x > leaf->x)
		{
			// split the left side of the rect
			leaf->split = 1;
			leaf->splitpos = (short)x;

			// create 2 new leaves
			leaf_t *left = AllocLeaf();
			leaf_t *right = AllocLeaf();
			memset(left, 0, sizeof(leaf_t));
			memset(right, 0, sizeof(leaf_t));
			leaf->left = left;
			leaf->right = right;

			left->x = leaf->x;
			left->y = leaf->y;
			left->wide = (short)(leaf->splitpos - leaf->x);
			left->tall = leaf->tall;

			right->x = leaf->splitpos;
			right->y = leaf->y;
			right->wide = (short)(leaf->wide - left->wide);
			right->tall = leaf->tall;

			// split the right leaf by the current rect
			AddSolidToTree(leaf->right, x, y, wide, tall);
		}
		else if (y > leaf->y)
		{
			// split the top edge
			leaf->split = 2;
			leaf->splitpos = (short)y;

			// create 2 new leaves (facing to the east)
			leaf_t *left = AllocLeaf();
			leaf_t *right = AllocLeaf();
			memset(left, 0, sizeof(leaf_t));
			memset(right, 0, sizeof(leaf_t));
			leaf->left = left;
			leaf->right = right;

			left->x = leaf->x;
			left->y = leaf->y;
			left->wide = leaf->wide;
			left->tall = (short)(y - leaf->y);

			right->x = leaf->x;
			right->y = leaf->splitpos;
			right->wide = leaf->wide;
			right->tall = (short)(leaf->tall + leaf->y - right->y);

			// split the right leaf by the current rect
			AddSolidToTree(leaf->right, x, y, wide, tall);
		}
		else if (x + wide < leaf->x + leaf->wide)
		{
			// split the right edge
			leaf->split = 1;
			leaf->splitpos = (short)(x + wide);

			// create 2 new leaves
			leaf_t *left = AllocLeaf();
			leaf_t *right = AllocLeaf();
			memset(left, 0, sizeof(leaf_t));
			memset(right, 0, sizeof(leaf_t));
			leaf->left = left;
			leaf->right = right;

			left->x = leaf->x;
			left->y = leaf->y;
			left->wide = (short)(leaf->splitpos - leaf->x);
			left->tall = leaf->tall;

			right->x = leaf->splitpos;
			right->y = leaf->y;
			right->wide = (short)(leaf->wide - left->wide);
			right->tall = leaf->tall;

			// split the left leaf by the current rect
			AddSolidToTree(leaf->left, x, y, wide, tall);
		}
		else if (y + tall < leaf->y + leaf->tall)
		{
			// split the bottom edge
			leaf->split = 2;
			leaf->splitpos = (short)(y + tall);

			// create 2 new leaves (facing to the east)
			leaf_t *left = AllocLeaf();
			leaf_t *right = AllocLeaf();
			memset(left, 0, sizeof(leaf_t));
			memset(right, 0, sizeof(leaf_t));
			leaf->left = left;
			leaf->right = right;

			left->x = leaf->x;
			left->y = leaf->y;
			left->wide = leaf->wide;
			left->tall = (short)(leaf->splitpos - leaf->y);

			right->x = leaf->x;
			right->y = leaf->splitpos;
			right->wide = leaf->wide;
			right->tall = (short)(leaf->tall - left->tall);

			// split the left leaf by the current rect
			AddSolidToTree(leaf->left, x, y, wide, tall);
		}
		else
		{
			// this is the exact same rect! don't draw this leaf
			leaf->filled = true;
			return;
		}
	}
}

// Fills the panel background, clipping if possible
void EditablePanel::PaintBackground()
{
	BaseClass::PaintBackground();
	return;

/*
	test code, using a screenspace bsp tree to reduce overdraw in vgui
	not yet fully functional

//	test: fill background with obnoxious color to show holes
//	surface()->DrawSetColor(Color(255, 0, 0, 255));
//	surface()->DrawFilledRect(0, 0, GetWide(), GetTall());
//	return;

	// reset the leaf memory
	g_iNextLeaf = 0;

	leaf_t *headNode = AllocLeaf();
	memset(headNode, 0, sizeof(leaf_t));

	headNode->wide = (short)GetWide();
	headNode->tall = (short)GetTall();

	// split the leaf by the first child
	for (int i = 0; i < GetChildCount(); i++)
	{
		Panel *child = GetChild(i);
		if (child->IsOpaque())
		{
			int x, y, wide, tall;
			child->GetBounds(x, y, wide, tall);

			// ignore small children
			if (wide + tall < 100)
				continue;

			AddSolidToTree(headNode, x, y, wide, tall);
		}
	}

	// walk the built tree, painting the background
	Color col = GetBgColor();
	surface()->DrawSetColor(col);
	for (i = 0; i < g_iNextLeaf; i++)
	{
		leaf_t *leaf = g_Leaves + i;
		if (leaf->splitpos || leaf->filled)
			continue;
		surface()->DrawFilledRect(leaf->x, leaf->y, leaf->x + leaf->wide, leaf->y + leaf->tall);
	}
*/
}

// Activates the build mode dialog for editing panels.
void EditablePanel::ActivateBuildMode()
{
	_buildGroup->SetEnabled(true);
}

// Loads panel settings from a resource file.
void EditablePanel::LoadControlSettings(const char *resourceName, const char *pathID)
{
	_buildGroup->LoadControlSettings(resourceName, pathID);
	ForceSubPanelsToUpdateWithNewDialogVariables();
	InvalidateLayout();
}

// registers a file in the list of control settings, so the vgui dialog can choose between them to edit
void EditablePanel::RegisterControlSettingsFile(const char *resourceName, const char *pathID)
{
	_buildGroup->RegisterControlSettingsFile(resourceName, pathID);
}

// sets the name of this dialog so it can be saved in the user config area
void EditablePanel::LoadUserConfig(const char *configName, int dialogID)
{
	KeyValues *data = system()->GetUserConfigFileData(configName, dialogID);

	delete [] m_pszConfigName;
	int len = Q_strlen(configName) + 1;
	m_pszConfigName = new char[ len ];
	Q_strncpy(m_pszConfigName, configName, len);
	m_pszConfigName[len] = '\0';
	m_iConfigID = dialogID;

	// apply our user config settings (this will recurse through our children)
	if (data)
	{
		ApplyUserConfigSettings(data);
	}
}

// saves all the settings to the document
void EditablePanel::SaveUserConfig()
{
	if (m_pszConfigName)
	{
		KeyValues *data = system()->GetUserConfigFileData(m_pszConfigName, m_iConfigID);

		// get our user config settings (this will recurse through our children)
		if (data)
		{
			GetUserConfigSettings(data);
		}
	}
}

// combines both of the above, LoadControlSettings & LoadUserConfig
void EditablePanel::LoadControlSettingsAndUserConfig(const char *dialogResourceName, int dialogID)
{
	LoadControlSettings(dialogResourceName);
	LoadUserConfig(dialogResourceName, dialogID);
}

// applies the user config settings to all the children
void EditablePanel::ApplyUserConfigSettings(KeyValues *userConfig)
{
	for (int i = 0; i < GetChildCount(); i++)
	{
		Panel *child = GetChild(i);
		if (child->HasUserConfigSettings())
		{
			const char *name = child->GetName();
			if (name && name[0])
			{
				child->ApplyUserConfigSettings(userConfig->FindKey(name, true));
			}
		}
	}
}

// gets all the children's user config settings
void EditablePanel::GetUserConfigSettings(KeyValues *userConfig)
{
	for (int i = 0; i < GetChildCount(); i++)
	{
		Panel *child = GetChild(i);
		if (child->HasUserConfigSettings())
		{
			const char *name = child->GetName();
			if (name && *name)
			{
				child->GetUserConfigSettings(userConfig->FindKey(name, true));
			}
		}
	}
}

// Save user config settings
void EditablePanel::OnClose()
{
	SaveUserConfig();
}

// Handle information requests
bool EditablePanel::RequestInfo(KeyValues *data)
{
	if (!Q_stricmp(data->GetName(), "BuildDialog"))
	{
		// a build dialog is being requested, give it one
		// a bit hacky, but this is a case where vgui.dll needs to reach out
		data->SetPtr("PanelPtr", new BuildModeDialog((BuildGroup *)data->GetPtr("BuildGroupPtr")));
		return true;
	}
	else if (!Q_stricmp(data->GetName(), "ControlFactory"))
	{
		Panel *newPanel = CreateControlByName(data->GetString("ControlName"));
		if (newPanel)
		{
			data->SetPtr("PanelPtr", newPanel);
			return true;
		}
	}
	return BaseClass::RequestInfo(data);
}

// Return the buildgroup that this panel is part of.
BuildGroup *EditablePanel::GetBuildGroup()
{
	return _buildGroup;
}

// Return a pointer to the nav group
FocusNavGroup &EditablePanel::GetFocusNavGroup()
{
	return m_NavGroup;
}

bool EditablePanel::RequestFocusNext(VPANEL panel)
{
	return m_NavGroup.RequestFocusNext(panel);
}

bool EditablePanel::RequestFocusPrev(VPANEL panel)
{
	return m_NavGroup.RequestFocusPrev(panel);
}

// Delegates focus to a sub panel
// Input  : direction - the direction in which focus travelled to arrive at this panel; forward = 1, back = -1
void EditablePanel::RequestFocus(int direction)
{
	// we must be a sub panel for this to be called
	// delegate focus
	if (direction == 1)
	{
		RequestFocusNext(NULL_PANEL);
	}
	else if (direction == -1)
	{
		RequestFocusPrev(NULL_PANEL);
	}
	else
	{
		BaseClass::RequestFocus();
	}
}

// Pass the focus down onto the last used panel
void EditablePanel::OnSetFocus()
{
	Panel *focus = m_NavGroup.GetCurrentFocus();
	if (focus && focus != this)
	{
		focus->RequestFocus();
	}
	else
	{
		focus = m_NavGroup.GetDefaultPanel();
		if (focus)
		{
			focus->RequestFocus();
			focus->OnSetFocus();
		}
	}

	BaseClass::OnSetFocus();
}

// Called when the resource file is loaded to set up the panel state
void EditablePanel::ApplySettings(KeyValues *inResourceData)
{
	Panel::ApplySettings(inResourceData);
	_buildGroup->ApplySettings(inResourceData);

	GetSize(_baseWide, _baseTall);
}

// Update focus info for navigation
void EditablePanel::OnRequestFocus(VPANEL subFocus, VPANEL defaultPanel)
{
	if (!ipanel()->IsPopup(subFocus))
	{
		defaultPanel = m_NavGroup.SetCurrentFocus(subFocus, defaultPanel);
	}

	BaseClass::OnRequestFocus(GetVPanel(), defaultPanel);
}

// Get the panel that currently has keyfocus
VPANEL EditablePanel::GetCurrentKeyFocus()
{
	Panel *focus = m_NavGroup.GetCurrentFocus();
	if (focus == this)
		return NULL_PANEL;

	if (focus)
	{
		if (focus->IsPopup())
			return BaseClass::GetCurrentKeyFocus();

		// chain down the editpanel hierarchy
		VPANEL subFocus = focus->GetCurrentKeyFocus();
		if (subFocus)
			return subFocus;

		// hit a leaf panel, return that
		return focus->GetVPanel();
	}

	return BaseClass::GetCurrentKeyFocus();
}

// Gets the panel with the specified hotkey
Panel *EditablePanel::HasHotkey(wchar_t key)
{
	// not visible, so can't respond to a hot key
	if (!IsVisible() || !IsEnabled())
	{
		return nullptr;
	}

	for (int i = 0; i < GetChildCount(); i++)
	{
		Panel *hot = GetChild(i)->HasHotkey(key);
		if (hot && hot->IsVisible() && hot->IsEnabled())
		{
			return hot;
		}
	}

	return nullptr;
}

// Shortcut function to setting enabled state of control
void EditablePanel::SetControlEnabled(const char *controlName, bool enabled)
{
	Panel *control = FindChildByName(controlName);
	if (control)
	{
		control->SetEnabled(enabled);
	}
}

// Shortcut function to setting visibility state of control
void EditablePanel::SetControlVisible(const char *controlName, bool visible)
{
	Panel *control = FindChildByName(controlName);
	if (control)
	{
		control->SetVisible(visible);
	}
}

// Shortcut function to set data in child controls
void EditablePanel::SetControlString(const char *controlName, const char *string)
{
	Panel *control = FindChildByName(controlName);
	if (control)
	{
		PostMessage(control, new KeyValues("SetText", "text", string));
	}
}

// Shortcut function to set data in child controls
void EditablePanel::SetControlInt(const char *controlName, int state)
{
	Panel *control = FindChildByName(controlName);
	if (control)
	{
		PostMessage(control, new KeyValues("SetState", "state", state));
	}
}

// Shortcut function to get data in child controls
int EditablePanel::GetControlInt(const char *controlName, int defaultState)
{
	Panel *control = FindChildByName(controlName);
	if (control)
	{
		KeyValues *data = new KeyValues("GetState");

		if (control->RequestInfo(data))
		{
			int state = data->GetInt("state", defaultState);
			data->deleteThis();
			return state;
		}
	}
	return defaultState;
}

// Shortcut function to get data in child controls
const char *EditablePanel::GetControlString(const char *controlName, const char *defaultString)
{
	static char buf[512];
	GetControlString(controlName, buf, sizeof(buf) - 1, defaultString);
	return buf;
}

// Shortcut function to get data in child controls
void EditablePanel::GetControlString(const char *controlName, char *buf, int bufSize, const char *defaultString)
{
	Panel *control = FindChildByName(controlName);
	KeyValues *data = new KeyValues("GetText");
	if (control && control->RequestInfo(data))
	{
		Q_strncpy(buf, data->GetString("text", defaultString), bufSize - 1);
	}
	else
	{
		// no value found, copy in default text
		Q_strncpy(buf, defaultString, bufSize - 1);
	}

	// ensure null termination of string
	buf[bufSize - 1] = '\0';

	// free
	data->deleteThis();
}

// localization variables (used in constructing UI strings)
void EditablePanel::SetDialogVariable(const char *varName, const char *value)
{
	GetDialogVariables()->SetString(varName, value);
	ForceSubPanelsToUpdateWithNewDialogVariables();
}

// localization variables (used in constructing UI strings)
void EditablePanel::SetDialogVariable(const char *varName, const wchar_t *value)
{
	GetDialogVariables()->SetWString(varName, value);
	ForceSubPanelsToUpdateWithNewDialogVariables();
}

// localization variables (used in constructing UI strings)
void EditablePanel::SetDialogVariable(const char *varName, int value)
{
	GetDialogVariables()->SetInt(varName, value);
	ForceSubPanelsToUpdateWithNewDialogVariables();
}

// localization variables (used in constructing UI strings)
void EditablePanel::SetDialogVariable(const char *varName, float value)
{
	GetDialogVariables()->SetFloat(varName, value);
	ForceSubPanelsToUpdateWithNewDialogVariables();
}

// redraws child panels with new localization vars
void EditablePanel::ForceSubPanelsToUpdateWithNewDialogVariables()
{
	if (m_pDialogVariables)
	{
		ipanel()->SendMessage(GetVPanel(), m_pDialogVariables, GetVPanel());
		for (int i = 0; i < ipanel()->GetChildCount(GetVPanel()); i++)
		{
			ipanel()->SendMessage(ipanel()->GetChild(GetVPanel(), i), m_pDialogVariables, GetVPanel());
		}
	}
}

// lazy creation of localization vars object
KeyValues *EditablePanel::GetDialogVariables()
{
	if (m_pDialogVariables)
		return m_pDialogVariables;

	m_pDialogVariables = new KeyValues("DialogVariables");
	return m_pDialogVariables;
}

// Virtual factory for control creation
Panel *EditablePanel::CreateControlByName(const char *controlName)
{
	if (!Q_stricmp(controlName, "Label"))
	{
		return new Label(NULL, NULL, "Label");
	}
	else if (Q_stricmp(controlName, "Button"))
	{
		return new Button(NULL, NULL, "Button");
	}
	else if (Q_stricmp(controlName, "CheckButton"))
	{
		return new CheckButton(NULL, NULL, "CheckButton");
	}
	else if (Q_stricmp(controlName, "ComboBox"))
	{
		return new ComboBox(NULL, NULL, 5, true);
	}
	else if (Q_stricmp(controlName, "Menu"))
	{
		return new Menu(NULL, "Menu");
	}
	else if(Q_stricmp(controlName, "MenuItem"))
	{
		return new MenuItem(NULL, NULL, "MenuItem");
	}
	else if(Q_stricmp(controlName, "MessageBox"))
	{
		return new MessageBox("MessageBox", "MessageBoxText");
	}
	else if(Q_stricmp(controlName, "ProgressBar"))
	{
		return new ProgressBar(NULL, NULL);
	}
	else if(Q_stricmp(controlName, "RadioButton"))
	{
		return new RadioButton(0, 0, "RadioButton");
	}
	else if(Q_stricmp(controlName, "ScrollBar"))
	{
		return new ScrollBar(NULL, NULL, false);
	}
	else if(Q_stricmp(controlName, "TextEntry"))
	{
		return new TextEntry(NULL, NULL);
	}
	else if(Q_stricmp(controlName, "RichText"))
	{
		return new RichText(NULL, NULL, true);
	}
	else if(Q_stricmp(controlName, "ToggleButton"))
	{
		return new ToggleButton(NULL, NULL, "ToggleButton");
	}
	else if(Q_stricmp(controlName, "ImagePanel"))
	{
		return new ImagePanel(NULL, "ResourceImagePanel");
	}
	else if(Q_stricmp(controlName, "AnimatingImagePanel"))
	{
		return new AnimatingImagePanel(NULL, NULL);
	}
	else if(Q_stricmp(controlName, "Panel"))
	{
		return new Panel(NULL, NULL);
	}
	else if(Q_stricmp(controlName, "Divider"))
	{
		return new Divider(NULL, "Divider");
	}
	else if(Q_stricmp(controlName, "URLLabel"))
	{
		return new URLLabel(NULL, NULL, "URLLabel", NULL);
	}
	else if(Q_stricmp(controlName, "EditablePanel"))
	{
		return new EditablePanel(NULL, NULL);
	}
	else if(!Q_stricmp(controlName, "BitmapImagePanel"))
	{
		return new CBitmapImagePanel(NULL, "BitmapImagePanel", NULL);
	}

	return NULL;
}
