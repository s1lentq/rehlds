#include "vgui/Cursor.h"
#include "vgui/IInput.h"
#include "vgui/ILocalize.h"
#include "vgui/IScheme.h"
#include "vgui/ISurface.h"
#include "vgui/IPanel.h"
#include "KeyValues.h"

#include "vgui_controls/Button.h"
#include "vgui_controls/ComboBox.h"
#include "vgui_controls/Menu.h"
#include "vgui_controls/MenuItem.h"
#include "vgui_controls/TextImage.h"

#include <ctype.h>

using namespace vgui;

namespace vgui
{
// Scroll bar button
class ComboBoxButton: public Button
{
public:
	ComboBoxButton(ComboBox *parent, const char *panelName, const char *text);
	virtual void ApplySchemeSettings(IScheme *pScheme);
	virtual IBorder *GetBorder(bool depressed, bool armed, bool selected, bool keyfocus);
	virtual void OnCursorExited();

	virtual Color GetButtonBgColor()
	{
		if (IsEnabled())
			return Button::GetButtonBgColor();

		return m_DisabledBgColor;
	}

private:
	Color m_DisabledBgColor;
};

ComboBoxButton::ComboBoxButton(ComboBox *parent, const char *panelName, const char *text) : Button(parent, panelName, text)
{
	SetButtonActivationType(ACTIVATE_ONPRESSED);
	m_pImage = vgui::scheme()->GetImage(text);
}

void ComboBoxButton::ApplySchemeSettings(IScheme *pScheme)
{
	Button::ApplySchemeSettings(pScheme);

	SetDefaultBorder(pScheme->GetBorder("ScrollBarButtonBorder"));

	// arrow changes color but the background doesnt.
	SetArmedColor(GetSchemeColor("MenuButton/ArmedArrowColor", pScheme), GetSchemeColor("MenuButton/ButtonBgColor", pScheme));
	SetDepressedColor(GetSchemeColor("MenuButton/ArmedArrowColor", pScheme), GetSchemeColor("MenuButton/ButtonBgColor", pScheme));
	SetDefaultColor(GetSchemeColor("MenuButton/ButtonArrowColor", pScheme), GetSchemeColor("MenuButton/ButtonBgColor", pScheme));
	m_DisabledBgColor = GetSchemeColor("ControlBG", pScheme);

	if (m_pImage)
		AddImage(m_pImage);
}

IBorder *ComboBoxButton::GetBorder(bool depressed, bool armed, bool selected, bool keyfocus)
{
	return NULL;
	// return Button::GetBorder(depressed, armed, selected, keyfocus);
}

// Dim the arrow on the button when exiting the box only if the menu is closed, so let the parent handle this.
void ComboBoxButton::OnCursorExited()
{
	// want the arrow to go grey when we exit the box if the menu is not open
	CallParentFunction(new KeyValues("CursorExited"));
}

} // namespace vgui


// Constructor
// parent - parent class
// panelName
// numLines - number of lines in dropdown menu
// allowEdit - whether combobox is editable or not
ComboBox::ComboBox(Panel *parent, const char *panelName, int numLines, bool allowEdit) : TextEntry(parent, panelName)
{
	SetEditable(allowEdit);
	SetHorizontalScrolling(false); // do not scroll, always Start at the beginning of the text.

	// create the drop-down menu
	m_pDropDown = new Menu(this, NULL);
	m_pDropDown->AddActionSignalTarget(this);

	// button to Activate menu
	m_pButton = new ComboBoxButton(this, NULL, "resource/icon_down");
	m_pButton->SetCommand("ButtonClicked");
	m_pButton->AddActionSignalTarget(this);

	SetNumberOfEditLines(numLines);

	m_bHighlight = false;
	m_iDirection = Menu::DOWN;
	m_iOpenOffsetY = 0;
}

// Destructor
ComboBox::~ComboBox()
{
	m_pDropDown->DeletePanel();
	m_pButton->DeletePanel();
}

// Set the number of items in the dropdown menu.
// numLines - number of items in dropdown menu
void ComboBox::SetNumberOfEditLines(int numLines)
{
	m_pDropDown->SetNumberOfVisibleItems(numLines);
}

// Add an item to the drop down
// itemText - name of dropdown menu item
int ComboBox::AddItem(const char *itemText, const KeyValues *userData)
{
	// when the menu item is selected it will send the custom message "SetText"
	return m_pDropDown->AddMenuItem(itemText, new KeyValues("SetText", "text", itemText), this, userData);
}

// Add an item to the drop down
// itemText - name of dropdown menu item
int ComboBox::AddItem(const wchar_t *itemText, const KeyValues *userData)
{
	// TODO: check me, how much we need to allocate
	int allocLen = ((wcslen(itemText) + 1) * sizeof(wchar_t) * 2);
	char *ansi = (char *)alloca(allocLen);
	if (!ansi)
	{
		return m_pDropDown->GetInvalidMenuID();
	}

	// get an ansi version for the menuitem name
	vgui::localize()->ConvertUnicodeToANSI(itemText, ansi, allocLen + 1);
	ansi[ allocLen + 1 ] = '\0';

	// add the element to the menu
	// when the menu item is selected it will send the custom message "SetText"
	KeyValues *kv = new KeyValues("SetText", "text", itemText);

	return m_pDropDown->AddMenuItem(ansi, kv, this, userData);
}

// Updates a current item to the drop down
// itemText - name of dropdown menu item
bool ComboBox::UpdateItem(int itemID, const char *itemText, const KeyValues *userData)
{
	if (!m_pDropDown->IsValidMenuID(itemID))
		return false;

	// when the menu item is selected it will send the custom message "SetText"
	m_pDropDown->UpdateMenuItem(itemID, itemText, new KeyValues("SetText", "text", itemText), userData);
	InvalidateLayout();
	return true;
}

// Updates a current item to the drop down
// itemText - name of dropdown menu item
bool ComboBox::UpdateItem(int itemID, const wchar_t *itemText, const KeyValues *userData)
{
	if (!m_pDropDown->IsValidMenuID(itemID))
		return false;

	int allocLen = ((wcslen(itemText) + 1) * sizeof(wchar_t) * 2);
	char *text = (char *)alloca(allocLen);
	if (!text)
	{
		return false;
	}

	// get an text version for the menuitem name
	vgui::localize()->ConvertUnicodeToANSI(itemText, text, allocLen + 1);
	text[ allocLen + 1 ] = '\0';

	// add the element to the menu
	// when the menu item is selected it will send the custom message "SetText"
	KeyValues *kv = new KeyValues("SetText", "text", text);
	m_pDropDown->UpdateMenuItem(itemID, itemText, kv, userData);
	InvalidateLayout();
	return true;
}

// Updates a current item to the drop down
// itemText - name of dropdown menu item
bool ComboBox::IsItemIDValid(int itemID)
{
	return m_pDropDown->IsValidMenuID(itemID);
}

void ComboBox::SetItemEnabled(const char *itemText, bool state)
{
	m_pDropDown->SetItemEnabled(itemText, state);
}

void ComboBox::SetItemEnabled(int itemID, bool state)
{
	m_pDropDown->SetItemEnabled(itemID, state);
}

// Remove all items from the drop down menu
void ComboBox::DeleteAllItems()
{
	m_pDropDown->DeleteAllItems();
}

int ComboBox::GetItemCount()
{
	return m_pDropDown->GetItemCount();
}

int ComboBox::GetItemIDFromRow(int row)
{
	// valid from [0, GetItemCount)
	return m_pDropDown->GetMenuID(row);
}

// Activate the item in the menu list, as if that menu item had been selected by the user
// itemID - itemID from AddItem in list of dropdown items
void ComboBox::ActivateItem(int itemID)
{
	m_pDropDown->ActivateItem(itemID);
}

// Activate the item in the menu list, as if that menu item had been selected by the user
// itemID - itemID from AddItem in list of dropdown items
void ComboBox::ActivateItemByRow(int row)
{
	m_pDropDown->ActivateItemByRow(row);
}

// Allows a custom menu to be used with the combo box
void ComboBox::SetMenu(Menu *menu)
{
	if (m_pDropDown)
	{
		m_pDropDown->MarkForDeletion();
	}

	m_pDropDown = menu;
}

// Layout the format of the combo box for drawing on screen
void ComboBox::PerformLayout()
{
	int wide, tall;
	GetPaintSize(wide, tall);

	BaseClass::PerformLayout();

	const int buttonY = 2;
	const int buttonSize = 16;
	const int buttonTall = tall - 2;

	m_pButton->SetBounds(wide - buttonSize - 4, buttonY, buttonSize, buttonTall);

	if (IsEditable())
	{
		SetCursor(dc_ibeam);
	}
	else
	{
		SetCursor(dc_arrow);
	}

	m_pButton->SetEnabled(IsEnabled());
	DoMenuLayout();
}

void ComboBox::DoMenuLayout()
{
	int x, y;
	int wide, tall;
	int mwide, mtall;
	int bwide, btall;

	GetPaintSize(wide, tall);

	if (m_iDirection == CURSOR)
	{
		// force the menu to appear where the mouse button was pressed
		vgui::input()->GetCursorPos(&x, &y);
	}
	else if (m_iDirection == ALIGN_WITH_PARENT && GetVParent())
	{
		x = 0;
		y = tall;

		ParentLocalToScreen(&x, &y);

		x -= 1; // take border into account
		y += m_iOpenOffsetY;
	}
	else
	{
		x = 0;
		y = tall;
		LocalToScreen(&x, &y);
	}

	m_pDropDown->GetSize(&mwide, &mtall);
	GetSize(&bwide, &btall);

	switch (m_iDirection)
	{
	// Menu prefers to open upward
	case Menu::UP:
	{
		y -= mtall + btall;
		y -= 1;

		m_pDropDown->SetPos(x, y);
		break;
	}
	case Menu::DOWN:
	{
		y += 1;
		m_pDropDown->SetPos(x, y);
		break;
	}
	default:
	{
		m_pDropDown->SetPos(x + 1, y + 1);
		break;
	}
	}

	// reset the width of the drop down menu to be the width of the combo box
	m_pDropDown->SetFixedWidth(GetWide());
}

// Sorts the items in the list
void ComboBox::SortItems()
{
}

// return the index of the last selected item
int ComboBox::GetActiveItem()
{
	return m_pDropDown->GetActiveItem();
}

KeyValues *ComboBox::GetActiveItemUserData()
{
	return m_pDropDown->GetItemUserData(GetActiveItem());
}

KeyValues *ComboBox::GetItemUserData(int itemID)
{
	return m_pDropDown->GetItemUserData(itemID);
}

// Output : Returns true on success, false on failure.
bool ComboBox::IsDropdownVisible()
{
	return m_pDropDown->IsVisible();
}

void ComboBox::ApplySchemeSettings(IScheme *pScheme)
{
	BaseClass::ApplySchemeSettings(pScheme);
	SetBorder(pScheme->GetBorder("ComboBoxBorder"));
}

// Set the visiblity of the drop down menu button.
void ComboBox::SetDropdownButtonVisible(bool state)
{
	m_pButton->SetVisible(state);
}

// overloads TextEntry MousePressed
void ComboBox::OnMousePressed(MouseCode code)
{
	if (!m_pDropDown)
		return;

	if (!IsEnabled())
		return;

	// make sure it's getting pressed over us (it may not be due to mouse capture)
	if (!IsCursorOver())
	{
		HideMenu();
		return;
	}

	if (IsEditable())
	{
		BaseClass::OnMousePressed(code);
		HideMenu();
	}
	else
	{
		// clicking on a non-editable text box just activates the drop down menu
		RequestFocus();
		DoClick();
	}
}

// Double-click acts the same as a single-click
void ComboBox::OnMouseDoublePressed(MouseCode code)
{
	if (IsEditable())
	{
		BaseClass::OnMouseDoublePressed(code);
	}
	else
	{
		OnMousePressed(code);
	}
}

// Called when a command is received from the menu
// Changes the label text to be that of the command
void ComboBox::OnCommand(const char *command)
{
	if (!stricmp(command, "ButtonClicked"))
	{
		// hide / show the menu underneath
		DoClick();
	}

	Panel::OnCommand(command);
}

void ComboBox::OnSetText(const wchar_t *newtext)
{
	// see if the combobox text has changed, and if so, post a message detailing the new text
	const wchar_t *text = newtext;

	// check if the new text is a localized string, if so undo it
	if (*text == '#')
	{
		char cbuf[255];
		vgui::localize()->ConvertUnicodeToANSI(text, cbuf, sizeof(cbuf));

		// try lookup in localization tables
		StringIndex_t unlocalizedTextSymbol = vgui::localize()->FindIndex(cbuf + 1);

		if (unlocalizedTextSymbol != INVALID_STRING_INDEX)
		{
			// we have a new text value
			text = vgui::localize()->GetValueByIndex(unlocalizedTextSymbol);
		}
	}

	wchar_t wbuf[255];
	GetText(wbuf, 254);

	if (wcscmp(wbuf, text))
	{
		// text has changed
		SetText(text);

		// fire off that things have changed
		PostActionSignal(new KeyValues("TextChanged", "text", text));
		Repaint();
	}

	// close the box
	HideMenu();
}

// Hides the menu
void ComboBox::HideMenu()
{
	if (!m_pDropDown)
		return;

	// hide the menu
	m_pDropDown->SetVisible(false);
	Repaint();
	OnHideMenu(m_pDropDown);
}

// Shows the menu
void ComboBox::ShowMenu()
{
	if (!m_pDropDown)
		return;

	// hide the menu
	m_pDropDown->SetVisible(false);
	DoClick();
}

// Called when the window loses focus; hides the menu
void ComboBox::OnKillFocus()
{
	SelectNoText();
}

// Called when the menu is closed
void ComboBox::OnMenuClose()
{
	HideMenu();

	if (HasFocus())
	{
		SelectAllText(false);
	}
	else if (m_bHighlight)
	{
		m_bHighlight = false;

		// we want the text to be highlighted when we request the focus
		// SelectAllOnFirstFocus(true);
		RequestFocus();
	}
	// if cursor is in this box or the arrow box
	// make sure it's getting pressed over us (it may not be due to mouse capture)
	else if (IsCursorOver())
	{
		SelectAllText(false);
		OnCursorEntered();

		// Get focus so the box will unhighlight if we click somewhere else.
		RequestFocus();
	}
	else
	{
		m_pButton->SetArmed(false);
	}
}

// Handles hotkey accesses
// FIXME: make this open different directions as necessary see menubutton.
void ComboBox::DoClick()
{
	// menu is already visible, hide the menu
	if (m_pDropDown->IsVisible())
	{
		HideMenu();
		return;
	}

	// do nothing if menu is not enabled
	if (!m_pDropDown->IsEnabled())
	{
		return;
	}

	// force the menu to Think
	m_pDropDown->PerformLayout();

	// make sure we're at the top of the draw order (and therefore our children as well)
	// RequestFocus();

	// We want the item that is shown in the combo box to show as selected
	int itemToSelect = -1;
	int i;
	wchar_t comboBoxContents[255];
	GetText(comboBoxContents, 255);

	for (i = 0 ; i < m_pDropDown->GetItemCount() ; i++)
	{
		wchar_t menuItemName[255];
		int menuID = m_pDropDown->GetMenuID(i);
		m_pDropDown->GetMenuItem(menuID)->GetText(menuItemName, 255);

		if (!wcscmp(menuItemName, comboBoxContents))
		{
			itemToSelect = i;
			break;
		}
	}

	// if we found a match, highlight it on opening the menu
	if (itemToSelect >= 0)
	{
		m_pDropDown->SetCurrentlyHighlightedItem(m_pDropDown->GetMenuID(itemToSelect));
	}

	// reset the dropdown's position
	DoMenuLayout();

	// make sure we're at the top of the draw order (and therefore our children as well)
	// this important to make sure the menu will be drawn in the foreground
	MoveToFront();

	// notify
	OnShowMenu(m_pDropDown);

	// show the menu
	m_pDropDown->SetVisible(true);

	// bring to focus
	m_pDropDown->RequestFocus();

	// no text is highlighted when the menu is opened
	SelectNoText();

	// highlight the arrow while menu is open
	m_pButton->SetArmed(true);

	Repaint();
}

// Brighten the arrow on the button when entering the box
void ComboBox::OnCursorEntered()
{
	// want the arrow to go white when we enter the box
	m_pButton->OnCursorEntered();
	TextEntry::OnCursorEntered();
}

// Dim the arrow on the button when exiting the box
void ComboBox::OnCursorExited()
{
	// want the arrow to go grey when we exit the box if the menu is not open
	if (!m_pDropDown->IsVisible())
	{
		m_pButton->SetArmed(false);
		TextEntry::OnCursorExited();
	}
}

void ComboBox::OnMenuItemSelected()
{
	m_bHighlight = true;

#if 0
	// For editable cbs, fill in the text field from whatever is chosen from the dropdown...
	if (m_bAllowEdit)
	{
		int idx = GetActiveItem();
		if (idx >= 0)
		{
			wchar_t name[ 256 ];
			GetItemText(idx, name, sizeof(name));

			OnSetText(name);
		}
	}
#endif

	Repaint();
}

void ComboBox::OnSizeChanged(int wide, int tall)
{
	BaseClass::OnSizeChanged(wide, tall);

	PerformLayout();

	// set the drawwidth.
	int bwide, btall;
	m_pButton->GetSize(bwide, btall);
	SetDrawWidth(wide - bwide);
}

void ComboBox::OnSetFocus()
{
	BaseClass::OnSetFocus();

	GotoTextEnd();
	SelectAllText(false);
}

// Handles up/down arrows
void ComboBox::OnKeyCodeTyped(KeyCode code)
{
	switch (code)
	{
	case KEY_UP:
	{
		MoveAlongMenuItemList(-1);
		break;
	}
	case KEY_DOWN:
	{
		MoveAlongMenuItemList(1);
		break;
	}
	default:
	{
		BaseClass::OnKeyCodeTyped(code);
		break;
	}
	}
}

// handles key input
void ComboBox::OnKeyTyped(wchar_t unichar)
{
	// don't play with key presses in edit mode
	if (IsEditable())
	{
		BaseClass::OnKeyTyped(unichar);
		return;
	}

	int itemToSelect = m_pDropDown->GetActiveItem();
	if (itemToSelect < 0)
	{
		itemToSelect = 0;
	}

	int i;
	wchar_t menuItemName[255];

	i = itemToSelect + 1;
	if (i >= m_pDropDown->GetItemCount())
	{
		i = 0;
	}

	while (i != itemToSelect)
	{
		int menuID = m_pDropDown->GetMenuID(i);
		m_pDropDown->GetMenuItem(menuID)->GetText(menuItemName, 254);

		if (towlower(menuItemName[0]) == towlower(unichar))
		{
			itemToSelect = i;
			break;
		}

		i++;
		if (i >= m_pDropDown->GetItemCount())
		{
			i = 0;
		}
	}

	if (itemToSelect >= 0 && itemToSelect < m_pDropDown->GetItemCount())
	{
		int menuID = m_pDropDown->GetMenuID(itemToSelect);
		m_pDropDown->GetMenuItem(menuID)->GetText(menuItemName, 255);
		OnSetText(menuItemName);
		SelectAllText(false);
		m_pDropDown->ActivateItem(itemToSelect);
	}
	else
	{
		BaseClass::OnKeyTyped(unichar);
	}
}

void ComboBox::MoveAlongMenuItemList(int direction)
{
	// We want the item that is shown in the combo box to show as selected
	int itemToSelect = -1;
	wchar_t menuItemName[255];
	int i;

	wchar_t comboBoxContents[255];
	GetText(comboBoxContents, 254);
	for (i = 0 ; i < m_pDropDown->GetItemCount() ; i++)
	{
		int menuID = m_pDropDown->GetMenuID(i);
		m_pDropDown->GetMenuItem(menuID)->GetText(menuItemName, 254);

		if (!wcscmp(menuItemName, comboBoxContents))
		{
			itemToSelect = i;
			break;
		}
	}
	// if we found this item, then we scroll up or down
	if (itemToSelect >= 0)
	{
		int newItem = itemToSelect + direction;
		if (newItem < 0)
		{
			newItem = 0;
		}
		else if (newItem >= m_pDropDown->GetItemCount())
		{
			newItem = m_pDropDown->GetItemCount() - 1;
		}

		int menuID = m_pDropDown->GetMenuID(newItem);
		m_pDropDown->GetMenuItem(menuID)->GetText(menuItemName, 255);
		OnSetText(menuItemName);
		SelectAllText(false);
		m_pDropDown->ActivateItem(newItem);
	}
}

// Sets the direction from the menu button the menu should open
void ComboBox::SetOpenDirection(Menu::MenuDirection_e direction)
{
	m_iDirection = direction;
}

void ComboBox::SetUseFallbackFont(bool bState, HFont hFallback)
{
	BaseClass::SetUseFallbackFont(bState, hFallback);
	m_pDropDown->SetUseFallbackFont(bState, hFallback);
}
