#include "ikeyvalues.h"

namespace vgui2
{

class IKeyValues;
class IInput;
class ISchemeManager;
class ISurface;
class ISystem;
class IVGui;
class IPanel;
class IFileSystem;
class ILocalize;

IKeyValues *g_pKeyValuesInterface = nullptr;
IInput *g_pInputInterface = nullptr;
ISchemeManager *g_pSchemeInterface = nullptr;
ISurface *g_pSurfaceInterface = nullptr;
ISystem *g_pSystemInterface = nullptr;
IVGui *g_pVGuiInterface = nullptr;
IPanel *g_pPanelInterface = nullptr;
IFileSystem *g_pFileSystemInterface = nullptr;
ILocalize *g_pLocalizeInterface = nullptr;

static char g_szControlsModuleName[256];
static void InitializeInterface(const char *interfaceName, CreateInterfaceFn *factoryList, int numFactories)
{
	void *retval;
	for (int i = 0; i < numFactories; i++)
	{
		CreateInterfaceFn factory = factoryList[ i ];
		if (!factory)
			continue;

		retval = factory(interfaceName, NULL);
		if (retval)
			return retval;
	}

	// No provider for requested interface!!!
	// assert( !"No provider for requested interface!!!" );

	return NULL;
}

bool VGuiControls_Init(const char *moduleName, CreateInterfaceFn *factoryList, int numFactories)
{
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   133
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   134
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   135
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   136
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   137
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   138
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   139
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   140
//	InitializeInterface(const char *interfaceName,
//				CreateInterfaceFn* factoryList,
//				int numFactories)  //   141
}

void VGuiControls_Shutdown()
{
}

// returns the name of the module this has been compiled into
const char *GetControlsModuleName()
{
	return g_szControlsModuleName;
}

} // namespace vgui2

