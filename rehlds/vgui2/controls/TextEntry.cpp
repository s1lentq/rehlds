#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <UtlVector.h>

#include <vgui/Cursor.h>
#include <vgui/IInput.h>
#include <vgui/IScheme.h>
#include <vgui/ISystem.h>
#include <vgui/ISurface.h>
#include <vgui/ILocalize.h>
#include <vgui/IPanel.h>
#include <KeyValues.h>
#include <vgui/MouseCode.h>

#include <vgui_controls/Menu.h>
#include <vgui_controls/ScrollBar.h>
#include <vgui_controls/TextEntry.h>
#include <vgui_controls/Controls.h>
#include <vgui_controls/MenuItem.h>

enum
{
	MAX_BUFFER_SIZE = 999999,	// maximum size of text buffer
	SCROLLBAR_SIZE = 18,
	DRAW_OFFSET_X =	3,
	DRAW_OFFSET_Y =	1,
};

using namespace vgui;

// Constructor
TextEntry::TextEntry(Panel *parent, const char *panelName) : Panel(parent, panelName)
{
	SetTriplePressAllowed(true);

	_font = INVALID_FONT;

	m_bAllowNumericInputOnly = false;
	m_bAllowNonAsciiCharacters = false;
	_hideText = false;
	_editable = false;
	_verticalScrollbar = false;
	_cursorPos = 0;
	_currentStartIndex = 0;
	_horizScrollingAllowed = true;
	_cursorIsAtEnd = false;
	_putCursorAtEnd = false;
	_multiline = false;
	_cursorBlinkRate = 400;
	_mouseSelection = false;
	_mouseDragSelection = false;
	_vertScrollBar = NULL;
	_catchEnterKey = false;
	_maxCharCount = -1;
	_charCount = 0;
	_wrap = false;		// don't wrap by default
	_sendNewLines = false;	// don't pass on a newline msg by default
	_drawWidth = 0;

	m_hFallbackFont = INVALID_FONT;

	//a -1 for _select[0] means that the selection is empty
	_select[0] = -1;
	_select[1] = -1;
	m_pEditMenu = NULL;

	// this really just inits it when in here
	ResetCursorBlink();
	SetCursor(dc_ibeam);

	// position the cursor so it is at the end of the text
	GotoTextEnd();
	SetEditable(true);

	// initialize the line break array
	m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);

	_recalculateBreaksIndex = 0;

	_firstFocusStatus = false;
	_selectAllOnFirstFocus = false;
	_scrollBarSize = SCROLLBAR_SIZE;

	if (m_bProportional)
	{
		int width, height;
		int sw, sh;

		vgui::surface()->GetProportionalBase(&width, &height);
		vgui::surface()->GetScreenSize(&sw, &sh);

		_scrollBarSize = (int)(sw / width * SCROLLBAR_SIZE);
	}
}

TextEntry::~TextEntry()
{
	delete m_pEditMenu;
}

void TextEntry::ApplySchemeSettings(IScheme *pScheme)
{
	BaseClass::ApplySchemeSettings(pScheme);

	SetFgColor(GetSchemeColor("WindowFgColor", pScheme));
	SetBgColor(GetSchemeColor("TextCursorColor", pScheme));

	_cursorColor = GetSchemeColor("TextCursorColor", pScheme);
	_disabledFgColor = GetSchemeColor("WindowDisabledFgColor", pScheme);
	_disabledBgColor = GetSchemeColor("ControlBG", pScheme);

	_selectionTextColor = GetSchemeColor("SelectionFgColor", GetFgColor(), pScheme);
	_selectionColor = GetSchemeColor("SelectionBgColor", pScheme);
	_defaultSelectionBG2Color = GetSchemeColor("SelectionBG2", pScheme);
	_focusEdgeColor = GetSchemeColor("BorderSelection", Color(0, 0, 0, 0), pScheme);

	SetBorder(pScheme->GetBorder("ButtonDepressedBorder"));

	_font = pScheme->GetFont("Default", IsProportional());
	SetFont(_font);
}

// Sets the color of the background when the control is disabled
void TextEntry::SetDisabledBgColor(Color col)
{
	_disabledBgColor = col;
}

// Sends a message if the data has changed
// Turns off any selected text in the window if we are not using the edit menu
void TextEntry::OnKillFocus()
{
	if (_dataChanged)
	{
		FireActionSignal();
		_dataChanged = false;
	}

	// check if we clicked the right mouse button or if it is down
	bool mouseRightClicked = vgui::input()->WasMousePressed(MOUSE_RIGHT);
	bool mouseRightUp = vgui::input()->WasMouseReleased(MOUSE_RIGHT);
	bool mouseRightDown = vgui::input()->IsMouseDown(MOUSE_RIGHT);

	if (mouseRightClicked || mouseRightDown || mouseRightUp)
	{
		int cursorX, cursorY;
		vgui::input()->GetCursorPos(cursorX, cursorY);

		// if we're right clicking within our window, we don't actually kill focus
		if (IsWithin(cursorX, cursorY))
			return;
	}

	if (IsEditable())
	{
		// clear any selection
		SelectNone();
	}

	// move the cursor to the start
	// GotoTextStart();

	// PostActionSignal(new KeyValues("TextKillFocus"));

	// chain
	BaseClass::OnKillFocus();
}

// Wipe line breaks after the size of a panel has been changed
void TextEntry::OnSizeChanged(int newWide, int newTall)
{
	BaseClass::OnSizeChanged(newWide, newTall);

	// blow away the line breaks list
	_recalculateBreaksIndex = 0;
	m_LineBreaks.RemoveAll();
	m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);

	// if we're bigger, see if we can scroll left to put more text in the window
	if (newWide > _drawWidth)
	{
		ScrollLeftForResize();
	}

	_drawWidth = newWide;
	InvalidateLayout();
}

// Set the text array - convert ANSI text to unicode and pass to unicode function
void TextEntry::SetText(const char *text)
{
	if (!text)
	{
		text = "";
	}

	if (text[0] == '#')
	{
		// check for localization
		wchar_t *wsz = vgui::localize()->Find(text);
		if (wsz)
		{
			SetText(wsz);
			return;
		}
	}

	wchar_t unicode[1024];
	vgui::localize()->ConvertANSIToUnicode(text, unicode, sizeof(unicode));
	SetText(unicode);
}

// Set the text array
// Using this function will cause all lineBreaks to be discarded.
// This is because this fxn replaces the contents of the text buffer.
// For modifying large buffers use insert functions.
void TextEntry::SetText(const wchar_t *wszText)
{
	if (!wszText)
	{
		wszText = L"";
	}

	int textLen = wcslen(wszText);
	m_TextStream.RemoveAll();
	m_TextStream.EnsureCapacity(textLen);

	int missed_count = 0;
	for (int i = 0; i < textLen; i++)
	{
		// don't insert \r characters
		if (wszText[i] == '\r')
		{
			missed_count++;
			continue;
		}

		m_TextStream.AddToTail(wszText[i]);
		SetCharAt(wszText[i], i - missed_count);
	}

	GotoTextStart();
	SelectNone();

	// reset the data changed flag
	_dataChanged = false;

	// blow away the line breaks list
	_recalculateBreaksIndex = 0;
	m_LineBreaks.RemoveAll();
	m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);

	InvalidateLayout();
}

// Sets the value of char at index position.
void TextEntry::SetCharAt(wchar_t ch, int index)
{
	if ((ch == '\n') || (ch == '\0'))
	{
		// if its not at the end of the buffer it matters.
		// redo the linebreaks
		// if (index != m_TextStream.Count())
		{
			_recalculateBreaksIndex = 0;
			m_LineBreaks.RemoveAll();
			m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);
		}
	}

	if (index < 0)
		return;

	if (index >= m_TextStream.Count())
	{
		m_TextStream.AddMultipleToTail(index - m_TextStream.Count() + 1);
	}

	m_TextStream[index] = ch;
	_dataChanged = true;
}

// Restarts the time of the next cursor blink
void TextEntry::ResetCursorBlink()
{
	_cursorBlink = false;
	_cursorNextBlinkTime = vgui::system()->GetTimeMillis() + _cursorBlinkRate;
}

// Hides the text buffer so it will not be drawn
void TextEntry::SetTextHidden(bool bHideText)
{
	_hideText = bHideText;
	Repaint();
}

// Return character width
int getCharWidth(HFont font, wchar_t ch)
{
	if (iswprint(ch))
	{
		int a, b, c;
		vgui::surface()->GetCharABCwide(font, ch, a, b, c);
		return (a + b + c);
	}

	return 0;
}

// Given cursor's position in the text buffer, convert it to the local window's x and y pixel coordinates
// cursorPos: cursor index
// cx, cy, the corresponding coords in the local window
void TextEntry::CursorToPixelSpace(int cursorPos, int &cx, int &cy)
{
	int yStart = GetYStart();

	int x = DRAW_OFFSET_X, y = yStart;
	_pixelsIndent = 0;
	int lineBreakIndexIndex = 0;

	for (int i = GetStartDrawIndex(lineBreakIndexIndex); i < m_TextStream.Count(); i++)
	{
		wchar_t ch = m_TextStream[i];
		if (_hideText)
		{
			ch = '*';
		}

		// if we've found the position, break
		if (cursorPos == i)
		{
			// even if this is a line break entry for the cursor, the next insert
			// will be at this position, which will push the line break forward one
			// so don't push the cursor down a line here...
			/*if (!_putCursorAtEnd)
			{
				// if we've passed a line break go to that
				if (m_LineBreaks[lineBreakIndexIndex] == i)
				{
					// add another line
					AddAnotherLine(x, y);
					lineBreakIndexIndex++;
				}
			}*/
			break;
		}

		// if we've passed a line break go to that
		if (m_LineBreaks.Count() &&
			lineBreakIndexIndex < m_LineBreaks.Count() &&
			m_LineBreaks[lineBreakIndexIndex] == i)
		{
			// add another line
			AddAnotherLine(x, y);
			lineBreakIndexIndex++;
		}

		// add to the current position
		x += getCharWidth(_font, ch);
	}

	cx = x;
	cy = y;
}

// Converts local pixel coordinates to an index in the text buffer
// This function appears to be used only in response to mouse clicking
// cy - pixel location
int TextEntry::PixelToCursorSpace(int cx, int cy)
{
	/*int w, h;
	GetSize(w, h);
	cx = clamp(cx, 0, w + 100);
	cy = clamp(cy, 0, h);*/

	// Start off assuming we clicked somewhere in the text
	_putCursorAtEnd = false;

	int fontTall = vgui::surface()->GetFontTall(_font);

	// where to Start reading
	int yStart = GetYStart();
	int x = DRAW_OFFSET_X, y = yStart;
	_pixelsIndent = 0;
	int lineBreakIndexIndex = 0;

	int startIndex = GetStartDrawIndex(lineBreakIndexIndex);
	bool onRightLine = false;
	int i;
	for (i = startIndex; i < m_TextStream.Count(); i++)
	{
		wchar_t ch = m_TextStream[i];
		if (_hideText)
		{
			ch = '*';
		}

		// if we are on the right line but off the end of if put the cursor at the end of the line
		if (m_LineBreaks[lineBreakIndexIndex] == i)
		{
			// add another line
			AddAnotherLine(x, y);
			lineBreakIndexIndex++;

			if (onRightLine)
			{
				_putCursorAtEnd = true;
				return i;
			}
		}

		// check to see if we're on the right line
		if (cy < yStart)
		{
			// cursor is above panel
			onRightLine = true;

			// this will make the text scroll up if needed
			_putCursorAtEnd = true;
		}
		else if (cy >= y && (cy < (y + fontTall + DRAW_OFFSET_Y)))
		{
			onRightLine = true;
		}

		int wide = getCharWidth(_font, ch);

		// if we've found the position, break
		if (onRightLine)
		{
			// off right side of window
			if (cx > GetWide())
			{
			}
			// off left side of window
			else if (cx < (DRAW_OFFSET_X + _pixelsIndent) || cy < yStart)
			{
				// move cursor one to left
				return i;
			}

			if (cx >= x && cx < (x + wide))
			{
				// check which side of the letter they're on
				// left side
				if (cx < (x + (wide * 0.5)))
				{
					return i;
				}
				// right side
				else
				{
					return i + 1;
				}
			}
		}

		x += wide;
	}

	return i;
}

// Draws a character in the panel
// ch - character to draw
// font - font to use
// x, y - pixel location to draw char at
// returns the width of the character drawn
int TextEntry::DrawChar(wchar_t ch, HFont font, int index, int x, int y)
{
	// add to the current position
	int charWide = getCharWidth(font, ch);
	int fontTall = vgui::surface()->GetFontTall(font);
	if (!iswprint(ch))
	{
		// draw selection, if any
		int selection0 = -1, selection1 = -1;
		GetSelectedRange(selection0, selection1);

		if (index >= selection0 && index < selection1)
		{
			// draw background selection color
			VPANEL focus = vgui::input()->GetFocus();

			Color bgColor;
			bool hasFocus = HasFocus();
			bool childOfFocus = focus && vgui::ipanel()->HasParent(focus, GetVPanel());

			// if one of the children of the SectionedListPanel has focus, then 'we have focus' if we're selected
			if (hasFocus || childOfFocus)
			{
				bgColor = _selectionColor;
			}
			else
			{
				bgColor =_defaultSelectionBG2Color;
			}

			vgui::surface()->DrawSetColor(bgColor);
			vgui::surface()->DrawFilledRect(x, y, x + charWide, y + 1 + fontTall);

			// reset text color
			vgui::surface()->DrawSetTextColor(_selectionTextColor);
		}

		/*if (index == selection1)
		{
			// we've come out of selection, reset the color
			vgui::surface()->DrawSetTextColor(GetFgColor());
		}*/

		vgui::surface()->DrawSetTextPos(x, y);
		vgui::surface()->DrawUnicodeChar(ch);

		return charWide;
	}

	return 0;
}

// Draw the cursor, cursor is not drawn when it is blinked gone
// x, y where to draw cursor
// returns true if cursor was drawn.
bool TextEntry::DrawCursor(int x, int y)
{
	if (!_cursorBlink)
	{
		int cx, cy;
		CursorToPixelSpace(_cursorPos, cx, cy);
		vgui::surface()->DrawSetColor(_cursorColor);

		int fontTall = vgui::surface()->GetFontTall(_font);
		vgui::surface()->DrawFilledRect(cx, cy, cx + 1, cy + fontTall);

		return true;
	}

	return false;
}

bool TextEntry::NeedsEllipses(HFont font, int *pIndex)
{
	Assert(pIndex);

	*pIndex = -1;

	// buffer on left and right end of text.
	int wide = DRAW_OFFSET_X;
	for (int i = 0; i < m_TextStream.Count(); ++i)
	{
		wide += getCharWidth(font, m_TextStream[i]);
		if (wide > _drawWidth)
		{
			*pIndex = i;
			return true;
		}
	}

	return false;
}

// Draws the text in the panel
void TextEntry::PaintBackground()
{
	// draw background
	Color col;
	if (IsEnabled())
	{
		col = GetBgColor();
	}
	else
	{
		col = _disabledBgColor;
	}

	vgui::surface()->DrawSetColor(col);

	int wide, tall;
	GetSize(wide, tall);
	vgui::surface()->DrawFilledRect(0, 0, wide, tall);

	HFont useFont = _font;
	vgui::surface()->DrawSetTextFont(useFont);

	if (IsEnabled())
	{
		col = GetFgColor();
	}
	else
	{
		col = _disabledFgColor;
	}

	vgui::surface()->DrawSetTextColor(col);
	_pixelsIndent = 0;

	int lineBreakIndexIndex = 0;
	int startIndex = GetStartDrawIndex(lineBreakIndexIndex);

	// where to Start drawing
	int x = DRAW_OFFSET_X + _pixelsIndent, y = GetYStart();

	// draw text with an elipsis
	if (!_multiline && !_horizScrollingAllowed)
	{
		int endIndex = m_TextStream.Count();

		// In editable windows only do the ellipsis if we don't have focus.
		// In non editable windows do it all the time.
		if ((!HasFocus() && IsEditable()) || !IsEditable())
		{
			int i = -1;

			// loop through all the characters and sum their widths
			bool addEllipses = NeedsEllipses(useFont, &i);
			if (addEllipses && !IsEditable() && m_bUseFallbackFont && m_hFallbackFont != INVALID_FONT)
			{
				// Switch to small font!!!
				useFont = m_hFallbackFont;
				vgui::surface()->DrawSetTextFont(useFont);
				addEllipses = NeedsEllipses(useFont, &i);
			}

			if (addEllipses)
			{
				int elipsisWidth = 3 * getCharWidth(useFont, '.');
				while (elipsisWidth > 0 && i >= 0)
				{
					elipsisWidth -= getCharWidth(useFont, m_TextStream[i]);
					i--;
				}
				endIndex = i + 1;
			}

			// if we take off less than the last 3 chars we have to make sure
			// we take off the last 3 chars so selected text will look right.
			if (m_TextStream.Count() - endIndex < 3 && m_TextStream.Count() - endIndex > 0)
			{
				endIndex = m_TextStream.Count() - 3;
			}
		}

		// draw the text
		int i;
		for (i = startIndex; i < endIndex; i++)
		{
			wchar_t ch = m_TextStream[i];
			if (_hideText)
			{
				ch = '*';
			}

			// draw the character and update xposition
			x += DrawChar(ch, useFont, i, x, y);

			// Restore color
			// vgui::surface()->DrawSetTextColor(col);

		}

		// add an elipsis
		if (endIndex < m_TextStream.Count())
		{
			x += DrawChar('.', useFont, i, x, y);
			i++;
			x += DrawChar('.', useFont, i, x, y);
			i++;
			x += DrawChar('.', useFont, i, x, y);
			i++;
		}
	}
	else
	{
		// draw the text
		for (int i = startIndex; i < m_TextStream.Count(); i++)
		{
			wchar_t ch = m_TextStream[i];
			if (_hideText)
			{
				ch = '*';
			}

			// if we've passed a line break go to that
			if (_multiline && m_LineBreaks[lineBreakIndexIndex] == i)
			{
				// add another line
				AddAnotherLine(x, y);
				lineBreakIndexIndex++;
			}

			// draw the character and update xposition
			x += DrawChar(ch, useFont, i, x, y);

			// Restore color
			// vgui::surface()->DrawSetTextColor(col);
		}
	}

	// custom border
	// TODO: need to replace this with scheme stuff (TextEntryBorder/TextEntrySelectedBorder)
	vgui::surface()->DrawSetColor(50, 50, 50, 255);

	if (IsEnabled() && IsEditable() && HasFocus())
	{
		// set a more distinct border color
		vgui::surface()->DrawSetColor(0, 0, 0, 255);
		DrawCursor(x, y);
	}
}

// Called when data changes or panel size changes
void TextEntry::PerformLayout()
{
	BaseClass::PerformLayout();
	RecalculateLineBreaks();

	// recalculate scrollbar position
	if (_verticalScrollbar)
	{
		LayoutVerticalScrollBarSlider();
	}

	// force a Repaint
	Repaint();
}

// moves x,y to the Start of the next line of text
void TextEntry::AddAnotherLine(int &cx, int &cy)
{
	cx = DRAW_OFFSET_X + _pixelsIndent;
	cy += (vgui::surface()->GetFontTall(_font) + DRAW_OFFSET_Y);
}

// Recalculates line breaks
void TextEntry::RecalculateLineBreaks()
{
	if (!_multiline || _hideText)
		return;

	if (m_TextStream.Count() < 1)
		return;

	HFont font = _font;

	// line break to our width -2 pixel to keep cursor blinking in window
	// (assumes borders are 1 pixel)
	int wide = GetWide() - 2;

	// subtract the scrollbar width
	if (_vertScrollBar)
	{
		wide -= _vertScrollBar->GetWide();
	}

	int charWidth;
	int x = DRAW_OFFSET_X, y = DRAW_OFFSET_Y;

	int wordStartIndex = 0;
	int wordLength = 0;
	bool hasWord = false;
	bool justStartedNewLine = true;
	bool wordStartedOnNewLine = true;

	int startChar;
	if (_recalculateBreaksIndex <= 0)
	{
		m_LineBreaks.RemoveAll();
		startChar = 0;
	}
	else
	{
		// remove the rest of the linebreaks list since its out of date.
		for (int i = _recalculateBreaksIndex + 1; i < m_LineBreaks.Count(); ++i)
		{
			m_LineBreaks.Remove((int)i);
			--i; // removing shrinks the list!
		}
		startChar = m_LineBreaks[_recalculateBreaksIndex];
	}

	// handle the case where this char is a new line, in that case
	// we have already taken its break index into account above so skip it.
	if (m_TextStream[startChar] == '\r' || m_TextStream[startChar] == '\n')
	{
		startChar++;
	}

	// loop through all the characters
	int i;
	for (i = startChar; i < m_TextStream.Count(); ++i)
	{
		wchar_t ch = m_TextStream[i];

		// line break only on whitespace characters
		if (!iswspace(ch))
		{
			if (hasWord)
			{
				// append to the current word
			}
			else
			{
				// Start a new word
				wordStartIndex = i;
				hasWord = true;
				wordStartedOnNewLine = justStartedNewLine;
				wordLength = 0;
			}
		}
		else
		{
			// whitespace/punctuation character
			// end the word
			hasWord = false;
		}

		// get the width
		charWidth = getCharWidth(font, ch);
		if (iswprint(ch))
		{
			justStartedNewLine = false;
		}

		// check to see if the word is past the end of the line [wordStartIndex, i)
		if ((x + charWidth) >= wide || ch == '\r' || ch == '\n')
		{
			// add another line
			AddAnotherLine(x, y);

			justStartedNewLine = true;
			hasWord = false;

			if (ch == '\r' || ch == '\n')
			{
				// set the break at the current character
				m_LineBreaks.AddToTail(i);
			}
			else if (wordStartedOnNewLine || iswspace(ch))
			{
				// word is longer than a line, so set the break at the current cursor
				m_LineBreaks.AddToTail(i);
			}
			else
			{
				// set it at the last word Start
				m_LineBreaks.AddToTail(wordStartIndex);

				// just back to reparse the next line of text
				i = wordStartIndex;
			}

			// reset word length
			wordLength = 0;
		}

		// add to the size
		x += charWidth;
		wordLength += charWidth;
	}

	_charCount = i - 1;

	// end the list
	m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);

	// set up the scrollbar
	LayoutVerticalScrollBarSlider();
}

// Recalculate where the vertical scroll bar slider should be based on the current cursor line we are on.
void TextEntry::LayoutVerticalScrollBarSlider()
{
	// set up the scrollbar
	if (_vertScrollBar)
	{
		int wide, tall;
		GetSize(wide, tall);

		// make sure we factor in insets
		int ileft, iright, itop, ibottom;
		GetInset(ileft, iright, itop, ibottom);

		// with a scroll bar we take off the inset
		wide -= iright;
		_vertScrollBar->SetPos(wide - _scrollBarSize, 0);

		// scrollbar is inside the borders.
		_vertScrollBar->SetSize(_scrollBarSize, tall - ibottom - itop);

		// calculate how many lines we can fully display
		int displayLines = tall / (vgui::surface()->GetFontTall(_font) + DRAW_OFFSET_Y);
		int numLines = m_LineBreaks.Count();

		if (numLines <= displayLines)
		{
			// disable the scrollbar
			_vertScrollBar->SetEnabled(false);
			_vertScrollBar->SetRange(0, numLines);
			_vertScrollBar->SetRangeWindow(numLines);
			_vertScrollBar->SetValue(0);
		}
		else
		{
			// set the scrollbars range
			_vertScrollBar->SetRange(0, numLines);
			_vertScrollBar->SetRangeWindow(displayLines);
			_vertScrollBar->SetEnabled(true);

			// this should make it scroll one line at a time
			_vertScrollBar->SetButtonPressedScrollValue(1);

			// set the value to view the last entries
			int val = _vertScrollBar->GetValue();
			int maxval = _vertScrollBar->GetValue() + displayLines;
			if (GetCursorLine() < val)
			{
				while (GetCursorLine() < val)
				{
					val--;
				}
			}
			else if (GetCursorLine() >= maxval)
			{
				while (GetCursorLine() >= maxval)
				{
					maxval++;
				}

				// maxval -= displayLines;
				// val = maxval;
			}
			else
			{
				// val = GetCursorLine();
			}

			_vertScrollBar->SetValue(val);
			_vertScrollBar->InvalidateLayout();
			_vertScrollBar->Repaint();
		}
	}
}

// Set boolean value of baseclass variables.
void TextEntry::SetEnabled(bool state)
{
	BaseClass::SetEnabled(state);
	Repaint();
}

// Sets whether text wraps around multiple lines or not
// state - true or false
void TextEntry::SetMultiline(bool state)
{
	_multiline = state;
}

// Sets whether or not the edit catches and stores ENTER key presses
void TextEntry::SetCatchEnterKey(bool state)
{
	_catchEnterKey = state;
}

// Sets whether a vertical scrollbar is visible
// state - true or false
void TextEntry::SetVerticalScrollbar(bool state)
{
	_verticalScrollbar = state;

	if (_verticalScrollbar)
	{
		if (!_vertScrollBar)
		{
			_vertScrollBar = new ScrollBar(this, "ScrollBar", true);
			_vertScrollBar->AddActionSignalTarget(this);
		}

		_vertScrollBar->SetVisible(true);
	}
	else if (_vertScrollBar)
	{
		_vertScrollBar->SetVisible(false);
	}

	InvalidateLayout();
}

// Sets _editable flag
// state - true or false
void TextEntry::SetEditable(bool state)
{
	_editable = state;
}

// Create cut/copy/paste dropdown menu
void TextEntry::CreateEditMenu()
{
	// create a drop down cut/copy/paste menu appropriate for this object's states
	if (m_pEditMenu)
		delete m_pEditMenu;

	m_pEditMenu = new Menu(this, "EditMenu");

	// add cut/copy/paste drop down options if its editable, just copy if it is not
	if (_editable && !_hideText)
	{
		m_pEditMenu->AddMenuItem("#TextEntry_Cut", new KeyValues("DoCutSelected"), this);
	}

	if (!_hideText)
	{
		m_pEditMenu->AddMenuItem("#TextEntry_Copy", new KeyValues("DoCopySelected"), this);
	}

	if (_editable)
	{
		m_pEditMenu->AddMenuItem("#TextEntry_Paste", new KeyValues("DoPaste"), this);
	}

	m_pEditMenu->SetVisible(false);
	m_pEditMenu->SetParent(this);
	m_pEditMenu->AddActionSignalTarget(this);
}

// Returns state of _editable flag
bool TextEntry::IsEditable()
{
	return _editable && IsEnabled();
}

// We want single line windows to scroll horizontally and select text in response to clicking and holding outside window
void TextEntry::OnMouseFocusTicked()
{
	// if a button is down move the scrollbar slider the appropriate direction
	// text is being selected via mouse clicking and dragging
	if (_mouseDragSelection)
	{
		// we want the text to scroll as if we were dragging
		OnCursorMoved(0, 0);
	}
}

// If a cursor enters the window, we are not elegible for MouseFocusTicked events
void TextEntry::OnCursorEntered()
{
	// outside of window dont recieve drag scrolling ticks
	_mouseDragSelection = false;
}

// When the cursor is outside the window, if we are holding the mouse
// button down, then we want the window to scroll the text one char at a time using Ticks
// outside of window recieve drag scrolling ticks
void TextEntry::OnCursorExited()
{
	if (_mouseSelection)
		_mouseDragSelection = true;
}

// Handle selection of text by mouse
void TextEntry::OnCursorMoved(int x_, int y_)
{
	if (_mouseSelection)
	{
		// update the cursor position
		int x, y;
		vgui::input()->GetCursorPos(x, y);
		ScreenToLocal(x, y);
		_cursorPos = PixelToCursorSpace(x, y);

		// if we are at Start of buffer don't put cursor at end, this will keep
		// window from scrolling up to a blank line
		if (_cursorPos == 0)
			_putCursorAtEnd = false;

		// scroll if we went off left side
		if (_cursorPos == _currentStartIndex)
		{
			if (_cursorPos > 0)
				_cursorPos--;

			ScrollLeft();
			_cursorPos = _currentStartIndex;
		}
		if (_cursorPos != _select[1])
		{
			_select[1] = _cursorPos;
			Repaint();
		}
	}
}

// Handle Mouse button down events.
void TextEntry::OnMousePressed(MouseCode code)
{
	if (code == MOUSE_LEFT)
	{
		SelectCheck();

		// move the cursor to where the mouse was pressed
		int x, y;
		vgui::input()->GetCursorPos(x, y);
		ScreenToLocal(x, y);

		// save this off before calling PixelToCursorSpace()
		_cursorIsAtEnd = _putCursorAtEnd;
		_cursorPos = PixelToCursorSpace(x, y);

		// if we are at Start of buffer don't put cursor at end, this will keep
		// window from scrolling up to a blank line
		if (_cursorPos == 0)
			_putCursorAtEnd = false;

		// enter selection mode
		vgui::input()->SetMouseCapture(GetVPanel());
		_mouseSelection = true;

		if (_select[0] < 0)
		{
			// if no initial selection position, Start selection position at cursor
			_select[0] = _cursorPos;
		}

		_select[1] = _cursorPos;

		ResetCursorBlink();
		RequestFocus();
		Repaint();
	}
	else if (code == MOUSE_RIGHT) // check for context menu open
	{
		CreateEditMenu();
		Assert(m_pEditMenu);

		OpenEditMenu();
	}
}

// Handle mouse button up events
void TextEntry::OnMouseReleased(MouseCode code)
{
	_mouseSelection = false;

	vgui::input()->SetMouseCapture(NULL);

	// make sure something has been selected
	int cx0, cx1;
	if (GetSelectedRange(cx0, cx1))
	{
		if (cx1 - cx0 == 0)
		{
			// nullify selection
			_select[0] = -1;
		}
	}
}

// Handle mouse double clicks
void TextEntry::OnMouseDoublePressed(MouseCode code)
{
	// left double clicking on a word selects the word
	if (code == MOUSE_LEFT)
	{
		int selectSpot[2];

		// move the cursor just as if you single clicked.
		OnMousePressed(code);

		// then find the start and end of the word we are in to highlight it.
		GotoWordLeft();
		selectSpot[0] = _cursorPos;
		GotoWordRight();
		selectSpot[1] = _cursorPos;

		if (_cursorPos > 0)
		{
			if (iswspace(m_TextStream[_cursorPos - 1]))
			{
				selectSpot[1]--;
				_cursorPos--;
			}

			_select[0] = selectSpot[0];
			_select[1] = selectSpot[1];
			_mouseSelection = true;
		}
	}

}

// Turn off text selection code when mouse button is not down
void TextEntry::OnMouseCaptureLost()
{
	_mouseSelection = false;
}

// Masks which keys get chained up
// Maps keyboard input to text window functions.
void TextEntry::OnKeyCodeTyped(KeyCode code)
{
	_cursorIsAtEnd = _putCursorAtEnd;
	_putCursorAtEnd = false;

	bool shift = (vgui::input()->IsKeyDown(KEY_LSHIFT) || vgui::input()->IsKeyDown(KEY_RSHIFT));
	bool ctrl = (vgui::input()->IsKeyDown(KEY_LCONTROL) || vgui::input()->IsKeyDown(KEY_RCONTROL));
	bool alt = (vgui::input()->IsKeyDown(KEY_LALT) || vgui::input()->IsKeyDown(KEY_RALT));
	bool winkey = (vgui::input()->IsKeyDown(KEY_LWIN) || vgui::input()->IsKeyDown(KEY_RWIN));
	bool fallThrough = false;
	bool bUpdateCursorPos = true;

	if (ctrl)
	{
		switch (code)
		{
		case KEY_A:
		{
			SelectAllText(false);
			// move the cursor to the end
			_cursorPos = _select[1];
			break;
		}
		case KEY_INSERT:
		case KEY_C:
		{
			CopySelected();
			break;
		}
		case KEY_V:
		{
			DeleteSelected();
			Paste();
			break;
		}
		case KEY_X:
		{
			CopySelected();
			DeleteSelected();
			break;
		}
		case KEY_Z:
		{
			Undo();
			break;
		}
		case KEY_RIGHT:
		{
			GotoWordRight();
			break;
		}
		case KEY_LEFT:
		{
			GotoWordLeft();
			break;
		}
		case KEY_ENTER:
		{
			// insert a newline
			if (_multiline)
			{
				DeleteSelected();
				SaveUndoState();
				InsertChar('\n');
			}

			// fire newlines back to the main target if asked to
			if (_sendNewLines)
			{
				PostActionSignal(new KeyValues("TextNewLine"));
			}
			break;
		}
		case KEY_HOME:
		{
			GotoTextStart();
			break;
		}
		case KEY_END:
		{
			GotoTextEnd();
			break;
		}
		default:
		{
			fallThrough = true;
			break;
		}
		}
	}
	else if (alt)
	{
		// do nothing with ALT-x keys
		// if (!m_bAllowNonAsciiCharacters || (code != KEY_BACKQUOTE))
		{
			fallThrough = true;
		}
	}
	else
	{
		switch(code)
		{
		case KEY_TAB:
		case KEY_LSHIFT:
		case KEY_RSHIFT:
		case KEY_ESCAPE:
		{
			fallThrough = true;
			break;
		}
		case KEY_INSERT:
		{
			if (shift)
			{
				DeleteSelected();
				Paste();
			}
			else
			{
				fallThrough = true;
			}
			break;
		}
		case KEY_DELETE:
		{
			if (shift)
			{
				// shift-delete is cut
				CopySelected();
				DeleteSelected();
			}
			else
			{
				Delete();
			}
			break;
		}
		case KEY_LEFT:
		{
			GotoLeft();
			break;
		}
		case KEY_RIGHT:
		{
			GotoRight();
			break;
		}
		case KEY_UP:
		{
			if (_multiline)
			{
				GotoUp();
			}
			else
			{
				fallThrough = true;
			}
			break;
		}
		case KEY_DOWN:
		{
			if (_multiline)
			{
				GotoDown();
			}
			else
			{
				fallThrough = true;
			}
			break;
		}
		case KEY_HOME:
		{
			if (_multiline)
			{
				GotoFirstOfLine();
			}
			else
			{
				GotoTextStart();
			}
			break;
		}
		case KEY_END:
		{
			GotoEndOfLine();
			break;
		}
		case KEY_BACKSPACE:
		{
			int x0, x1;
			if (GetSelectedRange(x0, x1))
			{
				// act just like delete if there is a selection
				DeleteSelected();
			}
			else
			{
				Backspace();
			}
			break;
		}
		case KEY_ENTER:
		{
			// insert a newline
			if (_multiline && _catchEnterKey)
			{
				DeleteSelected();
				SaveUndoState();
				InsertChar('\n');
			}
			else
			{
				fallThrough = true;
			}
			// fire newlines back to the main target if asked to
			if (_sendNewLines)
			{
				PostActionSignal(new KeyValues("TextNewLine"));
			}
			break;
		}
		case KEY_PAGEUP:
		{
			int val = 0;
			if (_vertScrollBar)
			{
				val = _vertScrollBar->GetValue();
			}

			// if there is a scroll bar scroll down one rangewindow
			if (_multiline)
			{
				int displayLines = GetTall() / (vgui::surface()->GetFontTall(_font) + DRAW_OFFSET_Y);
				// move the cursor down
				for (int i = 0; i < displayLines; i++)
				{
					GotoUp();
				}
			}

			// if there is a scroll bar scroll down one rangewindow
			if (_vertScrollBar)
			{
				int window = _vertScrollBar->GetRangeWindow();
				int newval = _vertScrollBar->GetValue();
				int linesToMove = window - (val - newval);
				_vertScrollBar->SetValue(val - linesToMove - 1);
			}
			break;
		}
		case KEY_PAGEDOWN:
		{
			int val = 0;
			if (_vertScrollBar)
			{
				val = _vertScrollBar->GetValue();
			}

			if (_multiline)
			{
				int displayLines = GetTall() / (vgui::surface()->GetFontTall(_font) + DRAW_OFFSET_Y);
				// move the cursor down
				for (int i = 0; i < displayLines; i++)
				{
					GotoDown();
				}
			}

			// if there is a scroll bar scroll down one rangewindow
			if (_vertScrollBar)
			{
				int window = _vertScrollBar->GetRangeWindow();
				int newval = _vertScrollBar->GetValue();
				int linesToMove = window - (newval - val);
				_vertScrollBar->SetValue(val + linesToMove + 1);
			}
			break;
		}
		case KEY_F1:
		case KEY_F2:
		case KEY_F3:
		case KEY_F4:
		case KEY_F5:
		case KEY_F6:
		case KEY_F7:
		case KEY_F8:
		case KEY_F9:
		case KEY_F10:
		case KEY_F11:
		case KEY_F12:
		{
			fallThrough = true;
			break;
		}
		default:
		{
			// return if any other char is pressed.
			// as it will be a unicode char.
			// and we don't want select[1] changed unless a char was pressed that this fxn handles
			return;
		}
		}
	}

	// select[1] is the location in the line where the blinking cursor started
	_select[1] = _cursorPos;

	if (_dataChanged)
	{
		FireActionSignal();
	}

	// chain back on some keys
	if (fallThrough)
	{
		// keep state of cursor on fallthroughs
		_putCursorAtEnd = _cursorIsAtEnd;
		BaseClass::OnKeyCodeTyped(code);
	}
}

// Masks which keys get chained up
// Maps keyboard input to text window functions.
void TextEntry::OnKeyTyped(wchar_t unichar)
{
	_cursorIsAtEnd = _putCursorAtEnd;
	_putCursorAtEnd = false;

	bool fallThrough = false;

	// KeyCodes handle all non printable chars
	if (!iswprint(unichar) || unichar == '\t') // tab key (code 9) is printable but handled elsewhere
		return;

	// do readonly keys
	if (!IsEditable())
	{
		BaseClass::OnKeyTyped(unichar);
		return;
	}

	if (unichar)
	{
		DeleteSelected();
		SaveUndoState();
		InsertChar(unichar);
	}

	// select[1] is the location in the line where the blinking cursor started
	_select[1] = _cursorPos;

	if (_dataChanged)
	{
		FireActionSignal();
	}

	// chain back on some keys
	if (fallThrough)
	{
		// keep state of cursor on fallthroughs
		_putCursorAtEnd = _cursorIsAtEnd;
		BaseClass::OnKeyTyped(unichar);
	}
}

// Scrolls the list according to the mouse wheel movement
void TextEntry::OnMouseWheeled(int delta)
{
	if (_vertScrollBar)
	{
		MoveScrollBar(delta);
	}
	else
	{
		// if we don't use the input, chain back
		BaseClass::OnMouseWheeled(delta);
	}
}

// Scrolls the list
// delta - amount to move scrollbar up
void TextEntry::MoveScrollBar(int delta)
{
	if (_vertScrollBar)
	{
		int val = _vertScrollBar->GetValue();
		val -= (delta * 3);
		_vertScrollBar->SetValue(val);
	}
}

// Called every frame the entry has keyboard focus; blinks the text cursor
void TextEntry::OnKeyFocusTicked()
{
	int time = vgui::system()->GetTimeMillis();
	if (time > _cursorNextBlinkTime)
	{
		_cursorBlink = !_cursorBlink;
		_cursorNextBlinkTime = time + _cursorBlinkRate;
		Repaint();
	}
}

// Check if we are selecting text (so we can highlight it)
void TextEntry::SelectCheck()
{
	if (!HasFocus() || !(vgui::input()->IsKeyDown(KEY_LSHIFT) || vgui::input()->IsKeyDown(KEY_RSHIFT)))
	{
		_select[0] = -1;
	}
	else if (_select[0] == -1)
	{
		_select[0] = _cursorPos;
	}
}

// Set the maximum number of chars in the text buffer
void TextEntry::SetMaximumCharCount(int maxChars)
{
	_maxCharCount = maxChars;
}

int TextEntry::GetMaximumCharCount()
{
	return _maxCharCount;
}

void TextEntry::SetAutoProgressOnHittingCharLimit(bool state)
{
	m_bAutoProgressOnHittingCharLimit = state;
}

// Set whether to wrap the text buffer
void TextEntry::SetWrap(bool wrap)
{
	_wrap = wrap;
}

// Set whether to pass newline msgs to parent
void TextEntry::SendNewLine(bool send)
{
	_sendNewLines = send;
}

// Tell if an index is a linebreakindex
bool TextEntry::IsLineBreak(int index)
{
	for (int i = 0; i < m_LineBreaks.Count(); ++i)
	{
		if (index == m_LineBreaks[i])
			return true;
	}

	return false;
}

// Move the cursor one character to the left, scroll the text horizontally if needed
void TextEntry::GotoLeft()
{
	SelectCheck();

	// if we are on a line break just move the cursor to the prev line
	if (IsLineBreak(_cursorPos))
	{
		// if we're already on the prev line at the end dont put it on the end
		if (!_cursorIsAtEnd)
			_putCursorAtEnd = true;
	}

	// if we are not at Start decrement cursor
	if (!_putCursorAtEnd && _cursorPos > 0)
	{
		_cursorPos--;
	}

	ScrollLeft();
	ResetCursorBlink();
	Repaint();
}

// Move the cursor one character to the right, scroll the text horizontally if needed
void TextEntry::GotoRight()
{
	SelectCheck();

	// if we are on a line break just move the cursor to the next line
	if (IsLineBreak(_cursorPos))
	{
		if (_cursorIsAtEnd)
		{
			_putCursorAtEnd = false;
		}
		else
		{
			// if we are not at end increment cursor
			if (_cursorPos < m_TextStream.Count())
			{
				_cursorPos++;
			}
		}
	}
	else
	{
		// if we are not at end increment cursor
		if (_cursorPos < m_TextStream.Count())
		{
			_cursorPos++;
		}

		// if we are on a line break move the cursor to end of line
		if (IsLineBreak(_cursorPos))
		{
			if (!_cursorIsAtEnd)
				_putCursorAtEnd = true;
		}
	}

	// scroll right if we need to
	ScrollRight();
	ResetCursorBlink();
	Repaint();
}

// Find out what line the cursor is on
int TextEntry::GetCursorLine()
{
	// find which line the cursor is on
	int cursorLine;
	for (cursorLine = 0; cursorLine < m_LineBreaks.Count(); cursorLine++)
	{
		if (_cursorPos < m_LineBreaks[cursorLine])
			break;
	}

	// correct for when cursor is at end of line rather than Start of next
	if (_putCursorAtEnd)
	{
		// we are not at end of buffer, in which case there is no next line to be at the Start of
		if (_cursorPos != m_TextStream.Count())
			cursorLine--;
	}

	return cursorLine;
}

// Move the cursor one line up
void TextEntry::GotoUp()
{
	SelectCheck();

	if (_cursorIsAtEnd)
	{
		// we are on first line
		if ((GetCursorLine() - 1) == 0)
		{
			// stay at end of line
			_putCursorAtEnd = true;

			// dont move the cursor
			return;
		}
		else
			_cursorPos--;
	}

	int cx, cy;
	CursorToPixelSpace(_cursorPos, cx, cy);

	// move the cursor to the previous line
	MoveCursor(GetCursorLine() - 1, cx);
}

// Move the cursor one line down
void TextEntry::GotoDown()
{
	SelectCheck();

	if (_cursorIsAtEnd)
	{
		_cursorPos--;
		if (_cursorPos < 0)
			_cursorPos = 0;
	}

	int cx, cy;
	CursorToPixelSpace(_cursorPos, cx, cy);

	// move the cursor to the next line
	MoveCursor(GetCursorLine() + 1, cx);
	if (!_putCursorAtEnd && _cursorIsAtEnd)
	{
		_cursorPos++;
		if (_cursorPos > m_TextStream.Count())
		{
			_cursorPos = m_TextStream.Count();
		}
	}

	LayoutVerticalScrollBarSlider();
}

// Set the starting ypixel positon for a walk through the window
int TextEntry::GetYStart()
{
	if (_multiline)
	{
		// just Start from the top
		return DRAW_OFFSET_Y;
	}

	int fontTall = vgui::surface()->GetFontTall(_font);
	return (GetTall() / 2) - (fontTall / 2);
}

// Move the cursor to a line, need to know how many pixels are in a line
void TextEntry::MoveCursor(int line, int pixelsAcross)
{
	// clamp to a valid line
	if (line < 0)
		line = 0;

	if (line >= m_LineBreaks.Count())
		line = m_LineBreaks.Count() - 1;

	// walk the whole text set looking for our place
	// work out where to Start checking
	int yStart = GetYStart();

	int x = DRAW_OFFSET_X, y = yStart;
	int lineBreakIndexIndex = 0;
	_pixelsIndent = 0;
	int i;
	for (i = 0; i < m_TextStream.Count(); i++)
	{
		wchar_t ch = m_TextStream[i];

		if (_hideText)
		{
			ch = '*';
		}

		// if we've passed a line break go to that
		if (m_LineBreaks[lineBreakIndexIndex] == i)
		{
			if (lineBreakIndexIndex == line)
			{
				_putCursorAtEnd = true;
				_cursorPos = i;
				break;
			}

			// add another line
			AddAnotherLine(x, y);
			lineBreakIndexIndex++;

		}

		// add to the current position
		int charWidth = getCharWidth(_font, ch);
		if (line == lineBreakIndexIndex)
		{
			// check to see if we're in range
			if ((x + (charWidth / 2)) > pixelsAcross)
			{
				// found position
				_cursorPos = i;
				break;
			}
		}

		x += charWidth;
	}

	// if we never find the cursor it must be past the end
	// of the text buffer, to let's just slap it on the end of the text buffer then.
	if (i == m_TextStream.Count())
	{
		GotoTextEnd();
	}

	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Turn horizontal scrolling on or off.
// Horizontal scrolling is disabled in multline windows.
// Toggling this will disable it in single line windows as well.
void TextEntry::SetHorizontalScrolling(bool status)
{
	_horizScrollingAllowed = status;
}

// Horizontal scrolling function, not used in multiline windows
// Function will scroll the buffer to the left if the cursor is not in the window
// scroll left if we need to
void TextEntry::ScrollLeft()
{
	// early out
	if (_multiline)
	{
		return;
	}

	// early out
	if (!_horizScrollingAllowed)
	{
		return;
	}

	// scroll left if we need to
	if (_cursorPos < _currentStartIndex)
	{
		// dont scroll past the Start of buffer
		if (_cursorPos < 0)
		{
			_cursorPos = 0;
		}
		_currentStartIndex = _cursorPos;
	}

	LayoutVerticalScrollBarSlider();
}

void TextEntry::ScrollLeftForResize()
{
	// early out
	if (_multiline)
	{
		return;
	}

	// early out
	if (!_horizScrollingAllowed)
	{
		return;
	}

	// go until we hit leftmost
	while (_currentStartIndex > 0)
	{
		_currentStartIndex--;
		int nVal = _currentStartIndex;

		// check if the cursor is now off the screen
		if (IsCursorOffRightSideOfWindow(_cursorPos))
		{
			_currentStartIndex++;	// we've gone too far, return it
			break;
		}

		// IsCursorOffRightSideOfWindow actually fixes the _currentStartIndex,
		// so if our value changed that menas we really are off the screen
		if (nVal != _currentStartIndex)
			break;
	}

	LayoutVerticalScrollBarSlider();
}

// Horizontal scrolling function, not used in multiline windows
// Scroll one char right until the cursor is visible in the window.
// We do this one char at a time because char width isn't a constant.
void TextEntry::ScrollRight()
{
	if (!_horizScrollingAllowed)
		return;

	if (_multiline)
	{
	}
	// check if cursor is off the right side of window
	else if (IsCursorOffRightSideOfWindow(_cursorPos))
	{
		_currentStartIndex++;	// scroll over
		ScrollRight();		// scroll again, check if cursor is in window yet
	}

	LayoutVerticalScrollBarSlider();
}

// Check and see if cursor position is off the right side of the window just compare cursor's pixel coords with the window size coords.
// an integer cursor Position, if you pass _cursorPos fxn will tell you if current cursor is outside window.
//
// true: cursor is outside right edge or window
// false: cursor is inside right edge
bool TextEntry::IsCursorOffRightSideOfWindow(int cursorPos)
{
	int cx, cy;
	CursorToPixelSpace(cursorPos, cx, cy);

	// width of inside of window is GetWide() - 1
	int wx = GetWide() - 1;
	if (wx < 0)
		return false;

	return (cx >= wx);
}

// Check and see if cursor position is off the left side of the window just compare cursor's pixel coords with the window size coords.
// an integer cursor Position, if you pass _cursorPos fxn will tell you if current cursor is outside window.
//
// true - cursor is outside left edge or window
// false - cursor is inside left edge
bool TextEntry::IsCursorOffLeftSideOfWindow(int cursorPos)
{
	int cx, cy;
	CursorToPixelSpace(cursorPos, cx, cy);
	return (cx <= 0);
}

// Move the cursor over to the Start of the next word to the right
void TextEntry::GotoWordRight()
{
	SelectCheck();

	// search right until we hit a whitespace character or a newline
	while (++_cursorPos < m_TextStream.Count())
	{
		if (iswspace(m_TextStream[_cursorPos]))
			break;
	}

	// search right until we hit an nonspace character
	while (++_cursorPos < m_TextStream.Count())
	{
		if (!iswspace(m_TextStream[_cursorPos]))
			break;
	}

	if (_cursorPos > m_TextStream.Count())
		_cursorPos = m_TextStream.Count();

	// now we are at the start of the next word
	// scroll right if we need to
	ScrollRight();
	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Move the cursor over to the Start of the next word to the left
void TextEntry::GotoWordLeft()
{
	SelectCheck();

	if (_cursorPos < 1)
		return;

	// search left until we hit an nonspace character
	while (--_cursorPos >= 0)
	{
		if (!iswspace(m_TextStream[_cursorPos]))
			break;
	}

	// search left until we hit a whitespace character
	while (--_cursorPos >= 0)
	{
		if (iswspace(m_TextStream[_cursorPos]))
		{
			break;
		}
	}

	// we end one character off
	_cursorPos++;

	// now we are at the Start of the previous word
	// scroll left if we need to
	ScrollLeft();
	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Move cursor to the Start of the text buffer
void TextEntry::GotoTextStart()
{
	SelectCheck();
	_cursorPos = 0;			// set cursor to Start
	_putCursorAtEnd = false;
	_currentStartIndex = 0;		// scroll over to Start

	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Move cursor to the end of the text buffer
void TextEntry::GotoTextEnd()
{
	SelectCheck();
	_cursorPos = m_TextStream.Count();	// set cursor to end of buffer
	_putCursorAtEnd = true;			// move cursor Start of next line
	ScrollRight();				// scroll over until cursor is on screen

	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Move cursor to the Start of the current line
void TextEntry::GotoFirstOfLine()
{
	SelectCheck();

	// to get to the Start of the line you have to take into account line wrap
	// we have to figure out at which point the line wraps
	// given the current cursor position, select[1], find the index that is the
	// line Start to the left of the cursor
	// _cursorPos = 0; // TODO: this is wrong, should go to first non-whitespace first, then to zero
	_cursorPos = GetCurrentLineStart();
	_putCursorAtEnd = false;

	_currentStartIndex=_cursorPos;

	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Get the index of the first char on the current line
int TextEntry::GetCurrentLineStart()
{
	// quick out for non multline buffers
	if (!_multiline)
		return _currentStartIndex;

	int i;
	if (IsLineBreak(_cursorPos))
	{
		for (i = 0; i < m_LineBreaks.Count(); ++i)
		{
			if (_cursorPos == m_LineBreaks[i])
				break;
		}
		if (_cursorIsAtEnd)
		{
			if (i > 0)
			{
				return m_LineBreaks[i - 1];
			}
			return m_LineBreaks[0];
		}
		else
		{
			// we are already at Start
			return _cursorPos;
		}
	}

	for (i = 0; i < m_LineBreaks.Count(); ++i)
	{
		if (_cursorPos < m_LineBreaks[i])
		{
			if (i == 0)
				return 0;
			else
				return m_LineBreaks[i - 1];
		}
	}

	// if there were no line breaks, the first char in the line is the Start of the buffer
	return 0;
}

// Move cursor to the end of the current line
void TextEntry::GotoEndOfLine()
{
	SelectCheck();

	// to get to the end of the line you have to take into account line wrap in the buffer
	// we have to figure out at which point the line wraps
	// given the current cursor position, select[1], find the index that is the
	// line end to the right of the cursor
	// _cursorPos = m_TextStream.Count(); // TODO: this is wrong, should go to last non-whitespace, then to true EOL
	_cursorPos = GetCurrentLineEnd();
	_putCursorAtEnd = true;

	ScrollRight();
	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Get the index of the last char on the current line
int TextEntry::GetCurrentLineEnd()
{
	int i;
	if (IsLineBreak(_cursorPos))
	{
		for (i = 0; i < m_LineBreaks.Count() - 1; ++i)
		{
			if (_cursorPos == m_LineBreaks[i])
				break;
		}
		if (!_cursorIsAtEnd)
		{
			if (i == m_LineBreaks.Count() - 2)
				m_TextStream.Count();
			else
				return m_LineBreaks[i + 1];
		}
		else
			return _cursorPos; // we are already at end
	}

	for (i = 0; i < m_LineBreaks.Count() - 1; i++)
	{
		if (_cursorPos < m_LineBreaks[i])
		{
			return m_LineBreaks[i];
		}
	}

	return m_TextStream.Count();
}

// Insert a character into the text buffer
void TextEntry::InsertChar(wchar_t ch)
{
	// throw away redundant linefeed characters
	if (ch == '\r')
		return;

	// no newline characters in single-line dialogs
	if (!_multiline && ch == '\n')
		return;

	// no tab characters
	if (ch == '\t')
		return;

	if (m_bAllowNumericInputOnly)
	{
		if (!iswdigit(ch) && ((char)ch != '.'))
		{
			vgui::surface()->PlaySound("Resource\\warning.wav");
			return;
		}
	}

	// check against unicode characters
	if (!m_bAllowNonAsciiCharacters)
	{
		if (ch > 127)
			return;
	}

	// don't add characters if the max char count has been reached
	// ding at the user
	if (_maxCharCount > -1 && m_TextStream.Count() >= _maxCharCount)
	{
		if (_maxCharCount > 0 && _multiline && _wrap)
		{
			// if we wrap lines rather than stopping
			while (m_TextStream.Count() > _maxCharCount)
			{
				if (_recalculateBreaksIndex == 0)
				{
					// we can get called before this has been run for the first time :)
					RecalculateLineBreaks();
				}

				if (m_LineBreaks[0] > m_TextStream.Count())
				{
					// if the line break is the past the end of the buffer recalc
					_recalculateBreaksIndex = -1;
					RecalculateLineBreaks();
				}

				if (m_LineBreaks[0] + 1 < m_TextStream.Count())
				{
					// delete the line
					m_TextStream.RemoveMultiple(0, m_LineBreaks[0]);

					// in case we just deleted text from where the cursor is
					if (_cursorPos > m_TextStream.Count())
					{
						_cursorPos = m_TextStream.Count();
					}
					// shift the cursor up. don't let it wander past zero
					else
					{
						_cursorPos -= m_LineBreaks[0] + 1;
						if (_cursorPos < 0)
						{
							_cursorPos = 0;
						}
					}

					// move any selection area up
					if (_select[0] > -1)
					{
						_select[0] -= m_LineBreaks[0] + 1;

						if (_select[0] <= 0)
						{
							_select[0] = -1;
						}

						_select[1] -= m_LineBreaks[0] + 1;
						if (_select[1] <= 0)
						{
							_select[1] = -1;
						}
					}

					// now redraw the buffer
					for (int i = m_TextStream.Count() - 1; i >= 0; i--)
					{
						SetCharAt(m_TextStream[i], i + 1);
					}

					// redo all the line breaks
					_recalculateBreaksIndex = -1;
					RecalculateLineBreaks();
				}
			}
		}
		else
		{
			// make a sound
			// we've hit the max character limit
			vgui::surface()->PlaySound("Resource\\warning.wav");
			return;
		}
	}

	if (_wrap)
	{
		// when wrapping you always insert the new char at the end of the buffer
		SetCharAt(ch, m_TextStream.Count());
		_cursorPos = m_TextStream.Count();
	}
	else
	{
		// move chars right 1 starting from cursor, then replace cursorPos with char and increment cursor
		for (int i = m_TextStream.Count() - 1; i >= _cursorPos; i--)
		{
			SetCharAt(m_TextStream[i], i + 1);
		}

		SetCharAt(ch, _cursorPos);
		_cursorPos++;
	}

	// if its a newline char we can't do the slider until we recalc the line breaks
	if (ch == '\n')
	{
		RecalculateLineBreaks();
	}

	// see if we've hit the char limit
	if (m_bAutoProgressOnHittingCharLimit && m_TextStream.Count() == _maxCharCount)
	{
		// move the next panel (most likely another TextEntry)
		RequestFocusNext();
	}

	// scroll right if this pushed the cursor off screen
	ScrollRight();

	_dataChanged = true;

	CalcBreakIndex();
	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Get the lineBreakIndex index of the line before the cursor
// note _recalculateBreaksIndex < 0 flags RecalculateLineBreaks
// to figure it all out from scratch
void TextEntry::CalcBreakIndex()
{
	// an optimization to handle when the cursor is at the end of the buffer.
	// pays off if the buffer is large, and the search loop would be long.
	if (_cursorPos == m_TextStream.Count())
	{
		// we know m_LineBreaks array always has at least one element in it (99999 sentinel)
		// when there is just one line this will make recalc = -1 which is ok.
		_recalculateBreaksIndex = m_LineBreaks.Count() - 2;
		return;
	}

	_recalculateBreaksIndex = 0;

	// find the line break just before the cursor position
	while (_cursorPos > m_LineBreaks[_recalculateBreaksIndex])
		++_recalculateBreaksIndex;

	// -1 is ok.
	--_recalculateBreaksIndex;
}

// Insert a string into the text buffer, this is just a series
// of char inserts because we have to check each char is ok to insert
void TextEntry::InsertString(wchar_t *wszText)
{
	SaveUndoState();

	for (const wchar_t *ch = wszText; *ch != '\0'; ++ch)
	{
		InsertChar(*ch);
	}

	if (_dataChanged)
	{
		FireActionSignal();
	}
}

// Converts an ansi string to unicode and inserts it into the text stream
void TextEntry::InsertString(const char *text)
{
	// check for to see if the string is in the localization tables
	if (text[0] == '#')
	{
		wchar_t *wsz = vgui::localize()->Find(text);
		if (wsz)
		{
			InsertString(wsz);
			return;
		}
	}

	// straight convert the ansi to unicode and insert
	wchar_t unicode[1024];
	vgui::localize()->ConvertANSIToUnicode(text, unicode, sizeof(unicode));
	InsertString(unicode);
}

// Handle the effect of user hitting backspace key
// we delete the char before the cursor and reformat the text so it behaves like in windows.
void TextEntry::Backspace()
{
	if (!IsEditable())
		return;

	//if you are at the first position don't do anything
	if (_cursorPos == 0)
	{
		return;
	}

	//if the line is empty, don't do anything
	if (m_TextStream.Count() == 0)
	{
		return;
	}

	SaveUndoState();

	//shift chars left one, starting at the cursor position, then make the line one smaller
	for (int i = _cursorPos; i < m_TextStream.Count(); ++i)
	{
		SetCharAt(m_TextStream[i], i - 1);
	}

	m_TextStream.Remove(m_TextStream.Count() - 1);

	// As we hit the Start of the window, expose more chars so we can see what we are deleting
	if (_cursorPos == _currentStartIndex)
	{
		// windows tabs over 6 chars
		// dont scroll if there are not enough chars to scroll
		if (_currentStartIndex - 6 >= 0)
		{
			_currentStartIndex -= 6;
		}
		else
			_currentStartIndex = 0;
	}

	// move the cursor left one
	_cursorPos--;
	_dataChanged = true;

	// recalculate linebreaks (the fast incremental linebreak function doesn't work in this case)
	// _recalculateBreaksIndex = 0;
	// m_LineBreaks.RemoveAll();
	// m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);

	CalcBreakIndex();
	LayoutVerticalScrollBarSlider();
	ResetCursorBlink();
	Repaint();
}

// Deletes the current selection, if any, moving the cursor to the Start of the selection
void TextEntry::DeleteSelected()
{
	if (!IsEditable())
		return;

	// if the line is empty, don't do anything
	if (m_TextStream.Count() == 0)
		return;

	// get the range to delete
	int x0, x1;
	if (!GetSelectedRange(x0, x1))
	{
		// no selection, don't touch anything
		return;
	}

	SaveUndoState();

	// shift chars left one starting after cursor position, then make the line one smaller
	int dif = x1 - x0;
	for (int i = 0; i < dif; ++i)
	{
		m_TextStream.Remove(x0);
	}

	// clear any selection
	SelectNone();
	ResetCursorBlink();

	// move the cursor to just after the deleted section
	_cursorPos = x0;
	_dataChanged = true;

	_recalculateBreaksIndex = 0;
	m_LineBreaks.RemoveAll();
	m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);

	CalcBreakIndex();
	LayoutVerticalScrollBarSlider();
}

// Handle the effect of the user hitting the delete key removes the char in front of the cursor
void TextEntry::Delete()
{
	if (!IsEditable())
		return;

	// if the line is empty, don't do anything
	if (m_TextStream.Count() == 0)
		return;

	// get the range to delete
	int x0, x1;
	if (!GetSelectedRange(x0, x1))
	{
		// no selection, so just delete the one character
		x0 = _cursorPos;
		x1 = x0 + 1;

		// if we're at the end of the line don't do anything
		if (_cursorPos >= m_TextStream.Count())
			return;
	}

	SaveUndoState();

	// shift chars left one starting after cursor position, then make the line one smaller
	int dif = x1 - x0;
	for (int i = 0; i < dif; i++)
	{
		m_TextStream.Remove(x0);
	}

	ResetCursorBlink();

	// clear any selection
	SelectNone();

	// move the cursor to just after the deleted section
	_cursorPos = x0;
	_dataChanged = true;

	_recalculateBreaksIndex = 0;
	m_LineBreaks.RemoveAll();
	m_LineBreaks.AddToTail(MAX_BUFFER_SIZE);

	CalcBreakIndex();
	LayoutVerticalScrollBarSlider();
}

// Declare a selection empty
void TextEntry::SelectNone()
{
	// tag the selection as empty
	_select[0] = -1;
	Repaint();
}

// Load in the selection range so cx0 is the Start and cx1 is the end from smallest to highest (right to left)
bool TextEntry::GetSelectedRange(int &cx0, int &cx1)
{
	// if there is nothing selected return false
	if (_select[0] == -1)
	{
		return false;
	}

	// sort the two position so cx0 is the smallest
	cx0 = _select[0];
	cx1 = _select[1];

	int temp;
	if (cx1 < cx0)
	{
		temp = cx0;
		cx0 = cx1;
		cx1 = temp;
	}

	return true;
}

// Opens the cut/copy/paste dropdown menu
void TextEntry::OpenEditMenu()
{
	// get cursor position, this is local to this text edit window
	int cursorX, cursorY;
	vgui::input()->GetCursorPos(cursorX, cursorY);

	/* !!	disabled since it recursively gets panel pointers, potentially across dll boundaries,
			and doesn't need to be necessary (it's just for handling windowed mode)

	// find the frame that has no parent (the one on the desktop)
	Panel *panel = this;
	while (panel->GetParent() != NULL)
	{
		panel = panel->GetParent();
	}
	panel->ScreenToLocal(cursorX, cursorY);
	int x, y;
	// get base panel's postition
	panel->GetPos(x, y);

	// adjust our cursor position accordingly
	cursorX += x;
	cursorY += y;
	*/

	int x0, x1;

	// there is something selected
	if (GetSelectedRange(x0, x1))
	{
		m_pEditMenu->SetItemEnabled("&Cut", true);
		m_pEditMenu->SetItemEnabled("C&opy", true);
	}
	// there is nothing selected, disable cut/copy options
	else
	{
		m_pEditMenu->SetItemEnabled("&Cut", false);
		m_pEditMenu->SetItemEnabled("C&opy", false);
	}

	m_pEditMenu->SetVisible(true);
	m_pEditMenu->RequestFocus();

	// relayout the menu immediately so that we know it's size
	m_pEditMenu->InvalidateLayout(true);

	int menuWide, menuTall;
	m_pEditMenu->GetSize(menuWide, menuTall);

	// work out where the cursor is and therefore the best place to put the menu
	int wide, tall;
	vgui::surface()->GetScreenSize(wide, tall);

	if (wide - menuWide > cursorX)
	{
		// menu hanging right
		if (tall - menuTall > cursorY)
		{
			// menu hanging down
			m_pEditMenu->SetPos(cursorX, cursorY);
		}
		else
		{
			// menu hanging up
			m_pEditMenu->SetPos(cursorX, cursorY - menuTall);
		}
	}
	else
	{
		// menu hanging left
		if (tall - menuTall > cursorY)
		{
			// menu hanging down
			m_pEditMenu->SetPos(cursorX - menuWide, cursorY);
		}
		else
		{
			// menu hanging up
			m_pEditMenu->SetPos(cursorX - menuWide, cursorY - menuTall);
		}
	}

	m_pEditMenu->RequestFocus();
}

// Cuts the selected chars from the buffer and copies them into the clipboard
void TextEntry::CutSelected()
{
	CopySelected();
	DeleteSelected();

	// have to request focus if we used the menu
	RequestFocus();

	if (_dataChanged)
	{
		FireActionSignal();
	}
}

// Copies the selected chars into the clipboard
void TextEntry::CopySelected()
{
	if (_hideText)
		return;

	int x0, x1;
	if (GetSelectedRange(x0, x1))
	{
		CUtlVector<wchar_t> buf;
		for (int i = x0; i < x1; i++)
		{
			if (m_TextStream[i] == '\n')
			{
				buf.AddToTail('\r');
			}
			buf.AddToTail(m_TextStream[i]);
		}

		buf.AddToTail('\0');
		vgui::system()->SetClipboardText(buf.Base(), x1 - x0);
	}

	// have to request focus if we used the menu
	RequestFocus();

	if (_dataChanged)
	{
		FireActionSignal();
	}
}

// Pastes the selected chars from the clipboard into the text buffer truncates if text is longer than our _maxCharCount
void TextEntry::Paste()
{
	if (_hideText)
		return;

	if (!IsEditable())
		return;

	CUtlVector<wchar_t> buf;
	int bufferSize = vgui::system()->GetClipboardTextCount();
	if (!m_bAutoProgressOnHittingCharLimit)
	{
		bufferSize = _maxCharCount > 0 ? _maxCharCount + 1 : vgui::system()->GetClipboardTextCount(); // +1 for terminator
	}

	buf.AddMultipleToTail(bufferSize);
	int len = vgui::system()->GetClipboardText(0, buf.Base(), bufferSize * sizeof(wchar_t));
	if (len < 1)
		return;

	SaveUndoState();
	bool bHaveMovedFocusAwayFromCurrentEntry = false;

	// insert all the characters
	for (int i = 0; i < len && buf[i] != 0; i++)
	{
		if (m_bAutoProgressOnHittingCharLimit)
		{
			// see if we're about to hit the char limit
			if (m_TextStream.Count() == _maxCharCount)
			{
				// move the next panel (most likely another TextEntry)
				RequestFocusNext();

				// copy the remainder into the clipboard
				wchar_t *remainingText = &buf[i];
				vgui::system()->SetClipboardText(remainingText, len - i - 1);

				// set the next entry to paste
				if (GetVParent() && vgui::ipanel()->GetCurrentKeyFocus(GetVParent()) != GetVPanel())
				{
					bHaveMovedFocusAwayFromCurrentEntry = true;
					vgui::ipanel()->SendMessage(vgui::ipanel()->GetCurrentKeyFocus(GetVParent()), new KeyValues("DoPaste"), GetVPanel());
				}
				break;
			}
		}

		// insert the character
		InsertChar(buf[i]);
	}

	// restore the original clipboard text if neccessary
	if (m_bAutoProgressOnHittingCharLimit)
	{
		vgui::system()->SetClipboardText(buf.Base(), bufferSize);
	}

	_dataChanged = true;
	FireActionSignal();

	if (!bHaveMovedFocusAwayFromCurrentEntry)
	{
		// have to request focus if we used the menu
		RequestFocus();
	}
}

// Reverts back to last saved changes
void TextEntry::Undo()
{
	_cursorPos = _undoCursorPos;
	m_TextStream.CopyArray(m_UndoTextStream.Base(), m_UndoTextStream.Count());

	InvalidateLayout();
	Repaint();
	SelectNone();
}

// Saves the current state to the undo stack
void TextEntry::SaveUndoState()
{
	_undoCursorPos = _cursorPos;
	m_UndoTextStream.CopyArray(m_TextStream.Base(), m_TextStream.Count());
}

// Returns the index in the text buffer of the character the drawing should Start at
int TextEntry::GetStartDrawIndex(int &lineBreakIndexIndex)
{
	int startIndex = 0;
	int numLines = m_LineBreaks.Count();
	int startLine = 0;

	// determine the Start point from the scroll bar
	// do this only if we are not selecting text in the window with the mouse
	if (_vertScrollBar && !_mouseDragSelection)
	{
		// skip to line indicated by scrollbar
		startLine = _vertScrollBar->GetValue();
	}
	else
	{
		// check to see if the cursor is off the screen-multiline case
		HFont font = _font;
		int displayLines = GetTall() / (vgui::surface()->GetFontTall(font) + DRAW_OFFSET_Y);
		if (displayLines < 1)
		{
			displayLines = 1;
		}
		if (numLines > displayLines)
		{
			int cursorLine = GetCursorLine();
			startLine = _currentStartLine;

			// see if that is visible
			if (cursorLine < _currentStartLine)
			{
				// cursor is above visible area; scroll back
				startLine = cursorLine;
				if (_vertScrollBar)
				{
					// should be calibrated for speed
					MoveScrollBar(1);

					// adjust startline incase we hit a limit
					startLine = _vertScrollBar->GetValue();
				}
			}
			else if (cursorLine > (_currentStartLine + displayLines - 1))
			{
				// cursor is down below visible area; scroll forward
				startLine = cursorLine - displayLines + 1;
				if (_vertScrollBar)
				{
					MoveScrollBar(-1);
					startLine = _vertScrollBar->GetValue();
				}
			}
		}
		else if (!_multiline)
		{
			// check to see if cursor is off the right side of screen-single line case
			// get cursor's x coordinate in pixel space
			int x = DRAW_OFFSET_X;
			for (int i = _currentStartIndex; i < m_TextStream.Count(); i++)
			{
				wchar_t ch = m_TextStream[i];
				if (_hideText)
				{
					ch = '*';
				}

				// if we've found the position, break
				if (_cursorPos == i)
				{
					break;
				}

				// add to the current position
				x += getCharWidth(font, ch);
			}

			if (x >= GetWide())
			{
				_currentStartIndex++;
				// Keep searching...
				continue;
			}

			if (x <= 0)
			{
				// dont go past the Start of buffer
				if (_currentStartIndex > 0)
					_currentStartIndex--;
			}
		}
	}

	if (startLine > 0)
	{
		lineBreakIndexIndex = startLine;
		if (startLine && startLine < m_LineBreaks.Count())
		{
			startIndex = m_LineBreaks[startLine - 1];
		}
	}

	if (!_horizScrollingAllowed)
		return 0;

	_currentStartLine = startLine;
	if (_multiline)
		return startIndex;
	else
		return _currentStartIndex;
}

// Get a string from text buffer
// offset - index to Start reading from
// bufLen - length of string
void TextEntry::GetText(char *buf, int bufLen)
{
	if (m_TextStream.Count())
	{
		// temporarily null terminate the text stream so we can use the conversion function
		int nullTerminatorIndex = m_TextStream.AddToTail((wchar_t)0);
		vgui::localize()->ConvertUnicodeToANSI(m_TextStream.Base(), buf, bufLen);
		m_TextStream.FastRemove(nullTerminatorIndex);
	}
	else
	{
		// no characters in the stream
		buf[0] = '\0';
	}
}

// Get a string from text buffer
// offset - index to Start reading from
// bufLen - length of string
void TextEntry::GetText(wchar_t *wbuf, int bufLenInBytes)
{
	int len = m_TextStream.Count();
	if (m_TextStream.Count())
	{
		int terminator = min(len, (bufLenInBytes / (int)sizeof(wchar_t)) - 1);
		wcsncpy(wbuf, m_TextStream.Base(), terminator);
		wbuf[terminator] = '\0';
	}
	else
	{
		wbuf[0] = '\0';
	}
}

// Sends a message that the text has changed
void TextEntry::FireActionSignal()
{
	PostActionSignal(new KeyValues("TextChanged"));
	_dataChanged = false; // reset the data changed flag
	InvalidateLayout();
}

// Set the font of the buffer text
// font to change to
void TextEntry::SetFont(HFont font)
{
	_font = font;
	InvalidateLayout();
	Repaint();
}

// Called when the scrollbar slider is moved
void TextEntry::OnSliderMoved()
{
	Repaint();
}

bool TextEntry::RequestInfo(KeyValues *outputData)
{
	if (!stricmp(outputData->GetName(), "GetText"))
	{
		wchar_t wbuf[256];
		GetText(wbuf, 255);
		outputData->SetWString("text", wbuf);
		return true;
	}
	else if (!stricmp(outputData->GetName(), "GetState"))
	{
		wchar_t wbuf[64];
		GetText(wbuf, sizeof(wbuf));
		outputData->SetInt("state", _wtoi(wbuf));
		return true;
	}

	return BaseClass::RequestInfo(outputData);
}

void TextEntry::OnSetText(const wchar_t *text)
{
	SetText(text);
}

// as above, but sets an integer
void TextEntry::OnSetState(int state)
{
	char buf[64];
	Q_snprintf(buf, sizeof(buf), "%d", state);
	SetText(buf);
}

void TextEntry::ApplySettings(KeyValues *inResourceData)
{
	BaseClass::ApplySettings(inResourceData);
//	_font = scheme()->GetFont(GetScheme(), "Default", IsProportional());
//	SetFont(_font);

	SetTextHidden((bool)inResourceData->GetInt("textHidden", 0));
	SetEditable((bool)inResourceData->GetInt("editable", 1));
	SetMaximumCharCount(inResourceData->GetInt("maxchars", -1));
	SetAllowNumericInputOnly(inResourceData->GetInt("NumericInputOnly", 0));
	SetAllowNonAsciiCharacters(inResourceData->GetInt("unicode", 0));
}

void TextEntry::GetSettings(KeyValues *outResourceData)
{
	BaseClass::GetSettings(outResourceData);
	outResourceData->SetInt("textHidden", _hideText);
	outResourceData->SetInt("editable", IsEditable());
	outResourceData->SetInt("maxchars", GetMaximumCharCount());
	outResourceData->SetInt("NumericInputOnly", m_bAllowNumericInputOnly);
	outResourceData->SetInt("unicode", m_bAllowNonAsciiCharacters);
}

const char *TextEntry::GetDescription()
{
	static char buf[1024];
	Q_snprintf(buf, sizeof(buf), "%s, bool textHidden, bool editable, bool unicode, bool NumericInputOnly, int maxchars", BaseClass::GetDescription());
	return buf;
}

// Get the number of lines in the window
int TextEntry::GetNumLines()
{
	return m_LineBreaks.Count();
}

// Sets the height of the text entry window so all text will fit inside
void TextEntry::SetToFullHeight()
{
	PerformLayout();
	int wide, tall;
	GetSize(wide, tall);

	tall = GetNumLines() * (vgui::surface()->GetFontTall(_font) + DRAW_OFFSET_Y) + DRAW_OFFSET_Y + 2;
	SetSize(wide, tall);
	PerformLayout();
}

// Select all the text.
void TextEntry::SelectAllText(bool bResetCursorPos)
{
	if (bResetCursorPos)
	{
		_cursorPos = 0;
	}

	// if there's no text at all, select none
	if (m_TextStream.Count() == 0)
	{
		_select[0] = -1;
	}
	else
	{
		_select[0] = 0;
	}

	_select[1] = m_TextStream.Count();
}

// Select no text.
void TextEntry::SelectNoText()
{
	_select[0] = -1;
	_select[1] = 0;
}

// Sets the width of the text entry window so all text will fit inside
void TextEntry::SetToFullWidth()
{
	// probably be problems if you try using this on multi line buffers
	// or buffers with clickable text in them.
	if (_multiline)
		return;

	PerformLayout();
	int wide = 2 * DRAW_OFFSET_X; // buffer on left and right end of text.

	// loop through all the characters and sum their widths
	for (int i = 0; i < m_TextStream.Count(); ++i)
	{
		wide += getCharWidth(_font, m_TextStream[i]);
	}

	// height of one line of text
	int tall = (vgui::surface()->GetFontTall(_font) + DRAW_OFFSET_Y) + DRAW_OFFSET_Y + 2;
	SetSize(wide, tall);
	PerformLayout();
}

void TextEntry::SelectAllOnFirstFocus(bool status)
{
	_selectAllOnFirstFocus = status;
}

// Called when the text entry receives focus
void TextEntry::OnSetFocus()
{
	// see if we should highlight all on selection
	if (_selectAllOnFirstFocus)
	{
		_select[1] = m_TextStream.Count();
		_select[0] = 0;

		// cursor at end of line
		_cursorPos = _select[1];
		_selectAllOnFirstFocus = false;
	}
	else if (!vgui::input()->IsMouseDown(MOUSE_LEFT) && !vgui::input()->IsMouseDown(MOUSE_RIGHT))
	{
		// if we've tabbed to this field then move to the end of the text
		GotoTextEnd();

		// clear any selection
		SelectNone();
	}

	BaseClass::OnSetFocus();
}

// Set the width we have to draw text in.
// Do not use in multiline windows.
void TextEntry::SetDrawWidth(int width)
{
	_drawWidth = width;
}

// Get the width we have to draw text in.
int TextEntry::GetDrawWidth()
{
	return _drawWidth;
}

void TextEntry::SetAllowNonAsciiCharacters(bool state)
{
	m_bAllowNonAsciiCharacters = state;
}

void TextEntry::SetAllowNumericInputOnly(bool state)
{
	m_bAllowNumericInputOnly = state;
}

int TextEntry::GetTextLength() const
{
	return m_TextStream.Count();
}

void TextEntry::SetUseFallbackFont(bool bState, HFont hFallback)
{
	m_bUseFallbackFont = bState;
	m_hFallbackFont = hFallback;
}
