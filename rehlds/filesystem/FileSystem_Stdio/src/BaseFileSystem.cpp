/*
*
*    This program is free software; you can redistribute it and/or modify it
*    under the terms of the GNU General Public License as published by the
*    Free Software Foundation; either version 2 of the License, or (at
*    your option) any later version.
*
*    This program is distributed in the hope that it will be useful, but
*    WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
*    General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program; if not, write to the Free Software Foundation,
*    Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*    In addition, as a special exception, the author gives permission to
*    link the code of this program with the Half-Life Game Engine ("HL
*    Engine") and Modified Game Libraries ("MODs") developed by Valve,
*    L.L.C ("Valve").  You must obey the GNU General Public License in all
*    respects for all of the code used other than the HL Engine and MODs
*    from Valve.  If you modify this file, you may extend this exception
*    to your version of the file, but you are not obligated to do so.  If
*    you do not wish to do so, delete this exception statement from your
*    version.
*
*/

#include "precompiled.h"

float g_flDummyFloat = 1.0f;
CBaseFileSystem *CBaseFileSystem::s_pFileSystem = nullptr;

CBaseFileSystem *BaseFileSystem()
{
	return CBaseFileSystem::s_pFileSystem;
}

CBaseFileSystem::CBaseFileSystem()
{
	s_pFileSystem = this;

	m_fwLevel = FILESYSTEM_WARNING_REPORTUNCLOSED;
	m_pfnWarning = nullptr;
	m_nOpenCount = 0;
}

void CBaseFileSystem::Mount()
{
	g_flDummyFloat += 1.0f;
}

void CBaseFileSystem::Unmount()
{
	;
}

void CBaseFileSystem::CreateDirHierarchy(const char *path, const char *pathID)
{
	CSearchPath *searchPath = GetWritePath(pathID);
	int len = strlen(searchPath->GetPath().String()) + strlen(path);
	char *buf = (char *)alloca((len + 1) * sizeof(char));

	strcpy(buf, searchPath->GetPath().String());
	strcat(buf, path);

	FixSlashes(buf);

	char *s = buf;
	char *end = &buf[len];
	while (s < end)
	{
		if (*s == CORRECT_PATH_SEPARATOR) {
			*s = '\0';
#ifdef _WIN32
			_mkdir(buf);
#else
			mkdir(buf, S_IRWXU | S_IRGRP | S_IROTH); // owner has rwx, rest have r
#endif // _WIN32

			*s = CORRECT_PATH_SEPARATOR;
		}

		s++;
	}

#ifdef _WIN32
	_mkdir(buf);
#else
	mkdir(buf, S_IRWXU | S_IRGRP | S_IROTH); // owner has rwx, rest have r
#endif // _WIN32
}

void CBaseFileSystem::PrintOpenedFiles()
{
	Trace_DumpUnclosedFiles();
}

bool CBaseFileSystem::GetCurrentDirectory(char *pDirectory, int maxlen)
{
#ifdef _WIN32
	if (!::GetCurrentDirectoryA(maxlen, pDirectory))
#else
	if (!getcwd(pDirectory, maxlen))
#endif // _WIN32
		return false;

	FixSlashes(pDirectory);

	// Strip the last slash
	int len = strlen(pDirectory);
	if (pDirectory[len - 1] == CORRECT_PATH_SEPARATOR) {
		pDirectory[len - 1] = '\0';
	}

	return true;
}

void CBaseFileSystem::AddSearchPathNoWrite(const char *pPath, const char *pathID)
{
	AddSearchPathInternal(pPath, pathID, false);
}

void CBaseFileSystem::AddSearchPath(const char *pPath, const char *pathID)
{
	AddSearchPathInternal(pPath, pathID, true);
}

// This is where search paths are created. Map files are created at head of list
// (they occur after file system paths have already been set) so they get highest priority.
// Otherwise, we add the disk (non-packfile) path and then the paks if they exist for the path.
void CBaseFileSystem::AddSearchPathInternal(const char *pPath, const char *pathID, bool bAllowWrite)
{
	if (strstr(pPath, ".bsp")) {
		return;
	}

	// Clean up the name
	char *newPath = (char *)alloca((MAX_PATH + 1) * sizeof(char));

#ifdef _WIN32
	if (strchr(pPath, ':'))
#else
	if (pPath && *pPath == '/')
#endif // _WIN32
	{
		strcpy(newPath, pPath);
	}
	else
	{
		GetCurrentDirectory(newPath, MAX_PATH);
		FixPath(newPath);
		if (memcmp(pPath, ".", 2u)) {
			strcat(newPath, pPath);
		}
	}

#ifdef _WIN32 // don't do this on linux!
	_strlwr(newPath);
#endif // _WIN32

	FixPath(newPath);

	// Make sure that it doesn't already exist
	CUtlSymbol pathSymbol(newPath);
	CUtlSymbol pathIDSymbol(pathID);

	for (int i = 0; i < m_SearchPaths.Count(); i++)
	{
		CSearchPath *pSearchPath = &m_SearchPaths[i];
		if (pSearchPath->GetPath() == pathSymbol && pSearchPath->GetPathID() == pathIDSymbol) {
			// this entry is already at the head
			return;
		}
	}

	// Add to list
	int newIndex = m_SearchPaths.AddToTail();
	CSearchPath *sp = &m_SearchPaths[newIndex];

	sp->SetPath(pathSymbol);
	sp->SetPathID(pathIDSymbol);
	sp->SetWritePath(bAllowWrite);

	// Add pack files for this path next
	AddPackFiles(newPath);
}

bool CBaseFileSystem::RemoveSearchPath(const char *pPath)
{
	char *newPath = (char *)alloca((strlen(pPath) + 1) * sizeof(char));

#ifdef _WIN32
	if (strchr(pPath, ':'))
#else
	if (pPath && *pPath == '/')
#endif // _WIN32
	{
		strcpy(newPath, pPath);
	}
	else
	{
		GetCurrentDirectory(newPath, MAX_PATH);
		FixPath(newPath);
		if (memcmp(pPath, ".", 2u)) {
			strcat(newPath, pPath);
		}
	}

#ifdef _WIN32 // don't do this on linux!
	_strlwr(newPath);
#endif // _WIN32

	FixPath(newPath);

	CUtlSymbol lookup(newPath);

	bool bret = false;

	// Count backward since we're possibly deleting one or more pack files, too
	int i;
	int c = m_SearchPaths.Count();
	for (i = c - 1; i >= 0; i--)
	{
		if (newPath && m_SearchPaths[i].GetPath() != lookup)
			continue;

		m_SearchPaths.Remove(i);
		bret = true;
	}

	return bret;
}

CBaseFileSystem::CSearchPath *CBaseFileSystem::GetWritePath(const char *pathID)
{
	int iPath = 0;
	CSearchPath *searchPath = m_SearchPaths.Base();
	while (searchPath && iPath < m_SearchPaths.Count())
	{
		if (searchPath->GetWritePath()) {
			break;
		}

		searchPath = &m_SearchPaths[iPath++];
	}

	if (!pathID || m_SearchPaths.Count() <= 0) {
		return searchPath;
	}

	CUtlSymbol lookup(pathID);
	for (int i = 0; i < m_SearchPaths.Count(); i++)
	{
		if (m_SearchPaths[i].GetPathID() == lookup) {
			return &m_SearchPaths[i];
		}
	}

	return searchPath;
}

bool CBaseFileSystem::IsDirectory(const char *pFileName)
{
	// Allow for UNC-type syntax to specify the path ID.
	struct _stat buf;

	for (int i = 0; i < m_SearchPaths.Count(); i++)
	{
		CSearchPath *sp = &m_SearchPaths[i];
		if (!sp->IsPackFile())
		{
			int len = strlen(sp->GetPath().String()) + strlen(pFileName);
			char *pTmpFileName = (char *)alloca((len + 1) * sizeof(char));
			strcpy(pTmpFileName, sp->GetPath().String());
			strcat(pTmpFileName, pFileName);

			FixSlashes(pTmpFileName);

			if (FS_stat(pTmpFileName, &buf) != -1)
			{
				if (buf.st_mode & _S_IFDIR)	// 0x4000
					return true;
			}
		}
	}

	return false;
}

/* <ce77> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:328 */
NOBODY FileHandle_t CBaseFileSystem::FindFile(CSearchPath *path, const char *pFileName, const char *pOptions, bool bFromCache)
{
//	{
//		bool bWrite;
//		char *pTmpFileName;
//		FILE *fp;
//		strchr(const char *__s, int __c)
//		CBaseFileSystem::FixSlashes(char *str)
//		{
//			CFileHandle *fh;
//			stat buf;
//			int sr;
//			CFileHandle::CFileHandle()
//		}
//		strchr(const char *__s, int __c)
//	}
//	{
//		CPackFileEntry search;
//		char *temp;
//		int searchresult;
//		CPackFileEntry::CPackFileEntry()
//		CBaseFileSystem::FixSlashes(char *str)
//		CUtlSymbol::operator=(CUtlSymbol &const src)
//		{
//			CPackFileEntry result;
//			CFileHandle *fh;
//			CUtlRBTree<CBaseFileSystem::CPackFileEntry, int>::operator[](int i)
//			CPackFileEntry::CPackFileEntry(CPackFileEntry &const)
//			CFileHandle::CFileHandle()
//		}
//	}
	return FILESYSTEM_INVALID_HANDLE;
}

/* <a699> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:406 */
NOBODY bool CBaseFileSystem::FileExists(const char *pFileName)
{
//	{
//		int i;
//		{
//			int size;
//			CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		}
//	}
	return false;
}

/* <d310> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:421 */
NOBODY FileHandle_t CBaseFileSystem::Open(const char *pFileName, const char *pOptions, const char *pathID)
{
//	{
//		CUtlSymbol lookup;
//		{
//			int i;
//			{
//				FileHandle_t filehandle;
//				CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//			}
//		}
//		CUtlSymbol::operator=(CUtlSymbol &const src)
//	}
//	{
//		CSearchPath *searchPath;
//		char *pTmpFileName;
//		FILE *fp;
//		CBaseFileSystem::GetWritePath(const char *pathID)
//		CBaseFileSystem::FixSlashes(char *str)
//		{
//			CFileHandle *fh;
//			stat buf;
//			int sr;
//			CFileHandle::CFileHandle()
//		}
//	}
	return FILESYSTEM_INVALID_HANDLE;
}

/* <d219> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:481 */
NOBODY FileHandle_t CBaseFileSystem::OpenFromCacheForRead(const char *pFileName, const char *pOptions, const char *pathID)
{
//	{
//		CUtlSymbol lookup;
//		CUtlSymbol::operator=(CUtlSymbol &const src)
//		{
//			int i;
//			{
//				FileHandle_t filehandle;
//				CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//			}
//		}
//	}
	return FILESYSTEM_INVALID_HANDLE;
}

/* <b798> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:509 */
NOBODY void CBaseFileSystem::Close(FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//		CUtlVector<_IO_FILE *>::Find(_IO_FILE *&const src)
//	}
//	CBaseFileSystem::Close(FileHandle_t file)
}

/* <b339> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:538 */
NOBODY void CBaseFileSystem::Seek(FileHandle_t file, int pos, FileSystemSeek_t whence)
{
//	{
//		CFileHandle *fh;
//		int seekType;
//	}
}

/* <b2c6> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:589 */
NOBODY unsigned int CBaseFileSystem::Tell(FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//	}
//	CBaseFileSystem::Tell(FileHandle_t file)
	return 0;
}

unsigned int CBaseFileSystem::Size(FileHandle_t file)
{
	CFileHandle *fh = reinterpret_cast<CFileHandle *>(file);
	if (!fh) {
		Warning(FILESYSTEM_WARNING, "FS:  Tried to Size NULL file handle!\n");
		return 0;
	}

	if (!fh->m_pFile) {
		Warning(FILESYSTEM_WARNING, "FS:  Tried to Size NULL file pointer inside valid file handle!\n");
		return 0;
	}

	return fh->m_nLength;
}

unsigned int CBaseFileSystem::Size(const char *pFileName)
{
	int i = 0;
	while (i < m_SearchPaths.Count())
	{
		int size = FastFindFileSize(&m_SearchPaths[i], pFileName);
		if (size != -1) {
			return size;
		}

		i++;
	}

	return -1;
}

/* <a459> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:646 */
NOBODY int CBaseFileSystem::FastFindFileSize(CSearchPath *path, const char *pFileName)
{
//	{
//		stat buf;
//		{
//			CPackFileEntry search;
//			char *temp;
//			int searchresult;
//			CPackFileEntry::CPackFileEntry()
//			CBaseFileSystem::FixSlashes(char *str)
//			CUtlSymbol::operator=(CUtlSymbol &const src)
//			{
//				CPackFileEntry result;
//				CPackFileEntry::CPackFileEntry(CPackFileEntry &const)
//			}
//		}
//		{
//			char pTmpFileName;
//			CBaseFileSystem::FixSlashes(char *str)
//		}
//	}
	return 0;
}

/* <b1e0> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:689 */
NOBODY bool CBaseFileSystem::EndOfFile(FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//	}
//	CBaseFileSystem::EndOfFile(FileHandle_t file)
	return false;
}

/* <b144> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:718 */
NOBODY int CBaseFileSystem::Read(void *pOutput, int size, FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//		int result;
//	}
	return 0;
}

/* <b0b7> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:743 */
NOBODY int CBaseFileSystem::Write(const void *pInput, int size, FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//	}
	return 0;
}

/* <b041> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:763 */
NOBODY int CBaseFileSystem::FPrintf(FileHandle_t file, char *pFormat, ...)
{
//	{
//		CFileHandle *fh;
//		va_list args;
//		int len;
//	}
	return 0;
}

/* <8eae> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:788 */
NOBODY void *CBaseFileSystem::GetReadBuffer(FileHandle_t file, int *outBufferSize, bool failIfNotInCache)
{
	return nullptr;
}

/* <8f01> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:798 */
NOBODY void CBaseFileSystem::ReleaseReadBuffer(FileHandle_t file, void *readBuffer)
{
}

/* <afce> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:806 */
NOBODY bool CBaseFileSystem::IsOk(FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//	}
//	CBaseFileSystem::IsOk(FileHandle_t file)
	return false;
}

/* <af5b> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:830 */
NOBODY void CBaseFileSystem::Flush(FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//	}
//	CBaseFileSystem::Flush(FileHandle_t file)
}

/* <aece> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:851 */
NOBODY char *CBaseFileSystem::ReadLine(char *pOutput, int maxChars, FileHandle_t file)
{
//	{
//		CFileHandle *fh;
//	}
//	CBaseFileSystem::ReadLine(char *pOutput, int maxChars, FileHandle_t file)
	return nullptr;
}

/* <97e6> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:871 */
NOBODY void CBaseFileSystem::RemoveFile(const char *pRelativePath, const char *pathID)
{
//	{
//		CSearchPath *searchPath;
//		char *pTmpFileName;
//		CBaseFileSystem::GetWritePath(const char *pathID)
//		CBaseFileSystem::FixSlashes(char *str)
//	}
}

/* <9bd8> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:884 */
NOBODY const char *CBaseFileSystem::GetLocalPath(const char *pFileName, char *pLocalPath, int localPathBufferSize)
{
//	{
//		stat buf;
//		int i;
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		{
//			CPackFileEntry search;
//			char *temp;
//			int searchresult;
//			CPackFileEntry::CPackFileEntry()
//			CBaseFileSystem::FixSlashes(char *str)
//			CUtlSymbol::operator=(CUtlSymbol &const src)
//			CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//			CUtlRBTree<CBaseFileSystem::CPackFileEntry, int>::Find(CPackFileEntry &const search)
//			{
//				char *pTmpFileName;
//				int len;
//				CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//				CBaseFileSystem::FixSlashes(char *str)
//			}
//		}
//		{
//			char *pTmpFileName;
//			int len;
//			CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//			CBaseFileSystem::FixSlashes(char *str)
//		}
//		CBaseFileSystem::FixSlashes(char *str)
//	}
	return nullptr;
}

/* <cc15> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:951 */
NOBODY FILE *CBaseFileSystem::Trace_FOpen(const char *filename, const char *options, bool bFromCache)
{
//	{
//		FILE *fp;
//		{
//			COpenedFile file;
//			COpenedFile::SetName(const char *name)
//			CUtlVector<CBaseFileSystem::COpenedFile>::AddToTail(COpenedFile &const src)
//			COpenedFile::~COpenedFile()
//		}
//	}
	return nullptr;
}

/* <b48d> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:978 */
NOBODY void CBaseFileSystem::Trace_FClose(FILE *fp)
{
//	{
//		COpenedFile file;
//		int result;
//		CUtlVector<CBaseFileSystem::COpenedFile>::Find(COpenedFile &const src)
//		{
//			COpenedFile found;
//			COpenedFile::COpenedFile(COpenedFile &const src)
//			COpenedFile::GetName()
//			CUtlVector<CBaseFileSystem::COpenedFile>::FindAndRemove(COpenedFile &const src)
//			COpenedFile::~COpenedFile()
//		}
//	}
}

void CBaseFileSystem::Trace_DumpUnclosedFiles()
{
	for (int i = 0; i < m_OpenedFiles.Count(); i++)
	{
		if (m_fwLevel <= FILESYSTEM_WARNING_QUIET) {
			continue;
		}

		COpenedFile *found = &m_OpenedFiles[i];
		Warning(FILESYSTEM_WARNING_REPORTUNCLOSED, "File %s was never closed\n", found ? found->GetName() : "???");
	}
}

/* <103de> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1031 */
NOBODY bool CBaseFileSystem::AddPackFile(const char *pFileName, const char *pathID)
{
	return false;
}

/* <fb5d> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1040 */
NOBODY bool CBaseFileSystem::AddPackFileFromPath(const char *pPath, const char *pakfile, bool bCheckForAppendedPack, const char *pathID)
{
//	{
//		char fullpath;
//		stat buf;
//		int size;
//		CSearchPath *sp;
//		int64 headeroffset;
//		CBaseFileSystem::FixSlashes(char *str)
//		CUtlVector<CBaseFileSystem::CSearchPath>::AddToTail()
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		CUtlSymbol::operator=(CUtlSymbol &const src)
//		CUtlSymbol::operator=(CUtlSymbol &const src)
//		CFileHandle::CFileHandle()
//		{
//			packappenededheader_t appended;
//			CUtlVector<CBaseFileSystem::CSearchPath>::Remove(int elem)
//		}
//		CUtlVector<CBaseFileSystem::CSearchPath>::Remove(int elem)
//		CUtlVector<_IO_FILE *>::AddToTail(_IO_FILE *&const src)
//	}
	return false;
}

/* <fa35> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1110 */
NOBODY bool CBaseFileSystem::PreparePackFile(CSearchPath &packfile, int64 offsetofpackinmetafile)
{
//	{
//		packheader_t header;
//		int i;
//		packfile_t *newfiles;
//		int numpackfiles;
//		{
//			CPackFileEntry lookup;
//			CPackFileEntry::CPackFileEntry()
//			CBaseFileSystem::FixSlashes(char *str)
//			CUtlSymbol::operator=(CUtlSymbol &const src)
//		}
//	}
	return false;
}

/* <f8ff> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1188 */
NOBODY bool CBaseFileSystem::Prepare64BitPackFile(CSearchPath &packfile, int64 offsetofpackinmetafile)
{
//	{
//		packheader64_t header;
//		packfile64_t *newfiles;
//		int numpackfiles;
//		{
//			int i;
//			{
//				CPackFileEntry lookup;
//				CPackFileEntry::CPackFileEntry()
//				CBaseFileSystem::FixSlashes(char *str)
//				CUtlSymbol::operator=(CUtlSymbol &const src)
//			}
//		}
//	}
	return false;
}

/* <ffbb> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1255 */
NOBODY void CBaseFileSystem::AddPackFiles(const char *pPath)
{
//	{
//		int pakcount;
//		int i;
//		{
//			char pakfile;
//			char fullpath;
//			stat buf;
//			CBaseFileSystem::FixSlashes(char *str)
//		}
//		{
//			char pakfile;
//		}
//	}
}

/* <bfeb> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1290 */
NOBODY void CBaseFileSystem::RemoveAllMapSearchPaths()
{
//	{
//		int i;
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		CUtlVector<CBaseFileSystem::CSearchPath>::Remove(int elem)
//	}
}

/* <a739> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1307 */
NOBODY int WildCardMatch(const char *mask, const char *name)
{
	return 0;
}

/* <a764> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1366 */
NOBODY const char *CBaseFileSystem::SearchPakFile(const char *pWildCard, int currentSearchPathID, bool first)
{
//	CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//	CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//	{
//		const char *file;
//		WildCardMatch(const char *mask, const char *name)
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		CUtlRBTree<CBaseFileSystem::CPackFileEntry, int>::operator[](int i)
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//	}
	return nullptr;
}

/* <d95c> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1390 */
NOBODY const char *CBaseFileSystem::FindFirst(const char *pWildCard, FileFindHandle_t *pHandle, const char *pathID)
{
//	{
//		FileFindHandle_t hTmpHandle;
//		FindData_t *pFindData;
//		CUtlSymbol lookup;
//		CUtlVector<CBaseFileSystem::FindData_t>::AddToTail()
//		CUtlVector<CBaseFileSystem::FindData_t>::operator[](int i)
//		CUtlVector<char>::AddMultipleToTail(int num)
//		CUtlVector<char>::Base()
//		CBaseFileSystem::FixSlashes(char *str)
//		{
//			const char *fileName;
//			CUtlSymbol::operator const UtlSymId_t()
//		}
//		CUtlSymbol::operator=(CUtlSymbol &const src)
//		CUtlVector<CBaseFileSystem::FindData_t>::Remove(int elem)
//	}
	return nullptr;
}

/* <a934> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1437 */
NOBODY const char *CBaseFileSystem::FindFirstHelper(const char *pWildCard, FileFindHandle_t *pHandle, int searchPath, FindData_t *pFindData)
{
//	CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//	{
//		const char *fname;
//	}
//	{
//		char *pTmpFileName;
//		int size;
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		CBaseFileSystem::FixSlashes(char *str)
//	}
	return nullptr;
}

/* <aa7f> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1481 */
NOBODY bool CBaseFileSystem::FileInSearchPaths(const char *pSearchWildcard, const char *pFileName, int minSearchPathID, int maxSearchPathID)
{
//	{
//		const char *tmp;
//		int pathStrLen;
//		int fileNameStrLen;
//		char *pFileNameWithPath;
//		int i;
//		{
//			char *fullFilePath;
//			int len;
//			stat buf;
//			CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		}
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		{
//			int curSearch;
//			bool ret;
//		}
//	}
	return false;
}

/* <ac12> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1548 */
NOBODY bool CBaseFileSystem::FindNextFileHelper(FindData_t *pFindData)
{
//	{
//		const char *file;
//		{
//			char *pFileNameNoPath;
//			strrchr(const char *__s, int __c)
//			CBaseFileSystem::FixSlashes(char *str)
//		}
//	}
//	{
//		char *pTmpFileName;
//		CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		CBaseFileSystem::FixSlashes(char *str)
//	}
//	{
//		const char *file;
//		strrchr(const char *__s, int __c)
//		CBaseFileSystem::FixSlashes(char *str)
//	}
//	CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
	return false;
}

/* <addb> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1625 */
NOBODY const char *CBaseFileSystem::FindNext(FileFindHandle_t handle)
{
//	{
//		FindData_t *pFindData;
//		CUtlVector<CBaseFileSystem::FindData_t>::operator[](int i)
//	}
	return nullptr;
}

/* <8f45> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1654 */
NOBODY bool CBaseFileSystem::FindIsDirectory(FileFindHandle_t handle)
{
//	{
//		FindData_t *pFindData;
//	}
	return false;
}

NOBODY void CBaseFileSystem::FindClose(FileFindHandle_t handle)
{
}

/* <742e> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1688 */
NOBODY void InitializeCharacterSets()
{
//	{
//		bool s_CharacterSetInitialized;
//	}
}

/* <91fd> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1702 */
NOBODY char *CBaseFileSystem::ParseFile(char *pFileBytes, char *pToken, bool *pWasQuoted)
{
//skipwhite:
//	{
//		boolconst com_ignorecolons;
//		characterset_t &const breaks;
//		int c;
//		int len;
//		InitializeCharacterSets()
//	}
	return nullptr;
}

/* <d15a> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1786 */
NOBODY long CBaseFileSystem::GetFileTime(const char *pFileName)
{
//	{
//		int i;
//		{
//			FileHandle_t filehandle;
//			CFileHandle *fh;
//			long int time;
//			CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//		}
//	}
	return 0L;
}

/* <91a9> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1811 */
NOBODY void CBaseFileSystem::FileTimeToString(char *pString, int maxCharsIncludingTerminator, long int fileTime)
{
}

/* <8f90> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1820 */
NOBODY void CBaseFileSystem::SetWarningFunc(WarningFunc_t pfnWarning)
{
}

/* <8fc5> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1828 */
NOBODY void CBaseFileSystem::SetWarningLevel(FileWarningLevel_t level)
{
}

/* <ae60> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1836 */
NOBODY void CBaseFileSystem::Warning(FileWarningLevel_t level, const char *fmt, ...)
{
//	{
//		va_list argptr;
//		char warningtext;
//	}
}

/* <903a> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1868 */
NOBODY bool CBaseFileSystem::FullPathToRelativePath(const char *pFullpath, char *pRelative)
{
//	{
//		bool success;
//		int inlen;
//		char *inpath;
//		CBaseFileSystem::FixSlashes(char *str)
//		{
//			int i;
//			{
//				char *searchbase;
//				CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//				CUtlVector<CBaseFileSystem::CSearchPath>::operator[](int i)
//				CBaseFileSystem::FixSlashes(char *str)
//			}
//		}
//	}
	return false;
}

/* <b8fc> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1918 */
NOBODY bool CBaseFileSystem::OpenedFileLessFunc(COpenedFile const &src1, COpenedFile const &src2)
{
	return false;
}

/* <be8e> ../filesystem/FileSystem_Stdio/BaseFileSystem.cpp:1926 */
NOBODY void CBaseFileSystem::RemoveAllSearchPaths()
{
//	CUtlVector<CBaseFileSystem::CSearchPath>::Purge()
//	CUtlVector<_IO_FILE *>::Purge()
}

NOXREF void CBaseFileSystem::StripFilename(char *path)
{
	int length = strlen(path) - 1;
	while (length > 0)
	{
		if (path[length] == CORRECT_PATH_SEPARATOR
			|| path[length] == INCORRECT_PATH_SEPARATOR)
			break;

		length--;
	}

	path[length] = '\0';
}

void CBaseFileSystem::FixSlashes(char *str)
{
	while (*str)
	{
		if (*str == INCORRECT_PATH_SEPARATOR) {
			*str = CORRECT_PATH_SEPARATOR;
		}

		str++;
	}
}

void CBaseFileSystem::FixPath(char *str)
{
	char *lastChar = &str[strlen(str) - 1];
	if (*lastChar != CORRECT_PATH_SEPARATOR && *lastChar != INCORRECT_PATH_SEPARATOR)
	{
		lastChar[1] = CORRECT_PATH_SEPARATOR;
		lastChar[2] = '\0';
	}

	FixSlashes(str);
}

CBaseFileSystem::COpenedFile::COpenedFile() : m_pFile(nullptr), m_pName(nullptr)
{
}

CBaseFileSystem::COpenedFile::~COpenedFile()
{
	if (m_pName) {
		delete[] m_pName;
		m_pName = nullptr;
	}
}

CBaseFileSystem::COpenedFile::COpenedFile(COpenedFile const &src)
{
	m_pFile = src.m_pFile;
	m_pName = nullptr;

	if (src.m_pName)
	{
		m_pName = new char [strlen(src.m_pName) + 1];
		strcpy(m_pName, src->m_pName);
	}
}

bool CBaseFileSystem::COpenedFile::operator==(COpenedFile const &src) const
{
	return m_pFile == src.m_pFile;
}

void CBaseFileSystem::COpenedFile::SetName(const char *name)
{
	if (m_pName) {
		delete[] m_pName;
	}

	m_pName = new char [strlen(name) + 1];
	strcpy(m_pName, name);
}

const char *CBaseFileSystem::COpenedFile::GetName()
{
	return m_pName ? m_pName : "???";
}

bool CBaseFileSystem::CSearchPath::PackFileLessFunc(CPackFileEntry const &src1, CPackFileEntry const &src2)
{
	return src1.m_Name < src2.m_Name;
}

CBaseFileSystem::CSearchPath::CSearchPath() :
	m_PackFiles(0, MAX_ENTRY_PATH, CSearchPath::PackFileLessFunc),
	m_Path(CUtlSymbol("")),
	m_bIsMapPath(false),
	m_bIsPackFile(false),
	m_bAllowWrite(true),
	m_lPackFileTime(0),
	m_nNumPackFiles(0),
{
}

CBaseFileSystem::CSearchPath::~CSearchPath()
{
	if (m_bIsPackFile && m_hPackFile)
	{
		s_pFileSystem->m_PackFileHandles.FindAndRemove(m_hPackFile->m_pFile);
		s_pFileSystem->Close(m_hPackFile->m_pFile);
	}
}
